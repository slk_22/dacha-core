<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"DISPLAY_DAYS" => Array(
		"NAME" => GetMessage("IMYIE_WHEATHER_DISPLAY_DAYS"),
		"TYPE" => "STRING",
		"DEFAULT" => "1",
	),
);?>