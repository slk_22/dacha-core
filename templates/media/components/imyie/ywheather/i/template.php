<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if(!empty($arResult["NOT_FORMATED"]["day"])):
	/*?><textarea><?print_r($arResult)?></textarea><?*/
	?><section class="info_wheather">
	<h1><?=GetMessage("IMYIE_WHEATHER_RIGHT_WHEATHER_NOW")?>, <?=$arResult["NOT_FORMATED"]["@attributes"]["city"]?></h1>
		<div class="wheather_wrap block_justify"><?
		foreach($arResult["NOT_FORMATED"]["day"] as $keyDay => $arDay):
			if($keyDay < $arParams["DISPLAY_DAYS"]):
			$date_arr = explode('-', $arDay["@attributes"]["date"]);
			$date_time = mktime(0, 0, 0, $date_arr[1], $date_arr[2], $date_arr[0]);
				?><article class="wheather_item" <?if($keyDay%5==4):?>style="border:none;"<?endif?>>
					<h1><?=FormatDate("D", $date_time)?><br/><?=$date_arr[2]?>/<?=$date_arr[1]?></h1><?
					$img_path = $templateFolder."/img/".$arDay["day_part"][4]["image-v3"].".gif";
					if(!empty($arDay["day_part"][4]["image-v3"]) && file_exists($_SERVER["DOCUMENT_ROOT"].$img_path)):
						?><img src="<?=$img_path?>" alt="<?=$arDay["day_part"][4]["weather_type"]?>" title="<?=$arDay["day_part"][4]["weather_type"]?>" /><?
					else:
						?><img src="<?=$templateFolder?>/img/na.png" alt="" /><?
					endif;
					?><div class="wheather_data"><?
					if($arDay["day_part"][4]["temperature-data"]["avg"] > $arDay["day_part"][5]["temperature-data"]["avg"]){
						$day_temp_min = $arDay["day_part"][5]["temperature-data"]["avg"];
						$day_temp_max = $arDay["day_part"][4]["temperature-data"]["avg"];
					}
					else{
						$day_temp_min = $arDay["day_part"][4]["temperature-data"]["avg"];
						$day_temp_max = $arDay["day_part"][5]["temperature-data"]["avg"];
					}
					if($day_temp_min>0):
						?>+<?
					endif;
					echo $day_temp_min?>...<?
					if($day_temp_max>0):
						?>+<?
					endif;
					echo $day_temp_max?>&#176;C<br/><?
					echo $arDay["day_part"][4]["weather_type_short"]?><br/><?
					echo $arDay["day_part"][4]["pressure"]?> <?=GetMessage("IMYIE_WHEATHER_PRESSURE_ED")?><br /><?
					echo $arDay["day_part"][4]["wind_speed"];?> <?=GetMessage("IMYIE_WHEATHER_WIND_SPEED_ED")
					?></div	>
				</article> <?
			endif;
		endforeach
		?></div>
	</section><?
endif?>
