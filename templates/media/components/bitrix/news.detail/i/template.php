﻿<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<article class="news_detail"><?
	if($arParams["DISPLAY_NAME"]!="N" && $arResult["NAME"]):
		?><h1><?=$arResult["NAME"];
		if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):
			?> | <time class="text_color"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></time><?
		endif;
		?></h1><?
	endif;
	if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arResult["FIELDS"]["PREVIEW_TEXT"]):
		?><p class="preview_text"><strong><?=$arResult["FIELDS"]["PREVIEW_TEXT"];unset($arResult["FIELDS"]["PREVIEW_TEXT"]);?></strong></p><?
	endif;
	if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):	?>

		<?php if ($arResult["PROPERTIES"]["W_MARK"]["VALUE"] == ''): ?>

			<?php 
				$res = CIBlockElement::GetList(array(), array("ID" => $arResult["ID"]));
				while($ob = $res->GetNextElement())
				{ 
				    $arFields = $ob->GetFields();
				    $pic2_id = $arFields['DETAIL_PICTURE'];// id детальной картинки
				}

				$arWaterMark = Array(
				    array(
				        "name" => "watermark",
				        "position" => "bottomright",
				        "type" => "image",
				        "size" => "small",
				        "file" => $_SERVER["DOCUMENT_ROOT"].'/watermark/wm.png',
				    )
				);

				$arFileTmp2 = CFile::ResizeImageGet(
				    $pic2_id,
				    array("width" => $arResult["DETAIL_PICTURE"]["WIDTH"], "height" => $arResult["DETAIL_PICTURE"]["HEIGHT"]),
				    BX_RESIZE_IMAGE_EXACT,
				    true,
				    $arWaterMark
				);
			?>

			<img class="detail_picture wm" src="<?=$arFileTmp2["src"]?>" alt="<?=$arResult["NAME"]?>"  title="<?=$arResult["NAME"]?>" />
			
		<?php else: ?>

		<img class="detail_picture" src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" alt="<?=$arResult["NAME"]?>"  title="<?=$arResult["NAME"]?>" />
		<?php endif ?>

	<? endif; ?>

	<div class="content"><?
	if($arResult["NAV_RESULT"]):
		if($arParams["DISPLAY_TOP_PAGER"]):
			echo $arResult["NAV_STRING"];
		endif;
		echo $arResult["NAV_TEXT"];
		if($arParams["DISPLAY_BOTTOM_PAGER"]):
			echo $arResult["NAV_STRING"];
		endif;
	elseif(strlen($arResult["DETAIL_TEXT"])>0):
		echo $arResult["DETAIL_TEXT"];
	else:
		echo $arResult["PREVIEW_TEXT"];
	endif;
/*
	foreach($arResult["FIELDS"] as $code=>$value):
		echo GetMessage("IBLOCK_FIELD_".$code)?>: <?=$value;?><br /><?
	endforeach;
	foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty):
		echo $arProperty["NAME"]?>: <?
		if(is_array($arProperty["DISPLAY_VALUE"])):
			echo implode(" / ", $arProperty["DISPLAY_VALUE"]);
		else:
			echo $arProperty["DISPLAY_VALUE"];
		endif
		?><br /><?
	endforeach;
*/
	?></div><?
	if(array_key_exists("USE_SHARE", $arParams) && $arParams["USE_SHARE"] == "Y"){
		?><div class="news-detail-share">
			<noindex><?
			$APPLICATION->IncludeComponent("bitrix:main.share", "", array(
					"HANDLERS" => $arParams["SHARE_HANDLERS"],
					"PAGE_URL" => $arResult["~DETAIL_PAGE_URL"],
					"PAGE_TITLE" => $arResult["~NAME"],
					"SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
					"SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
					"HIDE" => $arParams["SHARE_HIDE"],
				),
				$component,
				array("HIDE_ICONS" => "Y")
			);
			?></noindex>
		</div><?
	}
?></article>



 
<b>Теги</b>
<br>

<?
$arr= explode(',',$arResult[TAGS]);
foreach($arr as $keys)
{
   print '<a href="/search/?tags='.$keys.'" >'.$keys.'</a> ';
$tags_serach_tag .=$keys;
$tags_serach_tag_arr[]=$keys;
}
?>
<br><br>























<br><br>
<b style="font-size:16pt; color:#02A750;">Читайте еще по этой теме:</b>
<br><br>

<?php 
$arr= explode(',',$arResult[TAGS]);
$arr = implode('||', $arr);
					$arFilter = Array("IBLOCK_ID"=>1, Array("?TAGS"=>$arr, "!ID"=>$arResult[ID]), "ACTIVE"=>"Y", ">=DATE_ACTIVE_TO"   => array(false, ConvertTimeStamp(false, "FULL")), );
					$res = CIBlockElement::GetList(Array(), $arFilter, false, false);
					$etaj = array();
					while($ob = $res->GetNextElement()){
					   $arFieldsEtaj = $ob->GetFields();  
					   $etajefd[] = $arFieldsEtaj;
//echo '<pre>';
//print_r($etajefd);
//echo '</pre>';
					} 
echo '<ul>';
for($x = 0; $x <= 4; $x++){
?>
<li><a href="<?php echo $etajefd[$x]["DETAIL_PAGE_URL"];?>"><?php echo $etajefd[$x]["NAME"];?></a></li>
<?php
}
echo '<ul>';
				?>
<hr>