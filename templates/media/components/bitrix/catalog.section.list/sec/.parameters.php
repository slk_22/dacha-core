<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arSorts = array("ASC"=>GetMessage("T_IBLOCK_DESC_ASC"), "DESC"=>GetMessage("T_IBLOCK_DESC_DESC"));
$arSortFields = array(
	"ID"=>GetMessage("T_IBLOCK_DESC_FID"),
	"NAME"=>GetMessage("T_IBLOCK_DESC_FNAME"),
	"ACTIVE_FROM"=>GetMessage("T_IBLOCK_DESC_FACT"),
	"SORT"=>GetMessage("T_IBLOCK_DESC_FSORT"),
	"TIMESTAMP_X"=>GetMessage("T_IBLOCK_DESC_FTSAMP")
);
$arSectionTemplates = array(
	"vert" => GetMessage("NEWS_TMPL_NAME_1"),
	"hor" => GetMessage("NEWS_TMPL_NAME_2"),
);

$arProperty_LNS = array();
$rsProp = CIBlockProperty::GetList(array("sort"=>"asc", "name"=>"asc"), array("ACTIVE"=>"Y", "IBLOCK_ID"=>(isset($arCurrentValues["IBLOCK_ID"])?$arCurrentValues["IBLOCK_ID"]:$arCurrentValues["ID"])));
while ($arr=$rsProp->Fetch()){
	$arProperty[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
	if (in_array($arr["PROPERTY_TYPE"], array("L", "N", "S"))){
		$arProperty_LNS[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
	}
}
$arIBlockSections = array();
$rsSections = CIBlockSection::GetList(
    array("SORT"=>"ASC", "NAME" => "ASC"),
    array("ACTIVE"=>"Y", "IBLOCK_ID"=>(isset($arCurrentValues["IBLOCK_ID"])?$arCurrentValues["IBLOCK_ID"]:$arCurrentValues["ID"])),
	false,
	array("IBLOCK_ID", "ID", "NAME", "CODE")
);
while ($arr=$rsSections->Fetch()){
	$arIBlockSections[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
}
$arTemplateParameters = array(
	"DISPLAY_DATE" => array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_DATE"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"DISPLAY_NAME" => array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_NAME"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"DISPLAY_PICTURE" => array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_PICTURE"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"DISPLAY_PREVIEW_TEXT" => array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_TEXT"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"DISPLAY_DISPLAY" => array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_DISPLAY"),
		"TYPE" => "STRING",
		"DEFAULT" => "2",
	),
	/*****************/
	"NEWS_COUNT" => array(
		"PARENT" => "BASE",
		"NAME" => GetMessage("T_IBLOCK_DESC_LIST_CONT"),
		"TYPE" => "STRING",
		"DEFAULT" => "20",
	),
	"SORT_BY1" => array(
		"PARENT" => "DATA_SOURCE",
		"NAME" => GetMessage("T_IBLOCK_DESC_IBORD1"),
		"TYPE" => "LIST",
		"DEFAULT" => "ACTIVE_FROM",
		"VALUES" => $arSortFields,
		"ADDITIONAL_VALUES" => "Y",
	),
	"SORT_ORDER1" => array(
		"PARENT" => "DATA_SOURCE",
		"NAME" => GetMessage("T_IBLOCK_DESC_IBBY1"),
		"TYPE" => "LIST",
		"DEFAULT" => "DESC",
		"VALUES" => $arSorts,
		"ADDITIONAL_VALUES" => "Y",
	),
	"SORT_BY2" => array(
		"PARENT" => "DATA_SOURCE",
		"NAME" => GetMessage("T_IBLOCK_DESC_IBORD2"),
		"TYPE" => "LIST",
		"DEFAULT" => "SORT",
		"VALUES" => $arSortFields,
		"ADDITIONAL_VALUES" => "Y",
	),
	"SORT_ORDER2" => array(
		"PARENT" => "DATA_SOURCE",
		"NAME" => GetMessage("T_IBLOCK_DESC_IBBY2"),
		"TYPE" => "LIST",
		"DEFAULT" => "ASC",
		"VALUES" => $arSorts,
		"ADDITIONAL_VALUES" => "Y",
	),
	"FILTER_NAME" => array(
		"PARENT" => "DATA_SOURCE",
		"NAME" => GetMessage("T_IBLOCK_FILTER"),
		"TYPE" => "STRING",
		"DEFAULT" => "",
	),
	"FIELD_CODE" => CIBlockParameters::GetFieldCode(GetMessage("IBLOCK_FIELD"), "DATA_SOURCE"),
	"PROPERTY_CODE" => array(
		"PARENT" => "DATA_SOURCE",
		"NAME" => GetMessage("T_IBLOCK_PROPERTY"),
		"TYPE" => "LIST",
		"MULTIPLE" => "Y",
		"VALUES" => $arProperty_LNS,
		"ADDITIONAL_VALUES" => "Y",
	),
	"CHECK_DATES" => array(
		"PARENT" => "DATA_SOURCE",
		"NAME" => GetMessage("T_IBLOCK_DESC_CHECK_DATES"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"DETAIL_URL" => CIBlockParameters::GetPathTemplateParam(
		"DETAIL",
		"DETAIL_URL",
		GetMessage("T_IBLOCK_DESC_DETAIL_PAGE_URL"),
		"",
		"URL_TEMPLATES"
	),
	"HIDE_LINK_WHEN_NO_DETAIL" => array(
		"PARENT" => "ADDITIONAL_SETTINGS",
		"NAME" => GetMessage("T_IBLOCK_DESC_HIDE_LINK_WHEN_NO_DETAIL"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "N",
	),
	"FILTER_SECTION_CODE" => array(
		"PARENT" => "BASE",
		"NAME" => GetMessage("CP_BCSL_FILTER_SECTION_CODE"),
		"TYPE" => "LIST",
		"MULTIPLE" => "Y",
		"VALUES" => $arIBlockSections,
		"REFRESH" => "Y",
	),
);
if(!empty($arCurrentValues["FILTER_SECTION_CODE"])){
	foreach($arCurrentValues["FILTER_SECTION_CODE"] as $keySectionID => $sectionID){
		$arTemplateParameters["FILTER_SECTION_".$sectionID."_TEMPLATE"] = array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("CP_BCSL_FILTER_SECTION_TEMPLATE").' '.$arIBlockSections[$sectionID],
			"TYPE" => "LIST",
			"VALUES" => $arSectionTemplates,
			"DEFAULT" => $arSectionTemplates[0],
		);
		$arTemplateParameters["FILTER_SECTION_".$sectionID."_DISPLAY"] = array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("CP_BCSL_FILTER_SECTION_DISPLAY").' '.$arIBlockSections[$sectionID],
			"TYPE" => "STRING",
			"DEFAULT" => "3",
		);

	}
;
}?>