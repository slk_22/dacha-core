<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if(!empty($arResult)):
?><nav id="menu_foot"><?
$count = count($arResult) - 1;
foreach($arResult as $keyItem => $arItem):
	if($arItem["PERMISSION"] > "D"):
		?><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a><?
		if($keyItem != $count):
		?><span>&nbsp;| </span><?
		endif;
	endif;
endforeach
?></nav><?
endif?>