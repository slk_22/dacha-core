<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

if(!CModule::IncludeModule("iblock"))
	return;

/* _________________________________________________________________________________ */
$arFields = Array(
	"ID" => "articles",
	"SECTIONS" => "Y",
	"IN_RSS" => "N",
	"SORT" => 101,
	"LANG" => Array(
		"en" => Array(
			"NAME" => GetMessage("IBLOCK_1_EN_TYPE_NAME"),
			"ELEMENT_NAME" => GetMessage("IBLOCK_1_EN_ELEMENT_NAME"),
			"SECTION_NAME" => GetMessage("IBLOCK_1_EN_SECTION_NAME")
		),
		"ru" => Array(
			"NAME" => GetMessage("IBLOCK_1_RU_TYPE_NAME"),
			"ELEMENT_NAME" => GetMessage("IBLOCK_1_RU_ELEMENT_NAME"),
			"SECTION_NAME" => GetMessage("IBLOCK_1_RU_SECTION_NAME")
		)
	)
);

$obBlocktype = new CIBlockType;
$DB->StartTransaction();
$res = $obBlocktype->Add($arFields);
if(!$res)
{
	$DB->Rollback(); // error part
}
else
{
	$DB->Commit();
}

/* _________________________________________________________________________________ */
?>