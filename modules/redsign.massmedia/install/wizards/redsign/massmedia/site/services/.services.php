<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arServices = Array(
	// Create site
	"site_create" => array(
		"NAME" => GetMessage("SERVICE_STEP_SITE_CREATE"),
		"STAGES" => array(
			"index.php",
		),
		"MODULE_ID" => "main",
	),

	// Copy files (public and php_interface)
	"files" => array(
		"NAME" => GetMessage("SERVICE_STEP_COPY_FILES"),
		"STAGES" => array(
			"index.php",
		),
		"MODULE_ID" => "main",
	),

	// Copy template and theme
	"template" => array(
		"NAME" => GetMessage("SERVICE_STEP_COPY_TEMPLATES"),
		"STAGES" => array(
			"template.php",
			"theme.php",
		),
		"MODULE_ID" => "main",
	),

	// Set menu types
	"menu" => array(
		"NAME" => GetMessage("SERVICE_STEP_SET_MENU_TYPES"),
		"STAGES" => array(
			"index.php",
		),
		"MODULE_ID" => "main",
	),

	// Add our rules to urlrewrite
	"urlrewrite" => array(
		"NAME" => GetMessage("SERVICE_STEP_URL_REWRITE"),
		"STAGES" => array(
			"index.php",
		),
		"MODULE_ID" => "main",
	),

	// Set some settngs
	"settings" => array(
		"NAME" => GetMessage("SERVICE_STEP_SETTINGS"),
		"STAGES" => array(
			"index.php",
			"last_settings.php",
		),
		"MODULE_ID" => "main",
	),

	// Set iblock types and import iblock
	"iblock" => array(
		"NAME" => GetMessage("SERVICE_STEP_IBLOCK"),
		"STAGES" => array(
			"types.php",
			"news.php",
		),
		"MODULE_ID" => "iblock",
	),

	// MODULE DevCom
	"module_wheather" => array(
		"NAME" => GetMessage("SERVICE_STEP_INSTALL_MODULE_WHEATHER"),
		"STAGES" => array(
			"index.php",
		),
		"MODULE_ID" => "main",
	),

	// Replace macros
	"macros" => array(
		"NAME" => GetMessage("SERVICE_STEP_MACROS"),
		"STAGES" => array(
			"index.php",
		),
		"MODULE_ID" => "main",
	),
);
?>