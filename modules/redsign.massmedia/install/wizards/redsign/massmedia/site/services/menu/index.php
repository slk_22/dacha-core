<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if(CModule::IncludeModule('fileman')){
	$arMenuTypes['top'] = GetMessage("WIZ_MENU_top");
	$arMenuTypes['bottom'] = GetMessage("WIZ_MENU_catalog");
	SetMenuTypes($arMenuTypes, WIZARD_SITE_ID);
}?>