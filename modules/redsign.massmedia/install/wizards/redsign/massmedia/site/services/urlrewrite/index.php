<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arUrlRewrite = array();
if (file_exists(WIZARD_SITE_ROOT_PATH."/urlrewrite.php"))
{
	include(WIZARD_SITE_ROOT_PATH."/urlrewrite.php");
}

$arUrlsRewritesNew = array(
	array(
		"CONDITION"	=>	"#^#SITE_DIR#articles/#",
		"RULE"	=>	"",
		"ID"	=>	"bitrix:news",
		"PATH"	=>	"#SITE_DIR#articles/index.php",
	),
	
);

// dont lose old rewrite rules
$result = array_merge($arUrlRewrite, $arUrlsRewritesNew);

foreach($result as $key => $item){
	CUrlRewriter::Add($item);
}
?>