<?if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>
<div class="mfeedback"><?
if(!empty($arResult["ERROR_MESSAGE"])){
	foreach($arResult["ERROR_MESSAGE"] as $v)
		ShowError($v);
}
if(strlen($arResult["OK_MESSAGE"]) > 0){
	?><div class="mf-ok-text"><?=$arResult["OK_MESSAGE"]?></div><?
}
?>
<form action="<?=$APPLICATION->GetCurPage()?>" method="POST">
	<?=bitrix_sessid_post()?>
	<p>
		<label for="MF_USER_NAME" class="mf-text"><?=GetMessage("MFT_NAME")?><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("NAME", $arParams["REQUIRED_FIELDS"])):?><span class="mf-req">*</span><?endif?><br/></label>
		<input type="text" id="MF_USER_NAME" name="user_name" value="<?=$arResult["AUTHOR_NAME"]?>">
	</p>
	<p>
		<label for="MF_USER_EMAIL" class="mf-text"><?=GetMessage("MFT_EMAIL")?><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("EMAIL", $arParams["REQUIRED_FIELDS"])):?><span class="mf-req">*</span><?endif?><br/></label>
		<input type="text" id="MF_USER_EMAIL" name="user_email" value="<?=$arResult["AUTHOR_EMAIL"]?>">
	</p>
	<p>
		<label id="MF_MESSAGE" class="mf-text"><?=GetMessage("MFT_MESSAGE")?><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("MESSAGE", $arParams["REQUIRED_FIELDS"])):?><span class="mf-req">*</span><?endif?><br/></label>
		<textarea id="MF_MESSAGE" name="MESSAGE" rows="5" cols="40"><?=$arResult["MESSAGE"]?></textarea>
	</p><?
	if($arParams["USE_CAPTCHA"] == "Y"):
	?><input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>">
	<p class="mf-captcha">
		<label class="mf-text" for="MF_CAPTCHA"><?=GetMessage("MFT_CAPTCHA_CODE")?><span class="mf-req">*</span></label>
		<input type="text" id="MF_CAPTCHA" name="captcha_word" maxlength="50" value="">
		<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>" alt="CAPTCHA">
	</p><?
	endif;
	?><input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
	<input type="submit" name="submit" value="<?=GetMessage("MFT_SUBMIT")?>">
</form>
</div>