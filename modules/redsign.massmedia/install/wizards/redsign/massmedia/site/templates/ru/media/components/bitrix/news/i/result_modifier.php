<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if(!CModule::IncludeModule('iblock'))die();
if(!empty($arResult["VARIABLES"]["SECTION_CODE"]) || !empty($arResult["VARIABLES"]["SECTION_ID"])){
	$rsSection = CIBlockSection::GetList(array(), array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "CODE" => $arResult["VARIABLES"]["SECTION_CODE"]), false, array("IBLOCK_ID", "ID", "NAME", "LEFT_MARGIN", "RIGHT_MARGIN", "DEPTH_LEVEL"));
	if($arSection = $rsSection->Fetch()){
		$arResult["VARIABLES"]["SECTION_ID"] = $arSection["ID"];
		$arResult["VARIABLES"]["DEPTH_LEVEL"] = $arSection["DEPTH_LEVEL"];
		$arResult["VARIABLES"]["LEFT_MARGIN"] = $arSection["LEFT_MARGIN"];
		$arResult["VARIABLES"]["RIGHT_MARGIN"] = $arSection["RIGHT_MARGIN"];
		$arResult["VARIABLES"]["SECTION_NAME"] = $arSection["NAME"];
	}
}?>
