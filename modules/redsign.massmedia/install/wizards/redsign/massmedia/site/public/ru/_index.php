<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
$APPLICATION->SetTitle("Главная страница");

global $arElementsFilterID;
$arElementsFilterID = array();

global $arSectionsFilterCode;
$arSectionsFilterCode = array();
?>
<div id="news_last" class="col_x2_left">
	<div class="col_left_in"><?
	$arElementsID=$APPLICATION->IncludeComponent("bitrix:news.list", "day", array(
	"IBLOCK_TYPE" => "articles",
	"IBLOCK_ID" => "#IBLOCK_ID_news#",
	"NEWS_COUNT" => "2",
	"DISPLAY_DISPLAY" => "1",
	"SORT_BY1" => "propertysort_ITEM_STATUS",
	"SORT_ORDER1" => "ASC",
	"SORT_BY2" => "ACTIVE_FROM",
	"SORT_ORDER2" => "DESC",
	"FILTER_NAME" => "",
	"FIELD_CODE" => array(
		0 => "",
		1 => "",
	),
	"PROPERTY_CODE" => array(
		0 => "",
		1 => "",
	),
	"CHECK_DATES" => "Y",
	"DETAIL_URL" => "#SITE_DIR#articles/#SECTION_CODE#/#ELEMENT_CODE#/",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"CACHE_FILTER" => "N",
	"CACHE_GROUPS" => "Y",
	"PREVIEW_TRUNCATE_LEN" => "",
	"ACTIVE_DATE_FORMAT" => "d.m.Y",
	"SET_TITLE" => "N",
	"SET_STATUS_404" => "N",
	"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
	"ADD_SECTIONS_CHAIN" => "N",
	"HIDE_LINK_WHEN_NO_DETAIL" => "N",
	"PARENT_SECTION" => "",
	"PARENT_SECTION_CODE" => "",
	"INCLUDE_SUBSECTIONS" => "Y",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "N",
	"PAGER_TITLE" => "Новости",
	"PAGER_SHOW_ALWAYS" => "N",
	"PAGER_TEMPLATE" => "",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	"PAGER_SHOW_ALL" => "N",
	"DISPLAY_DATE" => "N",
	"DISPLAY_NAME" => "Y",
	"DISPLAY_PICTURE" => "Y",
	"DISPLAY_PREVIEW_TEXT" => "Y",
	"DISPLAY_SECTION_NAME" => "N",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);
	$arElementsFilterID = $arElementsID;
	$arrFilter = array(
		"!ID" => $arElementsFilterID,
		"!PROPERTY_ITEM_STATUS" => "Новость дня"
	);
	$arElementsID=$APPLICATION->IncludeComponent("bitrix:news.list", "hot", array(
	"IBLOCK_TYPE" => "articles",
	"IBLOCK_ID" => "#IBLOCK_ID_news#",
	"NEWS_COUNT" => "7",
	"DISPLAY_DISPLAY" => "1",
	"SORT_BY1" => "propertysort_ITEM_STATUS",
	"SORT_ORDER1" => "ASC",
	"SORT_BY2" => "ACTIVE_FROM",
	"SORT_ORDER2" => "DESC",
	"FILTER_NAME" => "arrFilter",
	"FIELD_CODE" => array(
		0 => "",
		1 => "",
	),
	"PROPERTY_CODE" => array(
		0 => "",
		1 => "",
	),
	"CHECK_DATES" => "Y",
	"DETAIL_URL" => "#SITE_DIR#articles/#SECTION_CODE#/#ELEMENT_CODE#/",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"CACHE_FILTER" => "N",
	"CACHE_GROUPS" => "Y",
	"PREVIEW_TRUNCATE_LEN" => "",
	"ACTIVE_DATE_FORMAT" => "d.m.Y",
	"SET_TITLE" => "N",
	"SET_STATUS_404" => "N",
	"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
	"ADD_SECTIONS_CHAIN" => "N",
	"HIDE_LINK_WHEN_NO_DETAIL" => "N",
	"PARENT_SECTION" => "",
	"PARENT_SECTION_CODE" => "",
	"INCLUDE_SUBSECTIONS" => "Y",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "N",
	"PAGER_TITLE" => "Новости",
	"PAGER_SHOW_ALWAYS" => "N",
	"PAGER_TEMPLATE" => "",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	"PAGER_SHOW_ALL" => "N",
	"DISPLAY_DATE" => "Y",
	"DISPLAY_NAME" => "Y",
	"DISPLAY_PICTURE" => "N",
	"DISPLAY_PREVIEW_TEXT" => "Y",
	"DISPLAY_SECTION_NAME" => "N",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);
	$arElementsFilterID = array_merge($arElementsFilterID, $arElementsID);
	$arrFilter = array(
		"!ID" => $arElementsFilterID,
		"!PROPERTY_ITEM_STATUS" => array("Новость дня", "Горячая новость")
	);
	$arElementsID=$APPLICATION->IncludeComponent("bitrix:news.list", "vert", array(
	"IBLOCK_TYPE" => "articles",
	"IBLOCK_ID" => "#IBLOCK_ID_news#",
	"NEWS_COUNT" => "3",
	"DISPLAY_DISPLAY" => "3",
	"DISPLAY_LINE" => "3",
	"SORT_BY1" => "propertysort_ITEM_STATUS",
	"SORT_ORDER1" => "ASC",
	"SORT_BY2" => "ACTIVE_FROM",
	"SORT_ORDER2" => "DESC",
	"FILTER_NAME" => "arrFilter",
	"FIELD_CODE" => array(
		0 => "",
		1 => "",
	),
	"PROPERTY_CODE" => array(
		0 => "",
		1 => "",
	),
	"CHECK_DATES" => "Y",
	"DETAIL_URL" => "#SITE_DIR#articles/#SECTION_CODE#/#ELEMENT_CODE#/",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"CACHE_FILTER" => "N",
	"CACHE_GROUPS" => "Y",
	"PREVIEW_TRUNCATE_LEN" => "",
	"ACTIVE_DATE_FORMAT" => "d.m.Y",
	"SET_TITLE" => "N",
	"SET_STATUS_404" => "N",
	"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
	"ADD_SECTIONS_CHAIN" => "N",
	"HIDE_LINK_WHEN_NO_DETAIL" => "N",
	"PARENT_SECTION" => "",
	"PARENT_SECTION_CODE" => "",
	"INCLUDE_SUBSECTIONS" => "Y",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "N",
	"PAGER_TITLE" => "Новости",
	"PAGER_SHOW_ALWAYS" => "N",
	"PAGER_TEMPLATE" => "",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	"PAGER_SHOW_ALL" => "N",
	"DISPLAY_DATE" => "N",
	"DISPLAY_NAME" => "Y",
	"DISPLAY_PICTURE" => "Y",
	"DISPLAY_PREVIEW_TEXT" => "N",
	"DISPLAY_SECTION_NAME" => "Y",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);
	?></div>
</div>
<div class="col_x1_right">
		<div class="adv_block"><?
			$APPLICATION->IncludeFile(
					$APPLICATION->GetTemplatePath("include_areas/adv_top.php"),
					Array(),
					Array("MODE"=>"html")
				);
			?></div><?
	$arElementsFilterID = array_merge($arElementsFilterID, $arElementsID);
	$arrFilter = array("!ID" => $arElementsFilterID);

	$APPLICATION->IncludeComponent("bitrix:news.list", "vert", array(
	"IBLOCK_TYPE" => "articles",
	"IBLOCK_ID" => "#IBLOCK_ID_news#",
	"NEWS_COUNT" => "4",
	"DISPLAY_DISPLAY" => "2",
	"DISPLAY_LINE" => "1",
	"SORT_BY1" => "propertysort_ITEM_STATUS",
	"SORT_ORDER1" => "ASC",
	"SORT_BY2" => "ACTIVE_FROM",
	"SORT_ORDER2" => "DESC",
	"FILTER_NAME" => "arrFilter",
	"FIELD_CODE" => array(
		0 => "",
		1 => "",
	),
	"PROPERTY_CODE" => array(
		0 => "",
		1 => "",
	),
	"CHECK_DATES" => "Y",
	"DETAIL_URL" => "#SITE_DIR#articles/#SECTION_CODE#/#ELEMENT_CODE#/",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"CACHE_FILTER" => "N",
	"CACHE_GROUPS" => "Y",
	"PREVIEW_TRUNCATE_LEN" => "",
	"ACTIVE_DATE_FORMAT" => "d.m.Y",
	"SET_TITLE" => "N",
	"SET_STATUS_404" => "N",
	"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
	"ADD_SECTIONS_CHAIN" => "N",
	"HIDE_LINK_WHEN_NO_DETAIL" => "N",
	"PARENT_SECTION" => "",
	"PARENT_SECTION_CODE" => "policy",
	"INCLUDE_SUBSECTIONS" => "Y",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "N",
	"PAGER_TITLE" => "Новости",
	"PAGER_SHOW_ALWAYS" => "N",
	"PAGER_TEMPLATE" => "",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	"PAGER_SHOW_ALL" => "N",
	"DISPLAY_DATE" => "Y",
	"DISPLAY_NAME" => "Y",
	"DISPLAY_PICTURE" => "Y",
	"DISPLAY_PREVIEW_TEXT" => "N",
	"DISPLAY_SECTION_NAME" => "Y",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);
?></div>

<div class="col_x2_right">
<?
	$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "i", array(
	"IBLOCK_TYPE" => "articles",
	"IBLOCK_ID" => "#IBLOCK_ID_news#",
	"SECTION_ID" => "",
	"SECTION_CODE" => "",
	"NEWS_COUNT" => "5",
	"FILTER_SECTION_CODE" => array(
		0 => "economics",
		1 => "society",
		2 => "sport",
		3 => "incidents",
	),
	"FILTER_SECTION_economics_TEMPLATE" => "vert",
	"FILTER_SECTION_economics_DISPLAY" => "3",
	"FILTER_SECTION_economics_STRING" => "3",
	"FILTER_SECTION_society_TEMPLATE" => "vert",
	"FILTER_SECTION_society_DISPLAY" => "2",
	"FILTER_SECTION_society_STRING" => "2",
	"FILTER_SECTION_sport_TEMPLATE" => "vert",
	"FILTER_SECTION_sport_DISPLAY" => "3",
	"FILTER_SECTION_sport_STRING" => "3",
	"FILTER_SECTION_incidents_TEMPLATE" => "hor",
	"FILTER_SECTION_incidents_DISPLAY" => "4",
	"FILTER_SECTION_incidents_STRING" => "2",
	"COUNT_ELEMENTS" => "Y",
	"TOP_DEPTH" => "1",
	"SECTION_FIELDS" => array(
		0 => "",
		1 => "",
	),
	"SECTION_USER_FIELDS" => array(
		0 => "",
		1 => "",
	),
	"SORT_BY1" => "propertysort_ITEM_STATUS",
	"SORT_ORDER1" => "ASC",
	"SORT_BY2" => "ACTIVE_FROM",
	"SORT_ORDER2" => "DESC",
	"FILTER_NAME" => "arrFilter",
	"FIELD_CODE" => array(
		0 => "",
		1 => "",
	),
	"PROPERTY_CODE" => array(
		0 => "",
		1 => "",
	),
	"CHECK_DATES" => "Y",
	"SECTION_URL" => "#SITE_DIR#articles/#SECTION_CODE#/",
	"DETAIL_URL" => "#SITE_DIR#articles/#SECTION_CODE#/#ELEMENT_CODE#/",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"CACHE_GROUPS" => "Y",
	"ADD_SECTIONS_CHAIN" => "N",
	"HIDE_LINK_WHEN_NO_DETAIL" => "N",
	"DISPLAY_DATE" => "Y",
	"DISPLAY_NAME" => "Y",
	"DISPLAY_PICTURE" => "Y",
	"DISPLAY_PREVIEW_TEXT" => "N",
	"DISPLAY_DISPLAY" => "3"
	),
	false
);
?></div>
<div class="col_x1_left">
	<div class="col_left_in">
		<div class="adv_block"><?
		$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
				"AREA_FILE_SHOW" => "file",
				"PATH" => SITE_TEMPLATE_PATH."/include_areas/adv_left1.php"
			)
		);
		?></div><?
		$arElementsFilterID = $arElementsID;
		$arFilterTop = array(
			//"!ID" => $arElementsFilterID,
			"!PROPERTY_ITEM_TOP" => false
		);
		$APPLICATION->IncludeComponent("bitrix:news.list", "last", array(
		"IBLOCK_TYPE" => "articles",
		"IBLOCK_ID" => "#IBLOCK_ID_news#",
		"NEWS_COUNT" => "15",
		"SORT_BY1" => "propertysort_ITEM_STATUS",
		"SORT_ORDER1" => "ASC",
		"SORT_BY2" => "ACTIVE_FROM",
		"SORT_ORDER2" => "DESC",
		"FILTER_NAME" => "arFilterTop",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "N",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"DISPLAY_DISPLAY" => "2",
		"AJAX_OPTION_ADDITIONAL" => "",
		"DEFAULT_SECTION_NAME" => "Актуальные темы"
		),
		false
		);
	?></div>
</div>
<div class="col_x2_left clear">
	<div class="col_left_in"><?
		$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "i", array(
	"IBLOCK_TYPE" => "articles",
	"IBLOCK_ID" => "#IBLOCK_ID_news#",
	"SECTION_ID" => "",
	"SECTION_CODE" => "",
	"NEWS_COUNT" => "5",
	"FILTER_SECTION_CODE" => array(
		0 => "incidents",
		1 => "stars",
		2 => "science",
		3 => "auto",
		4 => "family",
		5 => "health",
	),
	"FILTER_SECTION_incidents_TEMPLATE" => "vert",
	"FILTER_SECTION_incidents_DISPLAY" => "2",
	"FILTER_SECTION_incidents_STRING" => "2",
	"FILTER_SECTION_stars_TEMPLATE" => "vert",
	"FILTER_SECTION_stars_DISPLAY" => "3",
	"FILTER_SECTION_stars_STRING" => "3",
	"FILTER_SECTION_science_TEMPLATE" => "hor",
	"FILTER_SECTION_science_DISPLAY" => "2",
	"FILTER_SECTION_science_STRING" => "2",
	"FILTER_SECTION_auto_TEMPLATE" => "vert",
	"FILTER_SECTION_auto_DISPLAY" => "4",
	"FILTER_SECTION_auto_STRING" => "2",
	"FILTER_SECTION_family_TEMPLATE" => "vert",
	"FILTER_SECTION_family_DISPLAY" => "3",
	"FILTER_SECTION_family_STRING" => "3",
	"FILTER_SECTION_health_TEMPLATE" => "vert",
	"FILTER_SECTION_health_DISPLAY" => "3",
	"FILTER_SECTION_health_STRING" => "3",
	"COUNT_ELEMENTS" => "Y",
	"TOP_DEPTH" => "1",
	"SECTION_FIELDS" => array(
		0 => "",
		1 => "",
	),
	"SECTION_USER_FIELDS" => array(
		0 => "",
		1 => "",
	),
	"SORT_BY1" => "propertysort_ITEM_STATUS",
	"SORT_ORDER1" => "ASC",
	"SORT_BY2" => "ACTIVE_FROM",
	"SORT_ORDER2" => "DESC",
	"FILTER_NAME" => "arrFilter",
	"FIELD_CODE" => array(
		0 => "",
		1 => "",
	),
	"PROPERTY_CODE" => array(
		0 => "",
		1 => "",
	),
	"CHECK_DATES" => "Y",
	"SECTION_URL" => "#SITE_DIR#articles/#SECTION_CODE#/",
	"DETAIL_URL" => "#SITE_DIR#articles/#SECTION_CODE#/#ELEMENT_CODE#/",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"CACHE_GROUPS" => "Y",
	"ADD_SECTIONS_CHAIN" => "N",
	"HIDE_LINK_WHEN_NO_DETAIL" => "N",
	"DISPLAY_DATE" => "Y",
	"DISPLAY_NAME" => "Y",
	"DISPLAY_PICTURE" => "Y",
	"DISPLAY_PREVIEW_TEXT" => "N",
	"DISPLAY_DISPLAY" => "3"
	),
	false
);
	?></div>
</div><?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>