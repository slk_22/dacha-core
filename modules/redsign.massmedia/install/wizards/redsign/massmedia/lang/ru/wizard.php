<?
$MESS["wiz_structure_data"] = 'Установить демонстрационные данные модуля Информационный портал "The City Times".';
$MESS["wiz_restructure_data"] = 'Переустановить демонстрационные данные модуля Информационный портал "The City Times".';
$MESS['wiz_site_name'] = 'Информационный портал "The City Times"';
$MESS["wiz_site_desc"] = 'Информационный портал "The City Times" на 1С-Битрикс.';
$MESS["wiz_keywords"] = "информационный портал, новости, статьи, газеты, объявления, реклама, информация ";
$MESS["wiz_company_copyright"] = "Владелец сайта:";
$MESS["wiz_name"] = 'Информационный портал "The City Times"';
$MESS["wiz_copyright"] = "Владимир Владимирович";

$MESS["WIZ_COMPANY_NAME"] = "Название сайта или компании";
$MESS["WIZ_COMPANY_NAME_DEF"] = 'Информационный портал "The City Times""';
$MESS["WIZ_COMPANY_TELEPHONE"] = "Телефон для обратной связи";
$MESS["WIZ_COMPANY_TELEPHONE_DOP"] = "Дополнительный телефон";
$MESS["WIZ_COMPANY_TELEPHONE_CODE_T"] = "Код города: ";
$MESS["WIZ_COMPANY_TELEPHONE_CODE"] = "495";
$MESS["WIZ_COMPANY_TELEPHONE_CODE2"] = "8412";
$MESS["WIZ_COMPANY_TELEPHONE_DEF_T"] = "Номер телефона:";
$MESS["WIZ_COMPANY_TELEPHONE_DEF"] = "212 85 06";
$MESS["WIZ_COMPANY_TELEPHONE_DEF2"] = "44 52 81";
$MESS["WIZ_COMPANY_SCHEDULE"] = "График работы";
$MESS["WIZ_COMPANY_SCHEDULE_DEFAULT"] = "Пон-Пятн с 8:00 до 18:00";
$MESS["WIZ_COMPANY_SMALL_ADDRESS"] = "Адрес организации";
$MESS["WIZ_COMPANY_SMALL_ADDRESS_DEFAULT"] = "г. Москва, ул. Ленинградская, д. 66";

$MESS["WIZ_STEP_SITE_SET"] = "Настройки сайта";

$MESS["WIZ_IMPORT_LOCATION_VNIMANIE"] = "Внимание!<br>Решение рекомендуется устанавливать на чистую редакцию 1С-Битрикс. При установке на готовый интернет-магазин обязательно сделайте резервную копию!";
$MESS["WIZ_IMPORT_LOCATION_DISC_DEL"] = "Внимание! Все имеющиеся в системе местоположения будут удалены.";
$MESS["WIZ_IMPORT_LOCATION_DISC_ADD"] = "На сайт будет импортирован список стран и городов (рекомендуется при выборе первого пункта).";
$MESS["WIZ_IMPORT_LOCATION_VNIMANIE2"] = "Если вы ничего не поняли, что здесь написано – рекомендуется оставить все без изменений (первый пункт без галочки, второй – с галочкой).";
?>