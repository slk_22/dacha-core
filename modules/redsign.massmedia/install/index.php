<?global $MESS;
IncludeModuleLangFile(__FILE__);

Class redsign_massmedia extends CModule
{
    var $MODULE_ID = "redsign.massmedia";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	var $MODULE_GROUP_RIGHTS = "Y";

	function redsign_massmedia()
	{
		$arModuleVersion = array();

		$path = str_replace("\\", "/", __FILE__);
		$path = substr($path, 0, strlen($path) - strlen("/index.php"));
		include($path."/version.php");

        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        } else {
            $this->MODULE_VERSION = "1.0.0";
            $this->MODULE_VERSION_DATE = "2013.01.01";
        }

		$this->MODULE_NAME = GetMessage("REDSIGN_MODULES_INSTALL_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("REDSIGN_MODULES_INSTALL_DESCRIPTION");
		$this->PARTNER_NAME = GetMessage("REDSIGN_MODULES_INSTALL_COPMPANY_NAME");
        $this->PARTNER_URI  = "http://redsign.ru/";
	}

	// Install functions
	function InstallDB()
	{
		RegisterModule("redsign.massmedia");
		return TRUE;
	}

	function InstallFiles()
	{
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/redsign.massmedia/install/modules", $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules", true, true);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/redsign.massmedia/install/components", $_SERVER["DOCUMENT_ROOT"]."/bitrix/components", true, true);
		return TRUE;
	}

	function InstallPublic()
	{
		return TRUE;
	}

	function InstallEvents()
	{
		COption::SetOptionString("fileman", "use_code_editor", "N");
		COption::SetOptionString("iblock", "combined_list_mode", "Y");
		COption::SetOptionString("iblock", "show_xml_id", "Y");
		COption::SetOptionString("main", "optimize_css_files", "N");
		COption::SetOptionString("main", "optimize_js_files", "N");
		COption::SetOptionString("main", "compres_css_js_files", "N");

		COption::SetOptionString("redsign.massmedia", "wizard_version", "1" );

		// set option for brands
		//COption::SetOptionString("redsign.massmedia", "vkapi_id", "3458889" );
		return TRUE;
	}

	// UnInstal functions
	function UnInstallDB($arParams = Array())
	{
		UnRegisterModule("redsign.massmedia");
		return TRUE;
	}

	function UnInstallFiles()
	{
		return TRUE;
	}

	function UnInstallPublic()
	{
		return TRUE;
	}

	function UnInstallEvents()
	{
		COption::RemoveOption("redsign.massmedia");
		return TRUE;
	}

    function DoInstall()
    {
		global $APPLICATION, $step;
		$keyGoodFiles = $this->InstallFiles();
		$keyGoodDB = $this->InstallDB();
		$keyGoodEvents = $this->InstallEvents();
		$keyGoodPublic = $this->InstallPublic();
		$APPLICATION->IncludeAdminFile(GetMessage("SPER_INSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/redsign.massmedia/install/install.php");
    }

    function DoUninstall()
    {
		global $APPLICATION, $step;
		$keyGoodFiles = $this->UnInstallFiles();
		$keyGoodEvents = $this->UnInstallEvents();
		$keyGoodDB = $this->UnInstallDB();
		$keyGoodPublic = $this->UnInstallPublic();
		$APPLICATION->IncludeAdminFile(GetMessage("SPER_UNINSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/redsign.massmedia/install/uninstall.php");
    }
}
?>