<?
/**
 * Company developer: ALTASIB
 * Developer: adumnov
 * Site: http://www.altasib.ru
 * E-mail: dev@altasib.ru
 * @copyright (c) 2006-2015 ALTASIB
 */

IncludeModuleLangFile(__FILE__);
Class CAltasibGeoBaseSelected extends CAltasibGeoBaseAllSelected
{
	const UF_OBJECT_ENTITY_ID = "ALTASIB_GEOBASE";
	const UF_FIELD_URL = "UF_ALX_GB_URL";

	function UpdateCityRows()
	{
		global $USER_FIELD_MANAGER;

		$resCities = CAltasibGeoBaseAllSelected::GetMoreCities(true);
		while($arCities = $resCities->Fetch())
		{
			if(empty($arCities["R_FNAME"]))
			{
				$arRG = CAltasibGeoBase::GetRegionLang($arCities["CTR_CODE"], $arCities['R_ID']);
				if(!empty($arRG['region_name']))
				{
					if (LANG_CHARSET == 'windows-1251')
						$arRG['region_name'] = iconv("UTF-8", "windows-1251", $arRG['region_name']);

					$arCities["R_FNAME"] = $arRG['region_name'];
				}
			}

			$ent_id = $arCities['ID'];
			$arUserFields = $USER_FIELD_MANAGER->GetUserFields(self::UF_OBJECT_ENTITY_ID, $ent_id, LANGUAGE_ID);

			$strUFtable = '';

			if(!empty($arUserFields) && is_array($arUserFields))
			{
				foreach($arUserFields as $aField)
				{
					if(!empty($aField["VALUE"]) || (!is_array($aField["VALUE"]) && strlen((string)$aField["VALUE"])>0))
					{
						$strUFname = ($aField["LIST_COLUMN_LABEL"] ? $aField["LIST_COLUMN_LABEL"]: $aField["FIELD_NAME"]);

						if(is_array($aField["VALUE"]))
						{
							$iUFcount = count($aField["VALUE"]);
							if(!$iUFcount)
								$iUFcount = 1;
							$strUFtable .= '<tr>
								<td rowspan="'.$iUFcount.'" class="adm-detail-content-cell-l">'.$strUFname.'</td>
								<td class="adm-detail-content-cell-r" width="50%">'.htmlspecialcharsEx($aField["VALUE"][0]).'</td>
							</tr>';

							foreach($aField["VALUE"] as $kn=>$val)
							{
								if($kn == 0)
									continue;
								$strUFtable .= '<tr>
									<td class="adm-detail-content-cell-r" width="50%">'
										.htmlspecialcharsEx($val)
									.'</td>
								</tr>';
							}
						}
						else
						{
							$strUFtable .= '<tr>
								<td class="adm-detail-content-cell-l">'.$strUFname.'</td>
								<td class="adm-detail-content-cell-r" width="50%">'.htmlspecialcharsEx($aField["VALUE"]).'</td>
							</tr>';
						}
					}
				}

				if(!empty($strUFtable))
					$strUFtable = '<table cellspacing="0" cellpadding="10" width="100%" border="0" align="center">'
						.$strUFtable.'</table>';
			}

			echo '<tr class="altasib_geobase_city_line">'
				.'<td>'.htmlspecialcharsEx($arCities["ID"]).'</td>'
				.'<td>'.htmlspecialcharsEx($arCities["C_SOCR"]).'</td>'
				.'<td width="16%">'.htmlspecialcharsEx($arCities["C_NAME"])
				.'<td width="16%">'.(!empty($arCities["C_NAME_EN"]) ? htmlspecialcharsEx($arCities["C_NAME_EN"]) : '').'</td>'
				.'<td>'.$arCities["C_CODE"].'</td>'
				.'<td>'.(htmlspecialcharsEx(!empty($arCities['D_NAME']) ? $arCities['D_NAME'].' '.$arCities['D_SOCR'] : $arCities['ID_DISTRICT'])).'</td>'
				.'<td>'.htmlspecialcharsEx($arCities["R_FNAME"]).'</td>'
				.'<td>'.htmlspecialcharsEx($arCities["CTR_CODE"]).'</td>'
				.'<td>'.htmlspecialcharsEx($arCities["CTR_NAME_RU"]).'</td>'
				.'<td>'.$strUFtable
				.'<input class="altasib_gb_uf_edit" name="altasib_geobase_uf_'.$arCities['ID'].'" onclick="altasib_geobase_uf('.$ent_id.');return false;" value="'.(empty($strUFtable) ? GetMessage("ALTASIB_TABLE_ADD") : GetMessage("ALTASIB_TABLE_EDIT")).'"></td>'
				.'<td><input type="submit" name="altasib_geobase_del_'.$arCities['ID'].'" value="'.GetMessage("ALTASIB_TABLE_CITY_DELETE").'" onclick="altasib_geobase_delete_click('.$arCities['ID'].');return false;"></td>'
			.'</tr>';
		}
	}

	function BeforeAddCity($cityId)
	{
		$arData = CAltasibGeoBase::GetInfoKladrByCode($cityId);
		if (!$arData)
			return false;
		$arField = array(
			'ACTIVE' => 'Y',
			'SORT' => 500,
			'NAME' => $arData['CITY']['NAME'],
			'NAME_EN' => "",
			'CODE' => $arData['CODE'],
			'ID_DISTRICT' => $arData['CITY']['ID_DISTRICT'],
			'ID_REGION' => $arData['REGION']['CODE'],
			'COUNTRY_CODE' => "RU",
			'SOCR' => $arData['CITY']['SOCR']
		);

		return(CAltasibGeoBaseAllSelected::AddCity($arField));
	}

	function BeforeAddRegion($regId)
	{
		$arData = CAltasibGeoBase::GetRegionByCode($regId)->Fetch();
		if (!$arData)
			return false;
		$arField = array(
			'ACTIVE' => 'Y',
			'SORT' => 500,
			'NAME' => $arData['R_NAME'],
			'NAME_EN' => "",
			'CODE' => $arData['R_CODE'],
			'ID_DISTRICT' => $arData['R_CODE']."000",
			'ID_REGION' => $arData['R_CODE'],
			'COUNTRY_CODE' => "RU",
			'SOCR' => $arData['R_SOCR']
		);

		return(CAltasibGeoBaseAllSelected::AddCity($arField));
	}

	function BeforeAddMMCity($cityId)
	{
		$arData = CAltasibGeoBase::GetDataMMByID($cityId)->Fetch();
		if (!$arData)
			return false;
		$arField = array(
			'ACTIVE' => 'Y',
			'SORT' => 500,
			'NAME' => $arData['CITY_RU'],
			'NAME_EN' => $arData['CITY_EN'], // new
			'CODE' => $arData['CITY_ID'],
			'ID_DISTRICT' => "",
			'ID_REGION' => $arData['REGION_ID'],
			'COUNTRY_CODE' => $arData['COUNTRY_CODE'],
			'SOCR' => ""
		);

		return(CAltasibGeoBaseAllSelected::AddCity($arField));
	}

	function CheckFields(&$arFields)
	{
		if (is_set($arFields, "NAME") && strlen($arFields["NAME"]) <= 0)
			return false;
		if (is_set($arFields, "CODE") && strlen($arFields["CODE"]) <= 0)
			return false;
		if (is_set($arFields, "ID_REGION") && strlen($arFields["ID_REGION"]) <= 0)
			return false;

		return true;
	}

	function GetCityByID($ID, $afields, $active = false)
	{
		global $DB;
		$ID = IntVal($ID);
		if ($ID<=0) return false;

		if(empty($afields) || !is_array($afields))
			$strFields = "*";

		$strSql =
		"SELECT ".(!empty($strFields) ? $strFields : implode(',', $afields))." FROM altasib_geobase_selected ".
		"WHERE ID = ".$ID
		.($active != false ? 'AND ACTIVE = "Y"' : '')
		." ORDER BY ID";

		$db_res = $DB->Query($strSql, false, "File: ".__FILE__."<br />Line: ".__LINE__);

		if ($res = $db_res->Fetch())
		{
			return $res;
		}
		return false;
	}

	function GetAllCities($afields, $active = false)
	{
		global $DB;
		$strSql =
		"SELECT ".implode(',', $afields)." FROM altasib_geobase_selected "
		.($active != false ? 'WHERE ACTIVE = "Y"' : '')
		." ORDER BY `ID`, `SORT`";

		$db_res = $DB->Query($strSql, false, "File: ".__FILE__."<br />Line: ".__LINE__);

		return $db_res;
	}

	/**
	 * form constructor
	 */
	public static function GetTabs()
	{
		if(intval($_REQUEST['ID'])>0){

			$entID = intval($_REQUEST['ID']);

			global $USER_FIELD_MANAGER;

			if(
				(count($USER_FIELD_MANAGER->GetUserFields(self::UF_OBJECT_ENTITY_ID)) > 0) ||
				($USER_FIELD_MANAGER->GetRights(self::UF_OBJECT_ENTITY_ID) >= "W")
			)
			{
				$USER_FIELD_MANAGER->EditFormShowTab(self::UF_OBJECT_ENTITY_ID, false, $entID);
			}
		}
	}

	/**
	 * Saving values of the form
	 */
	public static function SetValues($ID)
	{
		if(intval($ID) > 0)
		{
			global $USER_FIELD_MANAGER;

			$arFields = array();

			$USER_FIELD_MANAGER->EditFormAddFields(self::UF_OBJECT_ENTITY_ID, $arFields);

			CUtil::decodeURIComponent($arFields);

			if(!empty($arFields))
			{
				$USER_FIELD_MANAGER->Update(self::UF_OBJECT_ENTITY_ID, intval($ID), $arFields);
			}
		}
	}
	/**
	 * The output value of the custom property by ID
	 */
	public static function GetFieldsCity($ID, $lang)
	{
		$ID = intval($ID);
		$arUFields = array();
		if(isset($ID))
		{
			global $USER_FIELD_MANAGER;
			$arUFields = $USER_FIELD_MANAGER->GetUserFields(self::UF_OBJECT_ENTITY_ID, $ID, $lang);
		}
		return $arUFields;
	}

	protected static function HandlerUrl($url)
	{
		$url = ToLower(trim($url));
		if(strpos($url,"http://") === false)
		{
			$url = "http://".$url;
		}
		return $url;
	}

	function GetCurrentCityFromSelected()
	{
		global $APPLICATION;
		if(!empty($_SESSION["ALTASIB_GEOBASE_CODE"]))
		{
			$arResult['USER_CHOICE'] = $_SESSION["ALTASIB_GEOBASE_CODE"];
		}
		else
		{
			// Cookies
			$arDataC = CAltasibGeoBase::deCodeJSON($APPLICATION->get_cookie("ALTASIB_GEOBASE_CODE"));
			if(is_array($arDataC) && !empty($arDataC))
				$arResult['USER_CHOICE'] = $arDataC;
		}
		if(empty($arResult['USER_CHOICE']))
		{
			$arDataO = CAltasibGeoBase::GetCodeByAddr(); // On-line auto detection

			if($arDataO["CITY"]["NAME"] != GetMessage('ALTASIB_GEOBASE_KLADR_CITY_NAME'))
			{
				$arResult['AUTODETECT'] = $arDataO;
			}
		}

		$arCities = CAltasibGeoBaseSelected::GetMoreCacheCities();

		$arSelCity = array();

		foreach($arCities as $arCity)
		{
			if($arResult['USER_CHOICE']["CODE"] == $arCity['C_CODE'])
			{
				$arSelCity = $arCity;
			}
			elseif($arResult['USER_CHOICE']["C_CODE"] == $arCity['C_CODE'])
			{
				$arSelCity = $arCity;
			}
			elseif($arResult['USER_CHOICE']["REGION"]["CODE"] == $arCity['C_CODE'])
			{
				$arSelCity = $arCity;
			}
			elseif($arResult['USER_CHOICE']["CITY_RU"] == $arCity['C_NAME'])
			{
				$arSelCity = $arCity;
			}
			elseif(!empty($arResult['USER_CHOICE']["CITY_EN"]) && strlen($arResult['USER_CHOICE']["CITY_EN"]) > 0
				&& !empty($arCity['C_NAME_EN']) && strlen($arCity['C_NAME_EN']) > 0
				&& $arResult['USER_CHOICE']["CITY_EN"] == $arCity['C_NAME_EN'])
			{
				$arSelCity = $arCity;
			}
			elseif($arResult['AUTODETECT']["CODE"] == $arCity['C_CODE'])
			{
				$arSelCity = $arCity;
			}
		}
		return $arSelCity;
	}

	function GetUFValue($value_id, $field_id = false)
	{
		global $USER_FIELD_MANAGER;

		if($field_id)
			return $USER_FIELD_MANAGER->GetUserFieldValue(self::UF_OBJECT_ENTITY_ID, $field_id, $value_id, false);
		else // URL as default
			return $USER_FIELD_MANAGER->GetUserFieldValue(self::UF_OBJECT_ENTITY_ID, self::UF_FIELD_URL, $value_id, false);
	}
}
?>