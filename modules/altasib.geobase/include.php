<?
/**
 * Company developer: ALTASIB
 * Developer: adumnov
 * Site: http://www.altasib.ru
 * E-mail: dev@altasib.ru
 * @copyright (c) 2006-2016 ALTASIB
 */

global $DBType;
IncludeModuleLangFile(__FILE__);

$arClassesList = array(
	"CAltasibGeoBaseTools" => "classes/general/geobase.php",
	"CAltasibGeoBaseSelected" => "classes/general/selected.php",
	"CAltasibGeoBaseIPTools" => "classes/general/iptools.php",
	"CAltasibGeoBaseMobile_Detect" => "classes/general/Mobile_Detect.php",
	"CAltasibGeoBaseImport" => "classes/".$DBType."/import.php"
);

Class CAltasibGeoBase
{
	const CITY_INC = "/upload/altasib/geobase/src/geoipcity.inc";
	const REG_VARS = "/upload/altasib/geobase/src/geoipregionvars.php";
	const GEO_LITE = "/upload/altasib/geobase/GeoLiteCity.dat";
	const MID = "altasib.geobase";

	function __construct($num1)
	{
		$this->RegionName = $num1;
	}

	function GetAddres($ip = "")
	{
		if(defined("NO_GEOBASE") && NO_GEOBASE === true)
			return false;

		global $APPLICATION;
		if($ip == "")
		{
			if(!is_array($_SESSION["ALTASIB_GEOBASE"]))
			{
				$ip = CAltasibGeoBaseIP::getUserHostIP();

				if(COption::GetOptionString(self::MID, "set_cookie", "Y") == "Y")
				{
					$last_ip = $APPLICATION->get_cookie("ALTASIB_LAST_IP");
					$strData = $APPLICATION->get_cookie("ALTASIB_GEOBASE");
				}
				if(($ip == $last_ip) && $strData && count(CAltasibGeoBase::deCodeJSON($strData)) > 0)
				{
					$arData = CAltasibGeoBase::deCodeJSON($strData);
				}
				else
				{
					$arData = CAltasibGeoBase::GetData($ip); //local_db, statistic - true
					if(!$arData)
					{
						if(COption::GetOptionString(self::MID, "ipgeobase_enable", "Y") != "N")
						{
							$arData = CAltasibGeoBaseIP::GetGeoDataIpgeobase_ru($ip);
						}
					}
					if(!$arData)
					{
						if(COption::GetOptionString(self::MID, "geoip_enable", "Y") != "N")
						{
							$arData = CAltasibGeoBaseIP::GetGeoDataGeoip_Elib_ru($ip);
						}
					}
					if(empty($arData["CITY_NAME"]))
					{
						$arDataMM = CAltasibGeoBase::GetMaxmindData($ip);
						if(!empty($arDataMM["COUNTRY_CODE"]))
							$arData = $arDataMM;
					}

					if(COption::GetOptionString(self::MID, "set_cookie", "Y") == "Y")
					{
						$strData = CAltasibGeoBase::CodeJSON($arData);
						$APPLICATION->set_cookie("ALTASIB_LAST_IP", $ip, time() + 31104000); //60*60*24*30*12
						$APPLICATION->set_cookie("ALTASIB_GEOBASE", $strData, time() + 31104000); //60*60*24*30*12
					}

					$events = GetModuleEvents(self::MID, "OnAfterAutoDetectCity");
					while($arEvent = $events->Fetch()){
						ExecuteModuleEvent($arEvent, array($arData));
					}
				}
				$_SESSION["ALTASIB_GEOBASE"] = $arData;
			}
		}
		else
		{
			$arData = CAltasibGeoBase::GetData($ip);
			if(!$arData)
			{
				if(COption::GetOptionString(self::MID, "ipgeobase_enable", "Y") != "N")
				{
					$arData = CAltasibGeoBaseIP::GetGeoDataIpgeobase_ru($ip);
				}

				if(!$arData)
				{
					if(COption::GetOptionString(self::MID, "geoip_enable", "Y") != "N")
					{
						$arData = CAltasibGeoBaseIP::GetGeoDataGeoip_Elib_ru($ip);
					}
				}
				if(!$arData)
					return false;
			}
			return $arData;
		}
		return $_SESSION["ALTASIB_GEOBASE"];
	}

	function CodeJSON($data)
	{
		$jsonData = CUtil::PhpToJSObject($data);
		if(SITE_CHARSET !== "UTF-8")
			$jsonData = iconv(SITE_CHARSET, "UTF-8", $jsonData);
		$jsonData = str_replace("'", '"', $jsonData);
		return $jsonData;
	}
	function deCodeJSON($data)
	{
		$resData = (array)json_decode($data, true);
		if(SITE_CHARSET !== "UTF-8")
			$resData = CAltasibGeoBase::iconvArrUtfTo1251($resData);

		return $resData;
	}

	function iconvArrUtfTo1251($arr)
	{
		if(is_array($arr))
		{
			foreach($arr as $key=>$Prop)
			{
				if(is_array($Prop))
					$arProp[$key] = CAltasibGeoBase::iconvArrUtfTo1251($Prop);
				else
					$arProp[$key] = iconv('UTF-8', 'WINDOWS-1251', $Prop);
			}
		}
		else
			$arProp = iconv('UTF-8', 'WINDOWS-1251', $arr);

		return $arProp;
	}

	function OnPrologHandler()
	{
		global $APPLICATION;
		if(!IsModuleInstalled(self::MID))
			return false;

		if(defined("ADMIN_SECTION") && ADMIN_SECTION === true)
			return false;

		if(defined("NO_GEOBASE") && NO_GEOBASE === true)
			return false;

		$up_link = trim(COption::GetOptionString(self::MID, "section_link", "/personal/order/make/"));
		if($up_link != "")
		{
			$dir = $APPLICATION->GetCurDir();

			if(substr($dir, 0, 8) == "/bitrix/")
				return false;

			$link_arr = explode(",", $up_link);
			if(!is_array($link_arr))
				return false;

			foreach($link_arr as $vol)
			{
				$vol = trim($vol);
				if($vol == "")
					continue;
				if(substr($vol, 0, 1) != "/")
					$vol = "/".$vol;

				$dir_substr = substr($dir, 0, strlen($vol));
				if($dir_substr == $vol)
				{
					CAltasibGeoBase::addScriptsOnSite();
					return true;
				}
			}
			return false;
		}
		else{
			CAltasibGeoBase::addScriptsOnSite();
			return true;
		}
	}

	function addScriptsOnSite()
	{
		global $APPLICATION;
		if(ADMIN_SECTION !== true && COption::GetOptionString(self::MID, "enable_jquery", "ON") == "ON")
			CJSCore::Init(array('jquery'));

		$APPLICATION->AddHeadScript("/bitrix/js/main/core/core.min.js", true);
		$APPLICATION->AddHeadScript("/bitrix/js/altasib/geobase/script.js", true);

		$def_loc = "";
		if(($strLoc = COption::GetOptionString(self::MID, "def_location", "")) != ""
			&& CModule::IncludeModule("sale"))
		{
			$arCtry = CSaleLocation::GetCountryLangByID($strLoc, LANGUAGE_ID);
			$def_loc = "altasib_geobase.def_location='".htmlspecialcharsbx(trim($arCtry["NAME"]))."';";
		}
		$bxLoc = "";
		$intLoc = CAltasibGeoBase::GetBXLocations();
		if(!empty($intLoc))
			$bxLoc = "altasib_geobase.bx_loc='".intval($intLoc)."';";

		if(CModule::IncludeModule('sale'))
		{
			$rsPType = CSalePersonType::GetList(Array("SORT" => "ASC"), Array("ACTIVE"=>'Y', "LID"=>SITE_ID));

			$arLocs = array();
			$arLocVals = array();
			$strLocTempl = COption::GetOptionString(self::MID, "location_template", "ORDER_PROP_");
			while($arPType = $rsPType->Fetch())
			{
				$fieldPT = "field_loc_".$arPType["ID"];
				$arLocs[] = $arPType["ID"];

				$strLocVals = COption::GetOptionString(self::MID, $fieldPT, "");
				if(!empty($strLocVals))
					$arLocVals[$arPType["ID"]] = $strLocTempl.$strLocVals;
			}
			if(!empty($arLocs))
			{
				$strPT = "altasib_geobase.pt=[";
				foreach($arLocs as $i => $value)
				{
					$strPT .= "'".htmlspecialcharsbx(trim($value))."'".($value == end($arLocs) ? "" : ",");
				}
				$strPT .= "];";
			}
			if(!empty($arLocVals))
			{
				$strPTvals = "altasib_geobase.pt_vals={";
				foreach($arLocVals as $k => $val)
				{
					$strPTvals .= "'".htmlspecialcharsbx(trim($k))."':'"
						.htmlspecialcharsbx(trim($val))."'".($val == end($arLocVals) ? "" : ",");
				}
				$strPTvals .= "};";
			}

			$arLocDefaults = array();
			$db_props = CSaleOrderProps::GetList(array("SORT" => "ASC"), array("IS_LOCATION"=>'Y'));
			while($arProps = $db_props->Fetch())
			{
				$arLocDefaults[] = $arProps["DEFAULT_VALUE"];
			}
			if(!empty($arLocDefaults))
			{
				$strPV = "altasib_geobase.pv_default=[".(implode(',' ,$arLocDefaults))."];";
			}
		}

		if(empty($strPT))
		{
			$strPT = "altasib_geobase.pt=['1','2'];";
		}
		if(empty($strPTvals))
		{
			$strPTvals = "altasib_geobase.pt_vals={"
				."'1':'".htmlspecialcharsbx(trim(COption::GetOptionString(self::MID, "field_loc_ind", "ORDER_PROP_2")))."',"
				."'2':'".htmlspecialcharsbx(trim(COption::GetOptionString(self::MID, "field_loc_leg", "ORDER_PROP_3")))."'};";
		}

		$script_vars = "<script>if(typeof altasib_geobase=='undefined')"
			."var altasib_geobase={};"
			.$strPT
			.$strPTvals
			."altasib_geobase.country='".GetMessage("ALTASIB_GEOBASE_RUSSIA")."';"
			."altasib_geobase.COOKIE_PREFIX='".COption::GetOptionString("main", "cookie_name", "BITRIX_SM")."';"
			."altasib_geobase.bitrix_sessid='".bitrix_sessid()."';"
			."altasib_geobase.SITE_ID='".SITE_ID."';"
			.$def_loc.$bxLoc.$strPV."</script>";
		$APPLICATION->AddHeadString($script_vars, true);
	}

	function OnBeforeUserAddHandler(&$arFields)
	{
		global $APPLICATION;

		if(defined("NO_GEOBASE") && NO_GEOBASE === true)
			return;

		//Cookies
		$arDataC = CAltasibGeoBase::deCodeJSON($APPLICATION->get_cookie("ALTASIB_GEOBASE_CODE"));

		if(!empty($_SESSION["ALTASIB_GEOBASE_CODE"]) || !empty($arDataC)){
			$arDataKLADR = CAltasibGeoBase::GetDataKladr();
		}
		else
		{
			$arData = CAltasibGeoBase::GetCodeByAddr();
			if($arData)
			{
				if($arData["CITY"]["NAME"] != GetMessage('ALTASIB_GEOBASE_KLADR_CITY_NAME'))
					$arDataKLADR = $arData;
			}
			else
				$arIPLocat = CAltasibGeoBase::GetAddres();
		}

		if($arIPLocat)
		{
			$strCountry = "";
			if(!empty($arIPLocat["COUNTRY"]))
				$strCountry = $arIPLocat["COUNTRY"];
			elseif(!empty($arIPLocat["COUNTRY_CODE"]))
			{
				if($arIPLocat["COUNTRY_CODE"] == "RU")
					$strCountry = GetMessage("ALTASIB_GEOBASE_RUSSIA");
			}
			$arCountries = GetCountryArray();
			foreach($arCountries['reference'] as $id => $country){
				if($strCountry == $country)
					$countryId = $arCountries['reference_id'][$id];
			}

			if(!empty($countryId))
				$arFields['PERSONAL_COUNTRY'] = $countryId;

			if(!empty($arIPLocat["REGION_NAME"]) && strlen($arIPLocat["REGION_NAME"])>0)
				$arFields['PERSONAL_STATE'] = $arIPLocat["REGION_NAME"];

			if(!empty($arIPLocat["CITY_NAME"]))
				$arFields['PERSONAL_CITY'] = $arIPLocat["CITY_NAME"];
		}
		elseif($arDataKLADR)
		{
			$strCountry = "";
			if(empty($arDataKLADR["COUNTRY"]))
				$strCountry = GetMessage("ALTASIB_GEOBASE_RUSSIA");
			else
				$strCountry = $arDataKLADR["COUNTRY"];

			$arCountries = GetCountryArray();
			foreach($arCountries["reference"] as $id => $country){
				if($strCountry == $country)
					$countryId = $arCountries["reference_id"][$id];
			}

			if(!empty($countryId))
				$arFields['PERSONAL_COUNTRY'] = $countryId;

			if(!empty($arDataKLADR["REGION"]["NAME"]) && strlen($arDataKLADR["REGION"]["NAME"])>0)
				$arFields['PERSONAL_STATE'] = $arDataKLADR["REGION"]["NAME"]." ".$arDataKLADR["REGION"]["SOCR"];

			if(!empty($arDataKLADR["CITY"]["NAME"]))
				$arFields['PERSONAL_CITY'] = $arDataKLADR["CITY"]["NAME"];
		}
		return true;
	}

	function GetData($ip)
	{
		$source = COption::GetOptionString(self::MID, "source", "local_db");
		if($source == "not_using")
			return false;
		elseif($source == "local_db")
			return CAltasibGeoBase::GetGeoData($ip);
		elseif($source == "statistic")
			return CAltasibGeoBase::GetStatisticData();
		elseif($source == "maxmind")
			return CAltasibGeoBase::GetMaxmindData($ip);
		elseif($source == "ipgb_mm"){
			$arData = CAltasibGeoBase::GetGeoData($ip);
			if(empty($arData["CITY_NAME"]))
				$arDataMM = CAltasibGeoBase::GetMaxmindData($ip);
			if(!empty($arDataMM["COUNTRY_CODE"]))
				$arData = $arDataMM;
			return $arData;
		}
	}

	function GetMaxmindData($ip)
	{
		if(file_exists($_SERVER["DOCUMENT_ROOT"].self::CITY_INC))
			require_once($_SERVER["DOCUMENT_ROOT"].self::CITY_INC);
		if(file_exists($_SERVER["DOCUMENT_ROOT"].self::REG_VARS))
			require_once($_SERVER["DOCUMENT_ROOT"].self::REG_VARS);

		if(file_exists($_SERVER["DOCUMENT_ROOT"].self::GEO_LITE))
			$gi = GeoIP_open($_SERVER["DOCUMENT_ROOT"].self::GEO_LITE, GEOIP_STANDARD);
		else
			return;

		$record = GeoIP_record_by_addr($gi, $ip); //Get data from the database
		$record = (array)$record; //Display data on the screen
		GeoIP_close($gi); //Close the database connection

		$arData = array(
			"COUNTRY_CODE" => $record["country_code"],
			"COUNTRY_CODE3" => $record["country_code3"],
			"COUNTRY_NAME" => $record["country_name"],
			"REGION_CODE" => $record["region"],
			"REGION_NAME" => $GEOIP_REGION_NAME[$record["country_code"]][$record["region"]],
			"CITY_NAME" => $record["city"],
			"POSTINDEX" => $record["postal_code"],
			"CONTINENT_CODE" => $record["continent_code"],
			"latitude" => $record["latitude"],
			"longitude" => $record["longitude"]
		);
		return $arData;
	}

	function GetGeoData($ip)
	{
		global $DB;
		if($DB->TableExists('altasib_geobase_codeip') && $DB->TableExists('altasib_geobase_cities'))
		{
			$arIP	= explode('.', $ip);
			$codeIP = $arIP[0]*pow(256, 3) + $arIP[1]*pow(256, 2) + $arIP[2]*256 + $arIP[/*2*/3];

			$data = $DB->Query("SELECT * FROM altasib_geobase_codeip
				INNER JOIN altasib_geobase_cities ON altasib_geobase_codeip.CITY_ID = altasib_geobase_cities.ID
				WHERE altasib_geobase_codeip.BLOCK_BEGIN <= $codeIP AND $codeIP <= altasib_geobase_codeip.BLOCK_END"
			);
			$arData = $data->Fetch();
		} else
			$arData = array();

		return $arData;
	}

	function GetIsUpdateDataFile($Host, $Path, $File, $localFilename)
	{
		$Response = "N";
		if(file_exists($localFilename)){
			$res = @fsockopen($Host, 80, $errno, $errstr, 3);

			if($res){
				$strRequest = "HEAD ".$Path.$File." HTTP/1.1\r\n";
				$strRequest.= "Host: ".$Host."\r\n";
				$strRequest.= "\r\n";

				fputs($res, $strRequest);
				while($line = fgets($res, 4096)){
					if(@preg_match("/Content-Length: *([0-9]+)/i", $line, $regs)){
						if(@filesize($localFilename) != trim($regs[1])){
							$Response = true;
						} else {
							$Response = false;
						}
						break;
					}
				}
				fclose($res);
			}
		} else
			$Response = true;
		return $Response;
	}
	function SetUpdateAgent()
	{
		if(COption::GetOptionString(self::MID, "get_update", "N") != "Y")
			return "CAltasibGeoBase::SetUpdateAgent();";

		define('LOAD_HOST', 'ipgeobase.ru');
		define('LOAD_PATH', '/files/db/Main/');
		define('LOAD_FILE', 'geo_files.tar.gz');
		if(!CAltasibGeoBaseIP::CheckServiceAccess("http://".LOAD_HOST.LOAD_PATH.LOAD_FILE))
			return "CAltasibGeoBase::SetUpdateAgent();";

		$strFilename = $_SERVER["DOCUMENT_ROOT"]."/upload/altasib/geoip/".basename('http://'.LOAD_HOST.LOAD_PATH.LOAD_FILE);

		if(CAltasibGeoBase::GetIsUpdateDataFile(LOAD_HOST, LOAD_PATH, LOAD_FILE, $strFilename)){
			include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/admin_notify.php");
			CAdminNotify::Add(
				array(
					"MESSAGE"		=> date('d.m.Y').GetMessage("ALTASIB_GEOBASE_AVAILABLE"),
					"TAG"			=> "GEOBASE_DB_UPDATE_".date('d.m.Y'),
					"MODULE_ID"		=> self::MID,
					"ENABLE_CLOSE"	=> "Y"
				)
			);
		}
		return "CAltasibGeoBase::SetUpdateAgent();";
	}
	function GetStatisticData()
	{
		if(CModule::IncludeModule("statistic")){
			$obCity = new CCity();
			$arCity = $obCity->GetFullInfo();

			$arResCity = array(
				"BLOCK_ADDR" => $arCity["IP_ADDR"]["VALUE"],
				"COUNTRY_CODE" => $arCity["COUNTRY_CODE"]["VALUE"],
				"COUNTRY" => $arCity["COUNTRY_NAME"]["VALUE"],
				"REGION_NAME" => $arCity["REGION_NAME"]["VALUE"],
				"CITY_NAME" => $arCity["CITY_NAME"]["VALUE"]
			);
			return $arResCity;
		}
		else
			return false;
	}
	function GetCitySuggest() //altasib_geobase_get.php
	{
		if($_SERVER["REQUEST_METHOD"] != "POST")
			return false;

		if(defined("NO_GEOBASE") && NO_GEOBASE === true)
			return;

		if(!check_bitrix_sessid('sessid') && !IsIE())
			return false;

		CUtil::JSPostUnescape();

		$res = false;
		if(!isset($_POST['city_name']) && !isset($_POST['save']))
		{
			return false;
		}
		elseif(empty($_POST['city_name']) && empty($_POST['save']))
			die('pusto');
		elseif(isset($_POST['city_name']) && empty($_POST['save']) && empty($_POST['set_loc']))
		{
			return CAltasibGeoBase::CitySearch(false);
		}
		elseif(isset($_POST['save']) && $_POST['save'] == 'Y')
		{
			if(!empty($_POST['city_id']))
			{
				global $DB;
				$city_id = $DB->ForSql($_POST['city_id']);
				$reg_id = $DB->ForSql($_POST['REGION_CODE']);
				if(!empty($reg_id))
					$res = CAltasibGeoBase::SetCodeKladr($city_id, $reg_id);
				else
					$res = CAltasibGeoBase::SetCodeKladr($city_id);
			}
			elseif(isset($_POST['CITY_NAME']) && isset($_POST['COUNTRY_CODE']))
			{
				global $DB;
				$city_name = $DB->ForSql($_POST['CITY_NAME']);
				$ctry_code = $DB->ForSql($_POST['COUNTRY_CODE']);
				$region_code = $DB->ForSql($_POST['REGION_CODE']);
				if($ctry_code == 'RU')
				{
					$rsCity = CAltasibGeoBase::GetCityByNameReg(trim(htmlspecialcharsEx($city_name)),
						array('ID', 'NAME', 'CODE'), '', false, false);
					if($arCity = $rsCity->Fetch())
					{
						$res = CAltasibGeoBase::SetCodeKladr($arCity["CODE"]);
					}
				}

				if(!$res)
				{
					$res = CAltasibGeoBase::SetCodeMM($city_name, $ctry_code, $region_code);
				}
			}

			if($res && COption::GetOptionString(self::MID, 'redirect_enable', 'Y') == "Y")
			{
				$strCityURL = "";
				$arSelCity = CAltasibGeoBaseSelected::GetCurrentCityFromSelected();
				if(!empty($arSelCity) && count($arSelCity) > 0)
				{
					$strCityURL = CAltasibGeoBaseSelected::GetUFValue($arSelCity["ID"]);
				}

				if(empty($strCityURL))
				{
					$strCityURL = "#reload";
				}
				return $res.";".$strCityURL;
			}

			return $res;
		}
	}
	function GetSelectCity() //handler for select_city.php
	{
		if(isset($_POST['show_select']) && $_POST['show_select'] == 'Y')
		{
			$Templ = explode(",", COption::GetOptionString(self::MID, "select_city_templates"));

			$arPars = array();
			$arPars["REGION_DISABLE"] = COption::GetOptionString(self::MID, 'region_disable', 'N');
			$arPars["POPUP_BACK"] = COption::GetOptionString(self::MID, 'popup_back', 'Y');

			global $APPLICATION;
			$APPLICATION->IncludeComponent(
				'altasib:geobase.select.city',
				(empty($Templ[0]) ? "" : $Templ[0]),
				$arPars,
				false
			);
		}
	}
	function GetYourCity() //handler for your_city.php
	{
		$res = false;
		if(isset($_POST['locate']) && $_POST['locate'] == 'Y')
		{
			if(!is_array($_SESSION["ALTASIB_GEOBASE_CODE"]))
			{
				global $APPLICATION;
				$TemplYC = explode(",", COption::GetOptionString(self::MID, "your_city_templates"));

				if(COption::GetOptionString(self::MID, "set_cookie", "Y") == "Y")
				{
					$strData = $APPLICATION->get_cookie("ALTASIB_GEOBASE_CODE");
				}
				if(empty($strData)){
					$arPars = array();
					$arPars["REGION_DISABLE"] = COption::GetOptionString(self::MID, 'region_disable', 'N');
					$arPars["POPUP_BACK"] = COption::GetOptionString(self::MID, 'popup_back', 'Y');

					$APPLICATION->IncludeComponent(
						"altasib:geobase.your.city",
						(empty($TemplYC[0]) ? "" : $TemplYC[0]),
						$arPars,
						false
					);
				}
			}
		}
		elseif(isset($_POST['set_loc']) && $_POST['set_loc'] == 'Y')
		{
			if(!empty($_POST['city_id']))
			{
				global $DB;
				$city_id = $DB->ForSql($_POST['city_id']);
				$reg_id = $DB->ForSql($_POST['REGION_CODE']);
				if(!empty($reg_id))
					$res = CAltasibGeoBase::SetCodeKladr($city_id, $reg_id);
				else
					$res = CAltasibGeoBase::SetCodeKladr($city_id);
			}
			elseif(isset($_POST['CITY_NAME']) && isset($_POST['COUNTRY_CODE']))
			{
				global $DB;
				$city_name = $DB->ForSql($_POST['CITY_NAME']);
				$ctry_code = $DB->ForSql($_POST['COUNTRY_CODE']);
				$region_code = $DB->ForSql($_POST['REGION_CODE']);
				$res = CAltasibGeoBase::SetCodeMM($city_name, $ctry_code, $region_code);
			}
			else
				$res = false;

			if($res && COption::GetOptionString(self::MID, 'redirect_enable', 'Y') == "Y")
			{
				$strCityURL = "";
				$arSelCity = CAltasibGeoBaseSelected::GetCurrentCityFromSelected();

				if(!empty($arSelCity) && count($arSelCity) > 0)
				{
					$strCityURL = CAltasibGeoBaseSelected::GetUFValue($arSelCity["ID"]);
				}

				if(empty($strCityURL))
				{
					$strCityURL = "#reload";
				}
				return $res.";".$strCityURL;
			}
		}
		return $res;
	}

	function CitySearch($bAdminSection = false)
	{
		$city = trim(urldecode($_POST['city_name']));
		if(SITE_CHARSET == 'windows-1251')
		{
			$city1 = @iconv("UTF-8", "windows-1251//IGNORE", $city); //All AJAX requests come in Unicode
			if($city1)
				$city = $city1;	//if used Windows-machine
		}
		$city = addslashes($city);
		$citylen = strlen($city);

		$arCity = array();
		$i = 0;

		if($bAdminSection)
			$bCitiesWorldEn = true;
		else
			$bCitiesWorldEn = (COption::GetOptionString(self::MID, 'cities_world_enable', 'Y') == "Y");

		$strRegionEn = COption::GetOptionString(self::MID, 'mode_location', 'cities');

		if(isset($_POST['lang']) && strtolower($_POST['lang']) == "ru" || !$bCitiesWorldEn) //LANGUAGE_ID
		{
			if($strRegionEn == "regions" || $strRegionEn == "all" || $bAdminSection)
			{
				if($citylen == 2)
					$rsRegions = CAltasibGeoBase::GetRegionByName($city, true, 7);
				elseif($citylen == 3)
					$rsRegions = CAltasibGeoBase::GetRegionByName($city, false, 20);
				elseif($citylen > 3)
					$rsRegions = CAltasibGeoBase::GetRegionByName($city, false, false);

				while($arData = $rsRegions->Fetch())
				{
					if(SITE_CHARSET == 'windows-1251')
					{
						$arCity[(string) $i] = array(
							'REGION' => iconv('windows-1251', 'utf-8', $arData['FULL_NAME']),
							'COUNTRY' => iconv('windows-1251', 'utf-8', GetMessage('ALTASIB_GEOBASE_RUSSIA'))
						);
					}
					else
					{
						$arCity[(string) $i] = array(
							'REGION' => $arData['FULL_NAME'],
							'COUNTRY' => GetMessage('ALTASIB_GEOBASE_RUSSIA')
						);
					}
					$arCity[(string) $i]['ID'] = $arData['ID'];
					$arCity[(string) $i]['DISTRICT'] = '';
					$arCity[(string) $i++]['C_CODE'] = $arData['CODE'];
				}
			}

			if($strRegionEn != "regions" || $bAdminSection)
			{
				if($citylen == 2)
					$rezData = CAltasibGeoBase::GetDataFromKLADR($city, true, 7);
				elseif($citylen == 3)
					$rezData = CAltasibGeoBase::GetDataFromKLADR($city, false, 20);
				elseif($citylen > 3)
					$rezData = CAltasibGeoBase::GetDataFromKLADR($city, false, false);

				$rezDataReg = CAltasibGeoBase::GetCityAsRegionOfKladrByName($city, false, false);

				if($rezDataReg)
				{
					while($arReg = $rezDataReg->Fetch()){
						if(array_key_exists("R_PINDEX", $arReg)){
							if(SITE_CHARSET == 'windows-1251'){
								$arCity[(string) $i] = array(
									'CITY' => iconv('windows-1251', 'utf-8', $arReg['R_SOCR'].'. '.$arReg['R_NAME']),
									'REGION' => iconv('windows-1251', 'utf-8', $arReg['R_FNAME'])
								);
							}
							else {
								$arCity[(string) $i] = array(
									'CITY' => $arReg['R_SOCR'].'. '.$arReg['R_NAME'],
									'REGION' => $arReg['R_FNAME']
								);
							}
							$arCity[(string) $i++]['C_CODE'] = $arReg['R_CODE'].'000000000';
						}
					}
				}
				while($arData = $rezData->Fetch())
				{
					if(array_key_exists("C_NAME", $arData)){
						if(SITE_CHARSET == 'windows-1251'){
							$arCity[(string) $i] = array(
								'CITY' => iconv('windows-1251', 'utf-8', $arData['C_SOCR'].'. '.$arData['C_NAME']),
								'DISTRICT' => iconv('windows-1251', 'utf-8', $arData['D_NAME'].' '.$arData['D_SOCR']),
								'REGION' => iconv('windows-1251', 'utf-8', $arData['R_NAME'])
							);
						}
						else {
							$arCity[(string) $i] = array(
								'CITY' => $arData['C_SOCR'].'. '.$arData['C_NAME'],
								'DISTRICT' => $arData['D_NAME'].' '.$arData['D_SOCR'],
								'REGION' => $arData['R_NAME']
							);
						}
						$arCity[(string) $i]['ID'] = $arData['ID'];
						$arCity[(string) $i++]['C_CODE'] = $arData['C_CODE'];
					}
				}

				if($arRegion = CAltasibGeoBase::GetHomeRegion()){
					if(SITE_CHARSET == 'windows-1251')
						$GLOBALS['RegionName'] = iconv('windows-1251', 'utf-8', $arRegion['FULL_NAME']);
					else
						$GLOBALS['RegionName'] = $arRegion['FULL_NAME'];

					usort($arCity, array('CAltasibGeoBaseTools', 'CompareArr'));
				}
				if($bCitiesWorldEn){
					$arMM = CAltasibGeoBase::CitySearchMM($city, $_POST['lang'], false);
					if(is_array($arMM))
						$arCity = array_merge($arCity, $arMM);
				}
			}
		}
		else
		{
			$arCity = CAltasibGeoBase::CitySearchMM($city, $_POST['lang'], true);
		}

		echo json_encode($arCity);
	}

	function CitySearchMM($city, $lang, $russia_en = false)
	{
		$citylen = strlen($city);
		$arCity = array();
		$i = 0;
		if($citylen == 2)
			$rezData = CAltasibGeoBase::GetDataByCities($city, $russia_en, true, 7);
		elseif($citylen == 3)
			$rezData = CAltasibGeoBase::GetDataByCities($city, $russia_en, false, 20);
		elseif($citylen > 3)
			$rezData = CAltasibGeoBase::GetDataByCities($city, $russia_en, false, false);

		if($rezData)
		{
			while($arData = $rezData->Fetch())
			{
				if(array_key_exists("CITY_EN", $arData))
				{
					if(CAltasibGeoBase::CheckCountry($arData['COUNTRY_CODE']))
					{
						$regName = CAltasibGeoBase::GetNameReg($arData['COUNTRY_CODE'], $arData['REGION_ID'], true);

						if(SITE_CHARSET == 'windows-1251'){
							$arCity[(string) $i] = array(
								'CITY' => iconv('windows-1251', 'utf-8', $arData['CITY_RU']),
								'COUNTRY' => iconv('windows-1251', 'utf-8', $arData['COUNTRY_RU'])
							);
							if(!empty($regName))
								$arCity[(string) $i]['REGION'] = $regName;
						}
						else {
							$arCity[(string) $i] = array(
								'CITY' => $arData['CITY_RU'],
								'COUNTRY' => $arData['COUNTRY_RU']
							);
							if(!empty($regName))
								$arCity[(string) $i]['REGION'] = $regName;
						}
					}else{
						$arCity[(string) $i]['CITY'] = $arData['CITY_EN'];
						$arCity[(string) $i]['COUNTRY'] = $arData['COUNTRY_EN'];
					}
					if(empty($arCity[$i]['REGION']) && file_exists($_SERVER["DOCUMENT_ROOT"].self::REG_VARS)){
						require_once($_SERVER["DOCUMENT_ROOT"].self::REG_VARS);
						$arCity[(string) $i]['REGION'] = $GEOIP_REGION_NAME[$arData['COUNTRY_CODE']][$arData['REGION_ID']];
					}
					$arCity[(string) $i]['ID'] = $arData['id'];
					$arCity[(string) $i]['C_CODE'] = $arData['CITY_ID'];
					$arCity[(string) $i++]['COUNTRY_CODE'] = $arData['COUNTRY_CODE'];
				}
			}
			return $arCity;
		}
	}

	function CheckCountry($country_code)
	{
		$arSNG = array("AZ", "AM", "BY", "KZ", "KG", "MD", "RU", "TJ", "TM", "UZ", "UA", "GE");
		return in_array($country_code, $arSNG);
	}

	function GetRegionLang($country_code, $region_id)
	{
		$rezRegLoc = CAltasibGeoBase::GetRegionLocation($country_code, $region_id, "ru", 1);
		if(!($arRegLoc = $rezRegLoc->Fetch()))
		{
			$arSNG = array("AZ", "AM", "BY", "KZ", "KG", "MD", "RU", "TJ", "TM", "UZ", "UA", "GE");
			$arSNGlang = array("az", "ascii", "be", "kk", "ky", "mo", "ru", "ascii", "ascii", "uz", "uk", "ka");

			$rezRL = CAltasibGeoBase::GetRegionLocation($country_code, $region_id,
					$arSNGlang[array_search($country_code, $arSNG)], 1);
			if(!($arRegLoc = $rezRL->Fetch()))
			{
				$rezEN = CAltasibGeoBase::GetRegionLocation($country_code, $region_id, "en", 1);
				if(!($arRegLoc = $rezEN->Fetch()))
				{
					$rezASCII = CAltasibGeoBase::GetRegionLocation($country_code, $region_id, "ascii", 1);
					$arRegLoc = $rezASCII->Fetch();
				}
			}
		}
		return $arRegLoc;
	}

	function GetCityAsRegionOfKladrByName($city, $strict=false, $limit=false)
	{
		global $DB;
		$city = $DB->ForSql($city);

		$data = $DB->Query('SELECT t1.FULL_NAME AS R_FNAME, t1.NAME AS R_NAME, t1.SOCR AS R_SOCR,
				t1.CODE AS R_CODE, t1.POSTINDEX AS R_PINDEX
			FROM altasib_geobase_kladr_region AS t1
			WHERE LOWER(t1.FULL_NAME)
			LIKE '.($strict != false ? '"'.strtolower($city).'" ' : 'CONCAT("'.strtolower($city).'", "%") ')
			.($limit != false ? 'LIMIT '.$limit : '')
			.'AND t1.ACTIVE = "Y" AND (t1.CODE = 77 OR t1.CODE = 78 OR t1.CODE = 92 OR t1.CODE = 99) ORDER BY R_CODE'
		);

		return $data;
	}
	function GetCityAsRegionOfKladrById($limit=false, $regId='77')
	{
		global $DB;

		$data = $DB->Query('SELECT t1.FULL_NAME AS R_FNAME, t1.NAME AS R_NAME, t1.SOCR AS R_SOCR,
				t1.CODE AS R_CODE, t1.POSTINDEX AS R_PINDEX
			FROM altasib_geobase_kladr_region AS t1
			WHERE (t1.CODE = "'.$regId.'") '
			.($limit != false ? 'LIMIT '.$limit : '')
			.'AND t1.ACTIVE = "Y" ORDER BY R_CODE'
		);
		return $data;
	}
	function GetRegionByName($city, $strict=false, $limit=false)
	{
		global $DB;
		$city = $DB->ForSql($city);

		$data = $DB->Query('SELECT `ID`, `FULL_NAME`, `NAME`, `SOCR`, `CODE` FROM altasib_geobase_kladr_region '
			.'WHERE LOWER(FULL_NAME) '
			.'LIKE '.($strict != false ? '"'.strtolower($city).'" ' : '"%'.strtolower($city).'%"')
			.' AND `ACTIVE` = "Y" ORDER BY `CODE`'
			.($limit != false ? ' LIMIT '.$limit : '')
		);
		return $data;
	}
	function GetRegionByCode($id)
	{
		global $DB;
		$city_id = $DB->ForSql($id);
		$rsData = $DB->Query('SELECT `ID` as R_ID, `FULL_NAME` as R_FNAME, `NAME` as R_NAME, `SOCR` as R_SOCR, `CODE` as R_CODE, `POSTINDEX` as R_PINDEX '
			.'FROM altasib_geobase_kladr_region '
			.'WHERE LOWER(CODE) LIKE "'.$city_id.'" ORDER BY ID LIMIT 1'
		);
		return $rsData;
	}
	function GetCityByName($city, $afields, $strict=false, $limit=false)
	{
		global $DB;
		$city = $DB->ForSql($city);

		$data = $DB->Query('SELECT '.implode(',', $afields)
			.' FROM altasib_geobase_kladr_cities
			WHERE LOWER(NAME)
			LIKE '.($strict != false ? '"'.strtolower($city).'" ' : 'CONCAT("'.strtolower($city).'", "%") ')
			.($limit != false ? 'LIMIT '.$limit : '')
			.'AND `ACTIVE` = "Y" ORDER BY `CODE`, `SORTINDEX` '
		);
		return $data;
	}
	function GetCityByNameReg($city, $afields, $idReg, $strict=false, $limit=false)
	{
		global $DB;
		$city = $DB->ForSql($city);

		$data = $DB->Query('SELECT '.implode(',', $afields)
			.' FROM altasib_geobase_kladr_cities
			WHERE LOWER(NAME)
			LIKE '.($strict != false ? '"'.strtolower($city).'" ' : 'CONCAT("'.strtolower($city).'", "%") ')
			.'AND LOWER(ID_DISTRICT) LIKE CONCAT("'.$idReg.'", "%") '
			.($limit != false ? 'LIMIT '.$limit : '')
			.'AND `ACTIVE` = "Y" ORDER BY `SORTINDEX` DESC, `STATUS` DESC'
		);
		return $data;
	}
	function GetDistrictByName($city, $afields, $strict=false, $limit=false)
	{
		global $DB;
		$city = $DB->ForSql($city);

		$data = $DB->Query('SELECT '.implode(',', $afields)
			.' FROM altasib_geobase_kladr_districts
			WHERE LOWER(NAME)
			LIKE '.($strict != false ? '"'.strtolower($city).'" ' : 'CONCAT("'.strtolower($city).'", "%") ')
			.($limit != false ? 'LIMIT '.$limit : '')
			.'AND `ACTIVE` = "Y" ORDER BY `CODE` '
		);
		return $data;
	}
	function GetDataSearch($city, $strict=false, $limit=false) //ipgeobase.ru - DB
	{
		global $DB;
		if($DB->TableExists('altasib_geobase_cities')){
			$city = $DB->ForSql($city);

			$data = $DB->Query('SELECT ID, CITY_NAME, REGION_NAME, COUNTY_NAME FROM altasib_geobase_cities
				WHERE LOWER(`CITY_NAME`)
				LIKE '.($strict != false ? '"'.strtolower($city).'" ' : 'CONCAT("'.strtolower($city).'", "%") ')
				.($limit != false ? 'LIMIT '.$limit : '')
			);
			return $data;
		} else
			return false;
	}
	function GetDataFromKLADR($city, $strict=false, $limit=false)
	{
		global $DB;
		$city = $DB->ForSql($city);

		if(COption::GetOptionString(self::MID, "set_sql", "Y") == "Y")
			$DB->Query("SET SQL_BIG_SELECTS=1");

		$dataKLADR = $DB->Query('SELECT t1.ID, t1.NAME AS C_NAME, t1.CODE AS C_CODE, t1.ID_DISTRICT, t1.SOCR AS C_SOCR,
				t2.FULL_NAME AS R_NAME, t2.CODE AS R_CODE,
				t3.NAME AS D_NAME, t3.SOCR AS D_SOCR
			FROM altasib_geobase_kladr_cities AS t1
			LEFT JOIN altasib_geobase_kladr_districts AS t3
				ON t1.ID_DISTRICT = t3.CODE
			LEFT JOIN altasib_geobase_kladr_region AS t2
				ON SUBSTRING(t1.ID_DISTRICT,1,2) = t2.CODE
			WHERE LOWER(t1.NAME) LIKE '
					.($strict != false ? '"'.strtolower($city).'" ' : 'CONCAT("'.strtolower($city).'", "%") ')
				.'AND t1.ACTIVE = "Y" ORDER BY t1.SORTINDEX, R_CODE, C_NAME '
				.($limit != false ? 'LIMIT '.$limit : '')
		);
		return $dataKLADR;
	}

	function GetDataByCountries($city, $strict=false, $limit=false)
	{
		global $DB;
		$city = $DB->ForSql($city);

		$data = $DB->Query('SELECT * FROM altasib_geobase_mm_country AS country
			WHERE LOWER(country.name_ru) LIKE '
				.($strict != false ? '"'.strtolower($city).'" ' : 'CONCAT("'.strtolower($city).'", "%") ')
			.'ORDER BY country.id '.($limit != false ? 'LIMIT '.$limit : '')
		);
		return $data;
	}
	function GetDataByCities($city, $ru_en=false, $strict=false, $limit=false)
	{
		global $DB;
		$city = $DB->ForSql($city);
		if($strict != false) $strict = true;

		if(COption::GetOptionString(self::MID, "set_sql", "Y") == "Y")
			$DB->Query("SET SQL_BIG_SELECTS=1");

		$data = $DB->Query('SELECT c1.id as CITY_ID, c1.country_id as COUNTRY_ID, c1.name_ru as CITY_RU,
				c1.name_en as CITY_EN, c1.region as REGION_ID, c1.postal_code as POST, c1.latitude, c1.longitude,
				c2.name_ru as COUNTRY_RU, c2.name_en as COUNTRY_EN, c2.code as COUNTRY_CODE
			FROM altasib_geobase_mm_city AS c1
			LEFT JOIN altasib_geobase_mm_country AS c2
			ON COUNTRY_ID = c2.id
			WHERE (LOWER(c1.name_ru) LIKE '
				.($strict ? '"'.strtolower($city).'" ' : 'CONCAT("'.strtolower($city).'", "%") ')
			.' OR LOWER(c1.name_en) LIKE '
				.($strict ? '"'.strtolower($city).'" ' : 'CONCAT("'.strtolower($city).'", "%") ')
			.($ru_en ? ') ' : ') AND c1.country_id != 20 && !(c1.country_id = 112 && (c1.region = 20 || c1.region = 11)) ') //Krim
			.' ORDER BY c1.id '.($limit != false ? 'LIMIT '.$limit : '')
		);
		return $data;
	}

	function GetNameReg($country_code, $region_id, $recursion=false)
	{
		$arSNG = array("AZ", "AM", "BY", "KZ", "KG", "MD", "RU", "TJ", "TM", "UZ", "UA", "GE");
		$arSNGlang = array("az", "ascii", "be", "kk", "ky", "mo", "ru", "ascii", "ascii", "uz", "uk", "ka");
		$rezRegLoc = CAltasibGeoBase::GetRegionLocation($country_code, $region_id, "ru", 1);
		if(!($arRegLoc = $rezRegLoc->Fetch()))
		{
			$rezRL = CAltasibGeoBase::GetRegionLocation($country_code, $region_id,
				$arSNGlang[array_search($country_code, $arSNG)], 1);
			if(!($arRegLoc = $rezRL->Fetch()))
			{
				$rezEN = CAltasibGeoBase::GetRegionLocation($country_code, $region_id, "en", 1);
				if(!($arRegLoc = $rezEN->Fetch()))
				{
					$rezASCII = CAltasibGeoBase::GetRegionLocation($country_code, $region_id, "ascii", 1);
					$arRegLoc = $rezASCII->Fetch();
				}
			}
			if(empty($arRegLoc["region_name"]) && $recursion){
				if(file_exists($_SERVER["DOCUMENT_ROOT"].self::REG_VARS))
					require_once($_SERVER["DOCUMENT_ROOT"].self::REG_VARS);
				else return;
				$reg_name = $GEOIP_REGION_NAME[$country_code][$region_id];
				$arRegID = CAltasibGeoBase::GetCodeReg($country_code, $reg_name);
				if(!empty($arRegID))
					$arRegLoc = CAltasibGeoBase::GetNameReg($country_code, $arRegID, false);
				else
					return;
			}
		}
		if(isset($arRegLoc["region_name"]))
			return $arRegLoc["region_name"];
		else
			return;
	}

	function GetCodeReg($country_code, $region_name)
	{
		global $DB;
		$country_code = $DB->ForSql($country_code);
		$data = $DB->Query('SELECT c1.region_code FROM altasib_geobase_mm_region as c1
				WHERE c1.country_code LIKE "'.$country_code.'"
				AND c1.region_name LIKE "'.$region_name.'%" LIMIT 1'
		);
		$rez = $data->Fetch();
		if(empty($rez["region_code"]) && strlen($region_name)> 5){
			return CAltasibGeoBase::GetCodeReg($country_code, substr($region_name, 0, -2));
		}
		return $rez["region_code"];
	}
	function GetRegionLocation($country_code, $region_code, $lang, $limit=false)
	{
		global $DB;
		$country_code = $DB->ForSql($country_code);
		$region_code = $DB->ForSql($region_code);
		$lang = $DB->ForSql($lang);
		$data = $DB->Query('SELECT * FROM altasib_geobase_mm_region AS c1
				WHERE c1.country_code LIKE "'.$country_code.'"
				AND c1.region_code LIKE "'.$region_code.'"
				AND c1.lang LIKE "'.$lang.'" '
				.($limit != false ? 'LIMIT '.$limit : '')
		);
		return $data;
	}

	function GetHomeRegion()
	{
		$arDataGeo = CAltasibGeoBase::GetAddres();
		if($arDataGeo)
		{
			$reg = $arDataGeo["REGION_NAME"];
			$findme = GetMessage("ALTASIB_GEOBASE_RESPUBLIC");
			$pos = strpos($reg, $findme);
			if($pos !== FALSE)
				$reg = substr($reg, $pos+10).' '.$findme;

			$rsRegions = CAltasibGeoBase::GetRegionByName($reg, false, false);
			if($arRegion = $rsRegions->Fetch())
			{
				$arInfo = array(
					"CODE" => $arRegion["CODE"],
					"NAME" => $arRegion["NAME"],
					"FULL_NAME" => $arRegion["FULL_NAME"],
					"SOCR" => $arRegion["SOCR"]
				);
			}
		}
		return $arInfo;
	}

	function GetCodeByAddr($ip_addr = "")
	{
		$arDataGeo = CAltasibGeoBase::GetAddres($ip_addr);
		if($arDataGeo)
		{
			if(empty($arDataGeo["REGION_NAME"]) && empty($arDataGeo["CITY_NAME"]))
			{
				if((COption::GetOptionString(self::MID, "ipgeobase_enable", "Y") == "Y"
					|| COption::GetOptionString(self::MID, "geoip_enable", "Y") == "Y")
					&& COption::GetOptionString(self::MID, "source", "local_db") == "statistic")
				{
					if(!$arDataGeo = CAltasibGeoBaseIPTools::GetGeoData($arDataGeo["BLOCK_ADDR"]))
						return false;
				}
				else
					return false;
			}
			$reg = $arDataGeo["REGION_NAME"];
			$findme = GetMessage("ALTASIB_GEOBASE_RESPUBLIC");
			$pos = strpos($reg, $findme);
			if($pos !== FALSE)
				$reg = trim(substr($reg, $pos+10).' '.$findme);
			$rsRegions = CAltasibGeoBase::GetRegionByName($reg, false, false);

			$arRegion = $rsRegions->Fetch();
			$regLen = strlen($reg);
			if(!$arRegion && $regLen > 5)
			{
				$len = ($pos !== FALSE ? 15 : 5);

				$reg = trim(substr($reg, 0, $regLen-$len));
				$rsRegions = CAltasibGeoBase::GetRegionByName($reg, false, false);
				$arRegion = $rsRegions->Fetch();

				if(!$arRegion && $regLen > 5){
					$reg = trim(substr($reg, 0, $regLen-5));
					$rsRegions = CAltasibGeoBase::GetRegionByName($reg, false, false);
					$arRegion = $rsRegions->Fetch();
				}
				if(!$arRegion && $regLen > 7){
					$reg = trim(substr($reg, 0, $regLen-5));
					$rsRegions = CAltasibGeoBase::GetRegionByName($reg, false, false);
					$arRegion = $rsRegions->Fetch();
				}
			}
			if($arRegion)
			{
				$arInfo["REGION"] = array(
					"CODE" => $arRegion["CODE"],
					"NAME" => $arRegion["NAME"],
					"FULL_NAME" => $arRegion["FULL_NAME"],
					"SOCR" => $arRegion["SOCR"]
				);
				$arInfo["DISTRICT"] = array( //default
					"CODE" => $arRegion["CODE"].'000',
					"NAME" => '',
					"SOCR" => ''
				);
				if($arRegion["CODE"] == 77 || $arRegion["CODE"] == 78
					|| $arRegion["CODE"] == 92 || $arRegion["CODE"] == 99){ //Mosqow SPb, Sevastopol, Baykonur
					$arInfo["CITY"] = array(
						"ID" => $arRegion["CODE"].'000000000',
						"NAME" => $arRegion["NAME"],
						"SOCR" => $arRegion["SOCR"],
						"POSTINDEX" => $arRegion["POSTINDEX"],
						"ID_DISTRICT" => $arRegion["CODE"].'000'
					);
					$arInfo["CODE"] = $arRegion["CODE"].'000000000';

					$rsCity = CAltasibGeoBase::GetCityByNameReg(trim(htmlspecialcharsEx($arDataGeo["CITY_NAME"])),
						array('ID', 'NAME', 'SOCR', 'POSTINDEX', 'ID_DISTRICT', 'CODE'), $arRegion["CODE"], false, false);
					if($arCity = $rsCity->Fetch()){
						$arInfo["CITY"] = array(
							"ID" => $arCity["ID"],
							"NAME" => $arCity["NAME"],
							"SOCR" => $arCity["SOCR"],
							"POSTINDEX" => $arCity["POSTINDEX"],
							"ID_DISTRICT" => $arCity["ID_DISTRICT"]
						);
						$arInfo["CODE"] = $arCity["CODE"];

						$rsDistrict = CAltasibGeoBase::GetDistrictByName($arCity["ID_DISTRICT"],
							array('NAME', 'SOCR', 'CODE'), false, false);
						if($arDistrict = $rsDistrict->Fetch()){
							$arInfo["DISTRICT"] = array(
								"CODE" => $arDistrict["CODE"],
								"NAME" => $arDistrict["NAME"],
								"SOCR" => $arDistrict["SOCR"]
							);
						}
					}
				}
				else //others cities
				{
					if(COption::GetOptionString(self::MID, "mode_location", "cities") != "regions")
					{
						$cName = trim(htmlspecialcharsEx($arDataGeo["CITY_NAME"]));
						$arSlct = array('ID', 'NAME', 'SOCR', 'POSTINDEX', 'ID_DISTRICT', 'CODE');
						$rsCity = CAltasibGeoBase::GetCityByNameReg($cName, $arSlct, $arRegion["CODE"], false, false);
						$arCity = $rsCity->Fetch();
						if(!$arCity)
						{
							$posCName = strpos($cName, "-");
							if($posCName !== FALSE)
								$cName = trim(substr($cName, 0, $posCName));
							$rsCity = CAltasibGeoBase::GetCityByNameReg($cName, $arSlct, $arRegion["CODE"], false, false);
							$arCity = $rsCity->Fetch();
						}
						if(!$arCity)
						{
							$posComa = strpos($cName, ".");
							if($posComa !== FALSE)
								$cName = trim(substr($cName, $posComa+1));
							$rsCity = CAltasibGeoBase::GetCityByNameReg($cName, $arSlct, $arRegion["CODE"], false, false);
							$arCity = $rsCity->Fetch();
						}
						if($arCity)
						{
							$arInfo["CITY"] = array(
								"ID" => $arCity["ID"],
								"NAME" => $arCity["NAME"],
								"SOCR" => $arCity["SOCR"],
								"POSTINDEX" => $arCity["POSTINDEX"],
								"ID_DISTRICT" => $arCity["ID_DISTRICT"]
							);
							$arInfo["CODE"] = $arCity["CODE"];

							$rsDistrict = CAltasibGeoBase::GetDistrictByName($arCity["ID_DISTRICT"],
								array('NAME', 'SOCR', 'CODE'), false, false);
							if($arDistrict = $rsDistrict->Fetch())
							{
								$arInfo["DISTRICT"] = array(
									"CODE" => $arDistrict["CODE"],
									"NAME" => $arDistrict["NAME"],
									"SOCR" => $arDistrict["SOCR"]
								);
							}
						}
					}
				}
			}
		}
		return $arInfo;
	}

	function GetCodeKladr()
	{
		global $APPLICATION;

		if(!is_array($_SESSION["ALTASIB_GEOBASE_CODE"]))
		{
			$ip = CAltasibGeoBaseIP::getUserHostIP();

			if(COption::GetOptionString(self::MID, "set_cookie", "Y") == "Y")
			{
				$last_ip = $APPLICATION->get_cookie("ALTASIB_LAST_IP");
				$strData = $APPLICATION->get_cookie("ALTASIB_GEOBASE_CODE");
			}

			if(($ip == $last_ip) && $strData)
			{
				$arData = CAltasibGeoBase::deCodeJSON($strData);
			}
			else
			{
				global $DB;

				if($DB->TableExists('altasib_geobase_kladr_region'))
					$arData = CAltasibGeoBase::GetCodeByAddr();
				elseif($DB->TableExists('altasib_kladr_region'))
					$arData = CAltasibGeoBaseTools::GetCodeKladrByAddr();

				if(!$arData) return false;

				if(COption::GetOptionString(self::MID, "set_cookie", "Y") == "Y"){
					$strData = CAltasibGeoBase::CodeJSON($arData);
					$APPLICATION->set_cookie("ALTASIB_LAST_IP", $ip, time() + 31104000); //60*60*24*30*12
					$APPLICATION->set_cookie("ALTASIB_GEOBASE_CODE", $strData, time() + 2592000); //60*60*24*30
				}
			}
			$_SESSION["ALTASIB_GEOBASE_CODE"] = $arData;
		}
		return $_SESSION["ALTASIB_GEOBASE_CODE"];
	}

	function GetDataKladr()
	{
		global $APPLICATION;

		if(!is_array($_SESSION["ALTASIB_GEOBASE_CODE"]))
		{
			$ip = CAltasibGeoBaseIP::getUserHostIP();

			if(COption::GetOptionString(self::MID, "set_cookie", "Y") == "Y")
			{
				$last_ip = $APPLICATION->get_cookie("ALTASIB_LAST_IP");
				$strData = $APPLICATION->get_cookie("ALTASIB_GEOBASE_CODE");
			}

			if(($ip == $last_ip) && $strData)
			{
				$arData = CAltasibGeoBase::deCodeJSON($strData);
			}
			else
			{
				global $DB;

				if($DB->TableExists('altasib_geobase_kladr_region'))
					$arData = CAltasibGeoBase::GetCodeByAddr();
				elseif($DB->TableExists('altasib_kladr_region'))
					$arData = CAltasibGeoBaseTools::GetCodeKladrByAddr();
			}
			if(!$arData)
				return false;
			else
				return $arData;
		}
		return $_SESSION["ALTASIB_GEOBASE_CODE"];
	}

	function SetCodeKladr($cityID, $regionCode = false)
	{
		global $APPLICATION;

		$strModLoc = COption::GetOptionString(self::MID, "mode_location", "cities");

		if($regionCode && $strModLoc != "cities")
		{
			if($strModLoc != "regions")
				$arData = CAltasibGeoBase::GetInfoKladrByCode($cityID, $regionCode);
			else
				$arData = CAltasibGeoBase::GetInfoKladrByCode(false, $regionCode);
		}
		else
		{
			$arData = CAltasibGeoBase::GetInfoKladrByCode($cityID);

			if(!$arData)
				$arData = CAltasibGeoBase::GetInfoMMByCode($cityID);
		}
		if(!$arData)
			return false;

		if(COption::GetOptionString(self::MID, "set_cookie", "Y") == "Y")
		{
			$strData = CAltasibGeoBase::CodeJSON($arData);
			$APPLICATION->set_cookie("ALTASIB_LAST_IP", $ip, time() + 31104000); //60*60*24*30*12
			$APPLICATION->set_cookie("ALTASIB_GEOBASE_CODE", $strData, time() + 2592000); //60*60*24*30
		}
		$_SESSION["ALTASIB_GEOBASE_CODE"] = $arData;

		unset($_SESSION["ALTASIB_GEOBASE_COUNTRY"]); //carrency & code of country
		CAltasibGeoBase::GetCurrency(false, true, (!empty($arData["COUNTRY_CODE"])? $arData["COUNTRY_CODE"]: ""));

		$events = GetModuleEvents(self::MID, "OnAfterSetSelectCity");
		while($arEvent = $events->Fetch()){
			ExecuteModuleEvent($arEvent, array($arData));
		}
		return true;
	}

	function SetCodeMM($cityName, $countryCode, $regionCode)
	{
		global $APPLICATION;

		$arData = CAltasibGeoBase::GetInfoMMByName($cityName, $countryCode, $regionCode);

		if(!$arData)
			return false;

		if(LANG_CHARSET != 'UTF-8' && !empty($arData["REGION"]))
			$arData["REGION"] = iconv("UTF-8", LANG_CHARSET, $arData["REGION"]);

		if(COption::GetOptionString(self::MID, "set_cookie", "Y") == "Y")
		{
			$strData = CAltasibGeoBase::CodeJSON($arData);
			$APPLICATION->set_cookie("ALTASIB_LAST_IP", $ip, time() + 31104000); //60*60*24*30*12
			$APPLICATION->set_cookie("ALTASIB_GEOBASE_CODE", $strData, time() + 2592000); //60*60*24*30
		}
		$_SESSION["ALTASIB_GEOBASE_CODE"] = $arData;

		unset($_SESSION["ALTASIB_GEOBASE_COUNTRY"]); //carrency & code of country
		CAltasibGeoBase::GetCurrency(false, true, (!empty($arData["COUNTRY_CODE"]) ? $arData["COUNTRY_CODE"] : ""));

		$events = GetModuleEvents(self::MID, "OnAfterSetSelectCity");
		while($arEvent = $events->Fetch()){
			ExecuteModuleEvent($arEvent, array($arData));
		}
		return true;
	}

	function GetInfoKladrByCode($city_ID, $region_code=false)
	{
		if((!$city_ID || $city_ID==$region_code) && !empty($region_code))
		{
			$arDataCode = CAltasibGeoBase::GetRegionByCode($region_code)->Fetch();
		}
		elseif($city_ID == '78000000000' || $city_ID == '77000000000'
			|| $city_ID == '92000000000' || $city_ID == '99000000000') //Moscow, Petersburg, Sevastopol, Baykonur
		{
			$rezDataReg = CAltasibGeoBase::GetCityAsRegionOfKladrById(false, $city_ID/1000000000)->Fetch();
			$arDataCode = array(
				"R_NAME" => $rezDataReg['R_NAME'],
				"R_FNAME" => $rezDataReg['R_FNAME'],
				"R_SOCR" => $rezDataReg['R_SOCR'],
				"R_CODE" => $rezDataReg['R_CODE'],
				"D_CODE" => '',
				"D_NAME" => '',
				"D_SOCR" => '',
				"ID" => '',
				"C_NAME" => $rezDataReg['R_NAME'],
				"C_SOCR" => $rezDataReg['R_SOCR'],
				"C_PINDEX" => $rezDataReg['R_PINDEX'],
				"ID_DISTRICT" => $rezDataReg['R_CODE'].'000',
				"C_CODE" => $rezDataReg['R_CODE'].'000000000',
			);
		}
		else
		{
			$arDataCode = CAltasibGeoBase::GetDataKladrByCode($city_ID)->Fetch();
		}

		if($arDataCode)
		{
			$arInfo["REGION"] = array(
				"CODE" => $arDataCode["R_CODE"],
				"NAME" => $arDataCode["R_NAME"],
				"FULL_NAME" => $arDataCode["R_FNAME"],
				"SOCR" => $arDataCode["R_SOCR"]
			);
			if(!empty($arDataCode["R_ID"]))
				$arInfo["REGION"]["ID"] = $arDataCode["R_ID"];
			if(!empty($arDataCode["R_PINDEX"]))
				$arInfo["REGION"]["POSTINDEX"] = $arDataCode["R_PINDEX"];

			$arInfo["DISTRICT"] = array(
				"CODE" => $arDataCode["D_CODE"],
				"NAME" => $arDataCode["D_NAME"],
				"SOCR" => $arDataCode["D_SOCR"]
			);
			$arInfo["CITY"] = array(
				"ID" => $arDataCode["ID"],
				"NAME" => $arDataCode["C_NAME"],
				"SOCR" => $arDataCode["C_SOCR"],
				"POSTINDEX" => $arDataCode["C_PINDEX"],
				"ID_DISTRICT" => $arDataCode["ID_DISTRICT"]
			);
			$arInfo["CODE"] = $arDataCode["C_CODE"];
		}
		return $arInfo;
	}

	function GetInfoMMByCode($city_ID)
	{
		$arDataCode = CAltasibGeoBase::GetDataMMByID($city_ID)->Fetch();
		if($arDataCode)
		{
			$arInfo = array(
				'CITY' => $arDataCode['CITY_EN'],
				'CITY_RU' => $arDataCode['CITY_RU'],
				'REGION_ID' => $arDataCode['REGION_ID'],
				'COUNTRY' => $arDataCode['COUNTRY_EN'],
				'COUNTRY_RU' => $arDataCode['COUNTRY_RU']
			);

			if(CAltasibGeoBase::CheckCountry($arDataCode['COUNTRY_CODE']))
			{
				$arRG = CAltasibGeoBase::GetRegionLang($arDataCode['COUNTRY_CODE'], $arDataCode['REGION_ID']);
				if(!empty($arRG['region_name']))
				{
					if(LANG_CHARSET == 'windows-1251')
						$arRG['region_name'] = iconv("UTF-8", "windows-1251", $arRG['region_name']);
					$arInfo['REGION'] = $arRG['region_name'];
				}
			}
			else
			{
				if(file_exists($_SERVER["DOCUMENT_ROOT"].self::REG_VARS))
				{
					require_once($_SERVER["DOCUMENT_ROOT"].self::REG_VARS);
					$arInfo['REGION'] = $GEOIP_REGION_NAME[$arDataCode['COUNTRY_CODE']][$arDataCode['REGION_ID']];
				}
			}

			if(isset($arDataCode['id']))
				$arInfo['ID'] = $arDataCode['id'];

			$arInfo['C_CODE'] = $arDataCode['CITY_ID'];
			$arInfo['COUNTRY_CODE'] = $arDataCode['COUNTRY_CODE'];
			$arInfo['POST'] = $arDataCode['POST'];
		}
		return $arInfo;
	}
	function GetInfoMMByName($cityName, $countryCode, $regionCode)
	{
		global $DB;
		if(COption::GetOptionString(self::MID, "set_sql", "Y") == "Y")
			$DB->Query("SET SQL_BIG_SELECTS=1");
		$dataMM = $DB->Query('SELECT c1.id as CITY_ID, c1.country_id as COUNTRY_ID, c1.name_ru as CITY_RU,
				c1.name_en as CITY_EN, c1.region as REGION_ID, c1.postal_code as POST, c1.latitude, c1.longitude,
				c2.name_ru as COUNTRY_RU, c2.name_en as COUNTRY_EN, c2.code as COUNTRY_CODE
			FROM altasib_geobase_mm_city AS c1
			LEFT JOIN altasib_geobase_mm_country AS c2
			ON COUNTRY_ID = c2.id
			WHERE c1.country_id LIKE "'.$countryCode.'" OR c2.code LIKE "'.$countryCode.'" AND '
				.(!empty($regionCode) ? ' c1.region LIKE "'.$regionCode.'" AND ' : '')
			.' LOWER(c1.name_ru) LIKE "'.$cityName.'" OR LOWER(c1.name_en) LIKE "'.$cityName.'" '
			.' ORDER BY CITY_ID LIMIT 1'
		);

		$arDataCode = $dataMM->Fetch();

		if($arDataCode)
		{
			$arInfo = array(
				'CITY' => $arDataCode['CITY_EN'],
				'CITY_RU' => $arDataCode['CITY_RU'],
				'REGION_ID' => $arDataCode['REGION_ID'],
				'COUNTRY' => $arDataCode['COUNTRY_EN'],
				'COUNTRY_RU' => $arDataCode['COUNTRY_RU']
			);

			if(CAltasibGeoBase::CheckCountry($arDataCode['COUNTRY_CODE'])){
				$arRG = CAltasibGeoBase::GetRegionLang($arDataCode['COUNTRY_CODE'], $arDataCode['REGION_ID']);
				if(!empty($arRG['region_name']))
					$arInfo['REGION'] = $arRG['region_name'];
			} else {
				if(file_exists($_SERVER["DOCUMENT_ROOT"].self::REG_VARS)){
					require_once($_SERVER["DOCUMENT_ROOT"].self::REG_VARS);
					$arInfo['REGION'] = $GEOIP_REGION_NAME[$arDataCode['COUNTRY_CODE']][$arDataCode['REGION_ID']];
				}
			}

			if(isset($arDataCode['id']))
				$arInfo['ID'] = $arDataCode['id'];
			$arInfo['C_CODE'] = $arDataCode['CITY_ID'];
			$arInfo['COUNTRY_CODE'] = $arDataCode['COUNTRY_CODE'];
			$arInfo['POST'] = $arDataCode['POST'];
		}
		else
			$arInfo = CAltasibGeoBase::GetAddres();
		return $arInfo;
	}

	function GetDataMMByID($id)
	{
		global $DB;
		$city_id = $DB->ForSql($id);
		if(COption::GetOptionString(self::MID, "set_sql", "Y") == "Y")
			$DB->Query("SET SQL_BIG_SELECTS=1");
		$dataMM = $DB->Query('SELECT c1.id as CITY_ID, c1.country_id as COUNTRY_ID, c1.name_ru as CITY_RU,
				c1.name_en as CITY_EN, c1.region as REGION_ID, c1.postal_code as POST, c1.latitude, c1.longitude,
				c2.name_ru as COUNTRY_RU, c2.name_en as COUNTRY_EN, c2.code as COUNTRY_CODE
			FROM altasib_geobase_mm_city AS c1
			LEFT JOIN altasib_geobase_mm_country AS c2
			ON COUNTRY_ID = c2.id
			WHERE LOWER(c1.id) LIKE "'.$city_id.'" ORDER BY CITY_ID LIMIT 1'
		);
		return $dataMM;
	}

	function GetDataKladrByCode($city)
	{
		global $DB;
		$city = $DB->ForSql($city);
		if(COption::GetOptionString(self::MID, "set_sql", "Y") == "Y")
			$DB->Query("SET SQL_BIG_SELECTS=1");
		$dataKLADR = $DB->Query('SELECT t1.ID, t1.NAME AS C_NAME, t1.CODE AS C_CODE, t1.ID_DISTRICT, t1.SOCR AS C_SOCR, t1.POSTINDEX AS C_PINDEX,
				t2.FULL_NAME AS R_FNAME, t2.CODE AS R_CODE, t2.NAME AS R_NAME, t2.SOCR AS R_SOCR,
				t3.NAME AS D_NAME, t3.SOCR AS D_SOCR, t3.CODE AS D_CODE
			FROM altasib_geobase_kladr_cities AS t1
			LEFT JOIN altasib_geobase_kladr_districts AS t3
				ON t1.ID_DISTRICT = t3.CODE
			LEFT JOIN altasib_geobase_kladr_region AS t2
				ON SUBSTRING(t1.ID_DISTRICT,1,2) = t2.CODE
			WHERE LOWER(t1.CODE) LIKE "'.$city.'" '
				.'AND t1.ACTIVE = "Y" ORDER BY C_NAME, R_CODE '
				.'LIMIT 1'
		);
		return $dataKLADR;
	}

	function GetTemplateProps($componentName, $templateName, $siteTemplate = "")
	{
		$arTemplateParameters = array();

		$componentName = trim($componentName);
		if(strlen($componentName) <= 0)
			return false;

		if(strlen($templateName) <= 0)
			$templateName = ".default";

		if(!preg_match("#[A-Za-z0-9_.-]#i", $templateName))
			return false;

		$path2Comp = CComponentEngine::MakeComponentPath($componentName);
		if(strlen($path2Comp) <= 0)
			return false;

		$componentPath = getLocalPath("components".$path2Comp);

		if(!CComponentUtil::isComponent($componentPath))
			return false;

		if($siteTemplate <> "")
			$siteTemplate = _normalizePath($siteTemplate);

		$folders = array();
		if($siteTemplate <> "")
		{
			$folders[] = "/local/templates/".$siteTemplate."/components".$path2Comp."/".$templateName;
		}
		$folders[] = "/local/templates/.default/components".$path2Comp."/".$templateName;
		$folders[] = "/local/components".$path2Comp."/templates/".$templateName;

		if($siteTemplate <> "")
		{
			$folders[] = BX_PERSONAL_ROOT."/templates/".$siteTemplate."/components".$path2Comp."/".$templateName;
		}
		$folders[] = BX_PERSONAL_ROOT."/templates/.default/components".$path2Comp."/".$templateName;
		$folders[] = "/bitrix/components".$path2Comp."/templates/".$templateName;
		global $APPLICATION;

		foreach($folders as $templateFolder)
		{
			if(file_exists($_SERVER["DOCUMENT_ROOT"].$templateFolder))
			{
				if(file_exists($_SERVER["DOCUMENT_ROOT"].$templateFolder."/script.js")){
					$APPLICATION->AddHeadScript($templateFolder."/script.js");
				}
				if(file_exists($_SERVER["DOCUMENT_ROOT"].$templateFolder."/style.css")){
					$APPLICATION->SetAdditionalCSS($templateFolder."/style.css");
				}
			}
		}
	}

	function UpOnBeforeEndBufferContent()
	{
		if(!IsModuleInstalled(self::MID))
			return;
		if(defined("ADMIN_SECTION") && ADMIN_SECTION === true)
			return;
		if(defined("NO_GEOBASE") && NO_GEOBASE === true)
			return;

		global $APPLICATION;
		CAltasibGeoBaseTools::CheckUpdateSessionData();
		CAltasibGeoBaseTools::CheckForRedirect(); //redirects
		CAltasibGeoBaseTools::AddScriptYourCityOnSite();
		return true;
	}

	function GetTypeCurrency($country=false, $ip_addr="")
	{
		if(CModule::IncludeModule("currency"))
			$currency = CCurrency::GetBaseCurrency();

		if(!$country)
			$country = CAltasibGeoBase::GetCountry($ip_addr);

		if(!empty($country))
			$currency = "USD";

		if($country == "RU")
			$currency = "RUB";
		else
		{
			$arEU = array('AT','BE','BG','GB','HU','DE','GR','DK','IE','ES','IT','CY','LV','LT','LU','MT',
				'NL','PL','PT','RO','SK','SI','FI','FR','CZ','SE','EE');
			if(in_array($country, $arEU))
				$currency = "EUR";
		}
		return $currency;
	}
	function GetCurrency($use_func=false, $reload=false, $country="")
	{
		$reload = !$reload ? false : true;
		global $APPLICATION;

		if($country == "")
		{
			if(!is_array($_SESSION["ALTASIB_GEOBASE_COUNTRY"]) || $reload)
			{
				$ip = CAltasibGeoBaseIP::getUserHostIP();

				if(COption::GetOptionString(self::MID, "set_cookie", "Y") == "Y"){
					$last_ip = $APPLICATION->get_cookie("ALTASIB_LAST_IP");
					$strCountry = $APPLICATION->get_cookie("ALTASIB_GEOBASE_COUNTRY");

					if(($ip == $last_ip) && $strCountry && !$reload)
						$arData = CAltasibGeoBase::deCodeJSON($strCountry);
				}

				if(empty($arData) || count($arData) == 0 || empty($arData["country"]))
				{
					$arCountry = CAltasibGeoBase::GetCountry("", $use_func);
					$arData = array("country" => $arCountry);

					if(COption::GetOptionString(self::MID, "set_cookie", "Y") == "Y"){
						$strData = CAltasibGeoBase::CodeJSON($arData);
						$APPLICATION->set_cookie("ALTASIB_LAST_IP", $ip, time() + 31104000);
						$APPLICATION->set_cookie("ALTASIB_GEOBASE_COUNTRY", $strData, time() +2592000);
					}
				}
				$_SESSION["ALTASIB_GEOBASE_COUNTRY"] = $arData;
			}
		}
		else
		{
			$arData = array("country" => $country);

			if(COption::GetOptionString(self::MID, "set_cookie", "Y") == "Y"){
				$strData = CAltasibGeoBase::CodeJSON($arData);
				$APPLICATION->set_cookie("ALTASIB_GEOBASE_COUNTRY", $strData, time() +2592000); //60*60*24*30
			}
			$_SESSION["ALTASIB_GEOBASE_COUNTRY"] = $arData;
		}
		return $_SESSION["ALTASIB_GEOBASE_COUNTRY"];
	}

	function GetCountry($ip_addr="", $use_func=true)
	{
		if($ip_addr == "")
		{
			global $APPLICATION;
			$CODE = "";
			$arDataC = $_SESSION["ALTASIB_GEOBASE_CODE"];
			if(!empty($arDataC["COUNTRY_CODE"]))
				$CODE = $arDataC["COUNTRY_CODE"];
			elseif(!empty($arDataC["REGION"]["CODE"]) && isset($arDataC["CITY"]["NAME"]))
				$CODE = "RU";
			else
			{
				$arDataC = CAltasibGeoBase::deCodeJSON($APPLICATION->get_cookie("ALTASIB_GEOBASE_CODE"));

				if(!empty($arDataC["COUNTRY_CODE"]))
					$CODE = $arDataC["COUNTRY_CODE"];
				elseif(!empty($arDataC["REGION"]["CODE"]) && isset($arDataC["CITY"]["NAME"]))
					$CODE = "RU";
			}

			if(!empty($arDataC) && isset($arDataC["CITY"]["NAME"]))
				$CODE = "RU";

			if(empty($CODE))
			{
				$arDataS = $_SESSION["ALTASIB_GEOBASE"];
				if(is_array($arDataS) && !empty($arDataS["COUNTRY_CODE"]))
				{
					$CODE = $arDataS["COUNTRY_CODE"];
				}
				else
				{
					if(COption::GetOptionString(self::MID, "set_cookie", "Y") == "Y"){
						$arDataS = CAltasibGeoBase::deCodeJSON($APPLICATION->get_cookie("ALTASIB_GEOBASE"));
						if(is_array($arDataS) && !empty($arDataS["COUNTRY_CODE"])){
							$CODE = $arDataS["COUNTRY_CODE"];
						}
					}
				}

				if(is_array($arDataS) && isset($arDataS["COUNTRY_CODE"]))
					$CODE = $arDataS["COUNTRY_CODE"];
			}
		}
		else
		{
			if($use_func)
				$arDataC = CAltasibGeoBase::GetCodeByAddr($ip_addr);
			if(!empty($arDataC) && $arDataC["REGION"]["CODE"] != "01" && !empty($arDataC["CITY"]["CODE"])){
				$CODE = "RU";
			}
			else{
				if($use_func)
					$arDataS = CAltasibGeoBase::GetAddres($ip_addr, false);
				if(!empty($arDataS))
					if(isset($arDataS["COUNTRY_CODE"]))
						$CODE = $arDataS["COUNTRY_CODE"];
			}
		}
		unset($arDataC, $arDataS);
		return $CODE;
	}

	function DeviceIdentification()
	{
		$checkType='';
		if(isset($_COOKIE['ALTASIB_SITETYPE']))
			$checkType=$_COOKIE['ALTASIB_SITETYPE'];
		else
		{
			$detect = new CAltasibGeoBaseMobile_Detect();
			if($detect->isMobile() && !$detect->isTablet())
				$checkType='mobile';
			else if($detect->isMobile() && $detect->isTablet())
				$checkType='pda';
			else
				$checkType='original';
			setcookie('ALTASIB_SITETYPE', $checkType, time()+2592000,'/'); //3600*24*30
			define('ALTASIB_SITETYPE',$checkType);
		}
		return($checkType);
	}

	function GetTemplate($str)
	{
		$ar_template = explode(",", $str);
		foreach($ar_template as &$value){
			if($value == SITE_TEMPLATE_ID)
				return true;
		}
	}
	function CheckSite($str)
	{
		$ar_sites = explode(",", $str);
		foreach($ar_sites as &$value)
		{
			if($value == SITE_ID)
				return true;
		}
	}
	function SearchSaleLocations($search, $siteId, $country=false, $region=false, $strLang = LANGUAGE_ID)
	{
		$arResult = array();
		global $APPLICATION, $DB;
		if(\Bitrix\Main\Loader::includeModule('sale'))
		{
			if(!empty($search) && is_string($search))
			{
				$search = $APPLICATION->UnJSEscape($search);
				$siteId = $DB->ForSql($siteId);
				if($country || $region){
					$filter = \Bitrix\Sale\SalesZone::makeSearchFilter("city", $siteId);
					$filter["~CITY_NAME"] = $search."%";
					$filter["LID"] = $strLang;

					if($country)
						$filter["~COUNTRY_NAME"] = $DB->ForSql($country);
					if($region)
						$filter["~REGION_NAME"] = $DB->ForSql($region);

					$rsLocationsList = CSaleLocation::GetList(
						array(
							"CITY_NAME_LANG" => "ASC",
							"COUNTRY_NAME_LANG" => "ASC",
							"SORT" => "ASC",
						),
						$filter,
						false,
						array("nTopCount" => 10),
						array("ID", "CITY_ID", "CITY_NAME", "COUNTRY_NAME_LANG", "REGION_NAME_LANG")
					);
					while($arCity = $rsLocationsList->GetNext())
					{
						$arResult[] = array(
							"ID" => $arCity["ID"],
							"NAME" => $arCity["CITY_NAME"],
							"REGION_NAME" => $arCity["REGION_NAME_LANG"],
							"COUNTRY_NAME" => $arCity["COUNTRY_NAME_LANG"],
						);
					}
				}
				else
				{
					$filter = \Bitrix\Sale\SalesZone::makeSearchFilter("city", $siteId);
					$filter["~CITY_NAME"] = $search."%";
					$filter["LID"] = $strLang;

					$rsLocationsList = CSaleLocation::GetList(
						array(
							"CITY_NAME_LANG" => "ASC",
							"COUNTRY_NAME_LANG" => "ASC",
							"SORT" => "ASC",
						),
						$filter,
						false,
						array("nTopCount" => 10),
						array("ID", "CITY_ID", "CITY_NAME", "COUNTRY_NAME_LANG", "REGION_NAME_LANG")
					);
					while($arCity = $rsLocationsList->GetNext())
					{
						$arResult[] = array(
							"ID" => $arCity["ID"],
							"NAME" => $arCity["CITY_NAME"],
							"REGION_NAME" => $arCity["REGION_NAME_LANG"],
							"COUNTRY_NAME" => $arCity["COUNTRY_NAME_LANG"],
						);
					}

					$filter = \Bitrix\Sale\SalesZone::makeSearchFilter("region", $siteId);
					$filter["~REGION_NAME"] = $search."%";
					$filter["LID"] = $strLang;
					$filter["CITY_ID"] = false;
					$rsLocationsList = CSaleLocation::GetList(
						array(
							"CITY_NAME_LANG" => "ASC",
							"COUNTRY_NAME_LANG" => "ASC",
							"SORT" => "ASC",
						),
						$filter,
						false,
						array("nTopCount" => 10),
						array("ID", "CITY_ID", "CITY_NAME", "COUNTRY_NAME_LANG", "REGION_NAME_LANG")
					);
					while($arCity = $rsLocationsList->GetNext())
					{
						$arResult[] = array(
							"ID" => $arCity["ID"],
							"NAME" => "",
							"REGION_NAME" => $arCity["REGION_NAME_LANG"],
							"COUNTRY_NAME" => $arCity["COUNTRY_NAME_LANG"],
						);
					}

					$filter = \Bitrix\Sale\SalesZone::makeSearchFilter("country", $siteId);
					$filter["~COUNTRY_NAME"] = $search."%";
					$filter["LID"] = $strLang;
					$filter["CITY_ID"] = false;
					$filter["REGION_ID"] = false;
					$rsLocationsList = CSaleLocation::GetList(
						array(
							"COUNTRY_NAME_LANG" => "ASC",
							"SORT" => "ASC",
						),
						$filter,
						false,
						array("nTopCount" => 10),
						array("ID", "COUNTRY_NAME_LANG")
					);
					while($arCity = $rsLocationsList->GetNext())
					{
						$arResult[] = array(
							"ID" => $arCity["ID"],
							"NAME" => "",
							"REGION_NAME" => "",
							"COUNTRY_NAME" => $arCity["COUNTRY_NAME_LANG"],
						);
					}
				}
			}
		}
		return $arResult;
	}

	function GetBXLocations()
	{
		$USER_CHOICE = CAltasibGeoBase::GetDataKladr();
		$auto = CAltasibGeoBase::GetAddres();

		if(!empty($USER_CHOICE["CITY"]["SOCR"])){
			$city = $USER_CHOICE["CITY"]["NAME"];
			$region = $USER_CHOICE["REGION"]["NAME"];
			$country = GetMessage("ALTASIB_GEOBASE_RUSSIA");
		}
		elseif(!empty($USER_CHOICE["COUNTRY_RU"]) || !empty($USER_CHOICE["CITY_RU"])){
			$arCurr = CAltasibGeoBase::GetCurrency($use_func=true, $reload=false);
			$city = ((!empty($USER_CHOICE["CITY_RU"]) && CAltasibGeoBase::CheckCountry($arCurr["country"])) ?
				$USER_CHOICE["CITY_RU"] : (!empty($USER_CHOICE["CITY"]) ? $USER_CHOICE["CITY"] : ''));
			$country = ((!empty($USER_CHOICE["COUNTRY_RU"]) && $arResult['RU_ENABLE'] == "Y") ?
			$USER_CHOICE["COUNTRY_RU"] : (!empty($USER_CHOICE["COUNTRY"]) ? $USER_CHOICE["COUNTRY"] : ''));
			$city_ru = ((!empty($USER_CHOICE["CITY_RU"])) ? $USER_CHOICE["CITY_RU"] : '');
			$country_ru = ((!empty($USER_CHOICE["COUNTRY_RU"])) ? $USER_CHOICE["COUNTRY_RU"] : '');
			$region = ((!empty($USER_CHOICE["region"])) ? $USER_CHOICE["region"] : '');
		}
		elseif(!empty($auto["CITY_NAME"])){
			$city = $auto["CITY_NAME"];
			$country = $auto["COUNTRY_NAME"];
			$region = $auto["REGION_NAME"];
		}

		if(CModule::IncludeModule(self::MID))
		{
			if(!empty($country)){
				$arLocsTwo = CAltasibGeoBase::SearchSaleLocations($city, SITE_ID, $country);
				if(empty($arLocsTwo)){
					$arLocsTwo = CAltasibGeoBase::SearchSaleLocations($city, SITE_ID, $country, false, "ru");
				}
				if(empty($arLocsTwo) && !empty($city_ru)){
					$arLocsTwo = CAltasibGeoBase::SearchSaleLocations($city_ru, SITE_ID, $country);
				}

				if(empty($arLocsTwo)){
					if($country_ru == GetMessage("ALTASIB_GEOBASE_RF") || $country == "Russian Federation")
						$arLocsTwo = CAltasibGeoBase::SearchSaleLocations($city, SITE_ID, "Russia");
						if(empty($arLocsTwo))
							$arLocsTwo = CAltasibGeoBase::SearchSaleLocations($city, SITE_ID, GetMessage("ALTASIB_GEOBASE_RUSSIA"));
						if(empty($arLocsTwo))
							$arLocsTwo = CAltasibGeoBase::SearchSaleLocations($city_ru, SITE_ID, GetMessage("ALTASIB_GEOBASE_RUSSIA"));
				}
			}
			if(empty($arLocsTwo) && !empty($country_ru)){
				$arLocsTwo = CAltasibGeoBase::SearchSaleLocations($city, SITE_ID, $country_ru);
				if(empty($arLocsTwo) && !empty($city_ru))
					$arLocsTwo = CAltasibGeoBase::SearchSaleLocations($city_ru, SITE_ID, $country_ru);
			}
			if(empty($arLocsTwo) && !empty($region)){
				$arLocsTwo = CAltasibGeoBase::SearchSaleLocations($region, SITE_ID, $country_ru);
			}
			//simplify
			if(empty($arLocsTwo)){
				$arLocs = CAltasibGeoBase::SearchSaleLocations($city, SITE_ID);
				if(!empty($city_ru))
					$arLocs[] = CAltasibGeoBase::SearchSaleLocations($city_ru, SITE_ID);

				if(empty($arLocs)){
					$arLocs = CAltasibGeoBase::SearchSaleLocations($country, SITE_ID);
					if(empty($arLocs) && !empty($country_ru)){
						$arLocs = CAltasibGeoBase::SearchSaleLocations($country_ru, SITE_ID);
					}
				}
			}

			if(!empty($arLocsTwo)){
				foreach($arLocsTwo as $unit){
					if(!empty($unit["NAME"])){
						$rez = $unit["ID"]; break;
					}
				}
				if(empty($rez))
					foreach($arLocsTwo as $unit){
						if(!empty($unit["ID"])){
							$rez = $unit["ID"]; break;
						}
					}
			}
			if(!empty($arLocs) && empty($rez)){
				foreach($arLocs as $unit){
					if(!empty($unit["NAME"])){
						$rez = $unit["ID"]; break;
					}
				}
				if(empty($rez)){
					foreach($arLocs as $unit){
						foreach($unit as $elem){
							if(!empty($elem["ID"])){
								$rez = $elem["ID"]; break;
							}
						}
					}
				}
				if(empty($rez))
					foreach($arLocs as $unit){
						if(!empty($unit["ID"])){
							$rez = $unit["ID"]; break;
						}
					}
			}
			if(!empty($rez))
				return $rez;
			else
				return false;
		}
	}
}

Class CAltasibGeoBaseAllSelected extends CAltasibGeoBase
{
	const MID = "altasib.geobase";

	function GetCitySelected()	//altasib_geobase_selected.php
	{
		if($_SERVER["REQUEST_METHOD"] != "POST")
			return false;

		if(!check_bitrix_sessid('sessid') && !IsIE())
			return false;

		if(!isset($_POST['city_name']) && !isset($_POST['add_city'])
			&& !isset($_POST['delete_city']) && !isset($_POST['update']))
		{
			return false;
		}
		elseif(empty($_POST['city_name']) && empty($_POST['add_city'])
			&& empty($_POST['delete_city']) && empty($_POST['update']))
			die('pusto');
		elseif(isset($_POST['city_name'])) //search cities
		{
			return CAltasibGeoBase::CitySearch(true);
		}
		elseif(isset($_POST['add_city']) && $_POST['add_city'] == 'Y'){ //add city
			if(isset($_POST['city_id'])){
				global $DB;
				$city_id = $DB->ForSql($_POST['city_id']);

				$sites = CSite::GetList($by="sort", $order="desc", Array("ACTIVE" => "Y"));
				while($Site= $sites->Fetch()){
					BXClearCache(true, $Site["LID"]."/altasib/geobase/");
				}

				if(strlen($city_id) <= 2)
					return(CAltasibGeoBaseSelected::BeforeAddRegion($city_id));
				elseif(strlen($city_id) < 11)
					return(CAltasibGeoBaseSelected::BeforeAddMMCity($city_id));
				else
					return(CAltasibGeoBaseSelected::BeforeAddCity($city_id));
			}
		}
		elseif(isset($_POST['delete_city']) && $_POST['delete_city'] == 'Y'){
			if(isset($_POST['entry_id'])){	//delete city from table altasib_geobase_selected
				global $DB;
				$city_id = $DB->ForSql($_POST['entry_id']);

				$sites = CSite::GetList($by="sort", $order="desc", Array("ACTIVE" => "Y"));
				while($Site= $sites->Fetch()){
					BXClearCache(true, $Site["LID"]."/altasib/geobase/");
				}

				return(CAltasibGeoBaseAllSelected::DeleteCity($city_id));
			}
		}
		elseif(isset($_POST['update']) && $_POST['update'] == 'Y'){ //restart html table
			return(CAltasibGeoBaseSelected::UpdateCityRows());
		}
	}

	function AddCity($arFields)
	{
		global $DB;

		if(!CAltasibGeoBaseSelected::CheckFields($arFields))
			return false;

		$arInsert = $DB->PrepareInsert("altasib_geobase_selected", $arFields);

		$strSql = "INSERT INTO altasib_geobase_selected(".$arInsert[0].") " . "VALUES(".$arInsert[1].")";
		$DB->Query($strSql, false, "File: ".__FILE__."<br />Line: ".__LINE__);
		$ID = IntVal($DB->LastID());

		return $ID;
	}

	function DeleteCity($ID)
	{
		global $DB;
		$ID = IntVal($ID);

		if($ID<=0)
			return false;

		$DB->Query("DELETE FROM altasib_geobase_selected WHERE ID = ".$ID, true);
			return true;
	}

	function GetMoreCities($active = false)
	{
		global $DB;
		$mm_exsts = $DB->TableExists('altasib_geobase_mm_country');
		if(COption::GetOptionString(self::MID, "set_sql", "Y") == "Y")
			$DB->Query("SET SQL_BIG_SELECTS=1");
		$strSql = 'SELECT t1.ID, t1.ACTIVE, t1.SORT, t1.NAME AS C_NAME, t1.NAME_EN AS C_NAME_EN, t1.CODE AS C_CODE, t1.ID_DISTRICT, t1.SOCR AS C_SOCR,
			t2.FULL_NAME AS R_FNAME, t3.NAME AS D_NAME, t3.SOCR AS D_SOCR, t1.COUNTRY_CODE AS CTR_CODE, '
			.($mm_exsts ? ' t4.name_ru AS CTR_NAME_RU,' : ''). ' t1.ID_REGION AS R_ID
			FROM altasib_geobase_selected AS t1
			LEFT JOIN altasib_geobase_kladr_districts AS t3
				ON t1.ID_DISTRICT = t3.CODE
			LEFT JOIN altasib_geobase_kladr_region AS t2
				ON SUBSTRING(t1.ID_DISTRICT,1,2) = t2.CODE '
			.($mm_exsts ? ' LEFT JOIN altasib_geobase_mm_country AS t4
				ON t1.COUNTRY_CODE = t4.code ' : '')
			.($active != false ? 'WHERE t1.ACTIVE = "Y" ' : '')
			.'ORDER BY t1.SORT, t1.ID ';

		$db_res = $DB->Query($strSql, false, "File: ".__FILE__."<br />Line: ".__LINE__);

		return $db_res;
	}

	function GetMoreCacheCities()
	{
		//PHP Cache
		$obCache = new CPHPCache();
		$cache_time = 86400;
		$cache_id = 'GetMoreCacheCities';
		$cache_path = SITE_ID.'/altasib/geobase/';
		if($obCache->InitCache($cache_time, $cache_id, $cache_path))
		{
			$arCities = $obCache->GetVars();
		}
		else
		{
			$resCities = CAltasibGeoBaseAllSelected::GetMoreCities(true);
			while($arCity = $resCities->Fetch()){
				$arCities[] = $arCity;
			}
			BXClearCache(true, SITE_ID."/altasib/geobase/");

			if($obCache->StartDataCache()){
				//If the cache file has expired, then the function returns true, otherwise - false
				$obCache->EndDataCache(
					$arCities
				);
			}
		}
		return $arCities;
	}
}


Class CAltasibGeoBaseIP extends CAltasibGeoBase
{
	const MID = "altasib.geobase";
	const ipgeobase = "http://ipgeobase.ru:7020/geo/";
	const geoip_top = "http://geoip.elib.ru/cgi-bin/getdata.pl";

	function GetGeoDataIpgeobase_ru($ip)
	{
		if(empty($ip))
			return;
		if(!CAltasibGeoBaseIP::CheckServiceAccess(self::ipgeobase))
			return;
		if(!function_exists('curl_init'))
		{
			if(!$text = file_get_contents(self::ipgeobase.'?ip='.$ip))
				return false;
		}
		else
		{
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, self::ipgeobase."?ip=".$ip);
			curl_setopt($ch, CURLOPT_HEADER, TRUE);
			curl_setopt($ch, CURLOPT_VERBOSE, TRUE);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_TIMEOUT, 1);

			$text = curl_exec($ch);
			$errno = curl_errno($ch);
			$errstr = curl_error($ch);
			curl_close($ch);

			if($errno)
				return false;
		}
		if(LANG_CHARSET != 'windows-1251')
			$text = iconv("windows-1251", SITE_CHARSET, $text);

		$arData_ = CAltasibGeoBaseIPTools::ParseXML($text);

		$arData = Array(
			"ID" => "",
			"BLOCK_BEGIN" => "",
			"BLOCK_END" => "",
			"BLOCK_ADDR" => $arData_["inetnum"],
			"COUNTRY_CODE" => $arData_["country"],
			"CITY_ID" => "",
			"CITY_NAME" => $arData_["city"],
			"REGION_NAME" => $arData_["region"],
			"COUNTY_NAME" => $arData_["district"],
			"BREADTH_CITY" => $arData_["lat"],
			"LONGITUDE_CITY" => $arData_["lng"]
		);

		return ($arData);
	}

	function GetGeoDataGeoip_Elib_ru($ip)
	{
		if(empty($ip))
			return;

		if(!CAltasibGeoBaseIP::CheckServiceAccess(self::geoip_top))
			return;

		$siteCode = COption::GetOptionString(self::MID, "geoip_top_code_".SITE_ID, "");

		$strUrl = self::geoip_top.'?ip='.$ip.'&hex=3f'; //3ffd
		if(!empty($siteCode))
			$strUrl .= "&sid=".$siteCode;

		if(!function_exists('curl_init'))
		{
			if(!$text = file_get_contents($strUrl))
				return false;
		}
		else
		{
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $strUrl);
			curl_setopt($ch, CURLOPT_HEADER, TRUE);
			curl_setopt($ch, CURLOPT_VERBOSE, TRUE);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_TIMEOUT, 5);

			$text = curl_exec($ch);
			$errno = curl_errno($ch);
			$errstr = curl_error($ch);
			curl_close($ch);

			if($errno)
				return false;
		}

		if(LANG_CHARSET != 'UTF-8')
			$text = iconv("UTF-8", SITE_CHARSET, $text);

		$arData_ = CAltasibGeoBaseIPTools::ParseXML($text);
		if(isset($arData_["Error"]) || empty($arData_))
			return false;

		$arData = Array(
			"IP_ADDR" => $ip,
			"COUNTRY" => $arData_["Country"],
			"CITY_NAME" => $arData_["Town"],
			"REGION_NAME" => $arData_["Region"],
			"BREADTH_CITY" => $arData_["Lat"],
			"LONGITUDE_CITY" => $arData_["Lon"],
			"TIME_ZONE" => $arData_["TZ"]
		);

		return ($arData);
	}

	function CheckServiceAccess($address) //Check for availability of the service
	{
		stream_context_set_default(
			array(
				'http' => array(
					'method' => 'HEAD',
					'timeout' => 7
				)
			)
		);
		$headers = @get_headers($address);
		if($headers === false)
		{
			$headers = self::get_headers_curl($address);
			if($headers === false)
				return "Not connection";
		}

		if(preg_match("/200/", $headers[0]))
			return true;

		return false;
	}

	function get_headers_curl($url)
	{
		if(!function_exists('curl_init'))
			return false;

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL,			$url);
		curl_setopt($ch, CURLOPT_HEADER,		true);
		curl_setopt($ch, CURLOPT_NOBODY,		true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_TIMEOUT,		15);

		$r = curl_exec($ch);
		$r = split("\n", $r);
		return $r;
	}

	public function getUserHostIP()
	{
		if(!empty($_SERVER['HTTP_X_REAL_IP']))	//check ip from share internet
		{
			$ip = $_SERVER['HTTP_X_REAL_IP'];
		}
		elseif(!empty($_SERVER['HTTP_CLIENT_IP']))	//check ip from share internet
		{
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		}
		elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR']))	//to check ip is pass from proxy
		{
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else
		{
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}
}

if(method_exists(CModule, "AddAutoloadClasses")){
	CModule::AddAutoloadClasses(
		"altasib.geobase",
		$arClassesList
	);
} else {
	foreach($arClassesList as $sClassName => $sClassFile){
		require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/altasib.geobase/".$sClassFile);
	}
}
?>