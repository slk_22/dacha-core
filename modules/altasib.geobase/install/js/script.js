if(typeof altasib_geobase=="undefined")
	var altasib_geobase={};

if(typeof BX!='undefined'){
	BX.ready(function(){
		$(document).ready(function(){
			altasib_geobase.parse_city();
			setTimeout('altasib_geobase.replace()',1000)
		});
	});
	BX.addCustomEvent("onAjaxSuccess",function(par1,par2,par3){
		if(typeof par1=='undefined'&&typeof par2=='undefined'&&typeof par3=='undefined')
			altasib_geobase.timeoutId=setTimeout('altasib_geobase.chrun()',300);
	});
}
else{
	$(document).ready(function(){
		altasib_geobase.parse_city();
		setTimeout('altasib_geobase.replace()',1000)
	});
}

altasib_geobase.replace_handler=null;

altasib_geobase.replace=function(){
	var country='',city='',region='';

	if(typeof altasib_geobase.city!="undefined")
		city=altasib_geobase.city;
	if(typeof altasib_geobase.region!="undefined"&&altasib_geobase.region!="undefined"&&altasib_geobase.region!="undefined undefined")
		region=altasib_geobase.region;

	if(typeof altasib_geobase.country!="undefined")
		country=altasib_geobase.country;
	else if(typeof altasib_geobase.def_location!="undefined")
		country=altasib_geobase.def_location;
	else
		country='';

	var pt=$('input[name="PERSON_TYPE"][checked="checked"]').attr('value');
	if(typeof pt=='undefined'||pt=='null'||pt=='')pt=1;

	if(typeof altasib_geobase.pt_vals!='undefined')
		var fieldName=altasib_geobase.pt_vals[pt];

	if(typeof fieldName=='undefined'&&typeof altasib_geobase.pt!='undefined'){
		if(typeof altasib_geobase.pt[0]!='undefined')
			var fieldName=altasib_geobase.pt_vals[altasib_geobase.pt[0]];
	}

	if(typeof fieldName!='undefined'&&fieldName.length>0){
		var fLoc=$('input[name="'+fieldName+'"]');
		var fLocVal=$('#'+fieldName+'_val');

		if(typeof fLoc=='undefined'||fLoc.length==0)
			fLoc=$('#'+fieldName);

		if(typeof fLocVal=='undefined'||fLocVal.length==0)
			if(typeof fLoc!='undefined'&&fLoc.length>0)
				fLocVal=fLoc;

		if(typeof fLoc=='undefined'||fLoc.length==0)
			if(typeof fLocVal!='undefined'&&fLocVal.length>0)
				fLoc=fLocVal;

		if(fLocVal.length>0&&typeof region=="undefined"&&typeof city=="undefined"){
			if(altasib_geobase.check_value(fLocVal.val(),'1'))
				fLocVal.val(city+', '+region+', '+country);
			if(altasib_geobase.check_value(fLocVal.attr('value'),'2')){
				if(region!='')
					fLocVal.attr('value',city+', '+region+', '+country);
				else
					fLocVal.attr('value',city+', '+country);
			}
		}
	}

	if(typeof fLoc!='undefined'&&fLoc.length!=0&&altasib_geobase.check_value(fLoc.val(),'3')){

		$.ajax({
			url:window.location.protocol+'//'+window.location.host+
				'/bitrix/components/bitrix/sale.ajax.locations/search.php?search='+city.replace(/[\u0080-\uFFFF]/g,
					function(s){return "%u"+('000'+s.charCodeAt(0).toString(16)).substr(-4);}),
			data:{'params':'siteId:'+altasib_geobase.SITE_ID},
			async:false,
			success:function(out){
				if(out.length > 2){
					out=$.parseJSON(out.replace(new RegExp("'",'g'),'"'));
					if(out!==null){
						if(out.length>0)
							out=out[0];
						if(typeof out=='object'){
							if(altasib_geobase.check_value(fLoc.val(),'6')){
								fLoc.val(out['ID']);

								if(fLoc.val()!=out['ID']){
									altasib_geobase.get_loc_fields(out['ID']);
								}
								altasib_geobase.send_form();
							}
						}
					}
				}
				if(typeof altasib_geobase.bx_loc!='undefined'&&(out=='[]'||out===null||typeof out=='undefined'||out=='')){
					if(altasib_geobase.check_value(fLoc.val(),'7')){
						fLoc.val(altasib_geobase.bx_loc);altasib_geobase.send_form();
					}
				}

				var val_fLocVal=fLocVal.val();
				if(typeof val_fLocVal!='undefined'&&val_fLocVal.length==0){
					if(altasib_geobase.check_value(fLocVal.val(),'4'))
						fLocVal.val(out['NAME']+', '+out['REGION_NAME']+', '+out['COUNTRY_NAME']);
					if(altasib_geobase.check_value(fLocVal.attr('value'),'5')){
						if(out['REGION_NAME']!='')
							fLocVal.attr('value',out['NAME']+', '+out['REGION_NAME']+', '+out['COUNTRY_NAME']);
						else
							fLocVal.attr('value',out['NAME']+', '+out['COUNTRY_NAME']);
					}
				}
			}
		});
	}

	if(typeof altasib_geobase.pt=='undefined')return;

	altasib_geobase.replace_handler = function(e){
		if(typeof e!='undefined'){
			if(typeof altasib_geobase.changeTimeStamp=='undefined')
				altasib_geobase.changeTimeStamp=e.timeStamp;
			else if(e.timeStamp==altasib_geobase.changeTimeStamp)
				return;
		}

		var itr=5;
		var interval=setInterval(function(){
			if(typeof altasib_geobase.pt_vals!='undefined'&&typeof e!='undefined')
				var lfield=altasib_geobase.pt_vals[$(e.target).attr('value')];
			else if(typeof altasib_geobase.pt[0]!='undefined')
				var lfield=altasib_geobase.pt_vals[altasib_geobase.pt[0]];
			var lfieldId=lfield;

			if(lfieldId[0]!="#")
				lfieldId='#'+lfieldId;

			var fLocVal=$(lfieldId+'_val');
			if(typeof fLocVal=='undefined'||fLocVal.length==0)
				fLocVal=$('input[name="'+lfield+'"]');

			var oLoc=$(lfieldId);
			if(typeof oLoc=='undefined'||oLoc.length==0)
				oLoc=$('input[name="'+lfield+'"]');

			var locId=lfield.split('_');
			if(typeof locId=='object'&&locId.length>0){
				var ctrProp=$('[name=COUNTRYORDER_PROP_'+locId[locId.length-1]+']');
			}

			if((oLoc.length>0&&altasib_geobase.check_value(oLoc.val(),'8'))||(typeof ctrProp!='undefined'&&ctrProp.length>0)){

				$.ajax({
					url:window.location.protocol+'//'+window.location.host+
						'/bitrix/components/bitrix/sale.ajax.locations/search.php?search='+city.replace(/[\u0080-\uFFFF]/g,
							function(s){return "%u"+('000'+s.charCodeAt(0).toString(16)).substr(-4);}),
					data:{'params':'siteId:'+altasib_geobase.SITE_ID},
					async:false,
					success:function(out){
						var tmp=out.split('NAME'),townId=tmp[0].split("'")[3];

						if(typeof oLoc=='undefined'||oLoc.length==0){
							var inpLoc=$('input[name="'+lfield+'"]');
							if(altasib_geobase.check_value(inpLoc.val(),'9'))
								inpLoc.val(townId);
						}else{
							if(altasib_geobase.check_value(oLoc.val(),'10'))
								oLoc.val(townId);
						}

						clearInterval(interval);
						if(altasib_geobase.timeoutId!='undefined')
							clearTimeout(altasib_geobase.timeoutId);

						if(out!='[]')
							eval("out="+out+";");

						if(typeof fLocVal=='undefined'||fLocVal.length==0)
							fLocVal=$('input[name="'+lfield+'"]');

						if(out==null)
							out=$.parseJSON(out.replace(new RegExp("'",'g'),'"'));
						if(out!==null&&out.length>2&&altasib_geobase.check_value(fLocVal.attr('value'),'11')){
							if(out['REGION_NAME']!= '')
								fLocVal.attr('value',out[0]['NAME']+', '+out[0]['REGION_NAME']+', '+out[0]['COUNTRY_NAME']);
							else
								fLocVal.attr('value',out[0]['NAME']+','+out[0]['COUNTRY_NAME']);
						}

						if(typeof ctrProp!='undefined'){
							if(typeof ctrProp!='undefined'&&ctrProp.length>0)
								altasib_geobase.get_loc_fields(townId);
						}

						if(typeof altasib_geobase.bx_loc!='undefined'&&(out=='[]'||out===null||typeof out=='undefined'||out=='')){
							if(typeof oLoc=='undefined'||oLoc.length==0){
								var inputLoc=$('input[name="'+lfield+'"]');
								if(altasib_geobase.check_value(inputLoc.val(),'12'))
									inputLoc.val(altasib_geobase.bx_loc);
							}else{
								if(altasib_geobase.check_value(oLoc.val(),'13'))
									oLoc.val(altasib_geobase.bx_loc);
							}
						}
						altasib_geobase.send_form();
					}
				});
			}
			else if(--itr <= 0){
				clearInterval(interval);
			}

			if(fLocVal.length>0){
				if(altasib_geobase.check_value(fLocVal.val(),'14'))
					fLocVal.val(city+', '+region+', '+country);
				if(altasib_geobase.check_value(fLocVal.attr('value'),'15')){
					if(region!='')
						fLocVal.attr('value',city+', '+region+', '+country);
					else
						fLocVal.attr('value',city+', '+country);
				}
			}

		},1900);
	}

	$('body').on('change','input[name="PERSON_TYPE"]',function(e){
		altasib_geobase.replace_handler(e);
	});
}

altasib_geobase.parse_city=function(){
	if((altasib_geobase.manual_code=altasib_geobase.getCookie(altasib_geobase.COOKIE_PREFIX+'_'+'ALTASIB_GEOBASE_CODE'))!==null)
		altasib_geobase.manual_code=$.parseJSON(decodeURIComponent(altasib_geobase.manual_code.replace(/\+/g," ")));

	if((altasib_geobase.auto_code=altasib_geobase.getCookie(altasib_geobase.COOKIE_PREFIX+'_'+'ALTASIB_GEOBASE'))!==null)
		altasib_geobase.auto_code=$.parseJSON(decodeURIComponent(altasib_geobase.auto_code.replace(/\+/g," ")));

	if(altasib_geobase.manual_code!==null){
		if(typeof altasib_geobase.manual_code['CITY_RU']!='undefined')
			altasib_geobase.city=altasib_geobase.manual_code['CITY_RU'];
		else if(typeof altasib_geobase.manual_code['CITY']!='undefined'){
			if(typeof altasib_geobase.manual_code['CITY']['NAME']!='undefined')
				altasib_geobase.city=altasib_geobase.manual_code['CITY']['NAME'];
			else if(typeof altasib_geobase.manual_code['CITY']=='string')
				altasib_geobase.city=altasib_geobase.manual_code['CITY'];
		}
		else if(typeof altasib_geobase.manual_code['CITY_NAME']!='undefined')
			altasib_geobase.city=altasib_geobase.manual_code['CITY_NAME'];

		if(typeof altasib_geobase.manual_code['REGION']!='undefined'){
			if(typeof altasib_geobase.manual_code['REGION']['NAME']!='undefined')
				altasib_geobase.region=altasib_geobase.manual_code['REGION']['NAME']+' '
				+(typeof altasib_geobase.manual_code['REGION']['SOCR']!='undefined' ?
					altasib_geobase.manual_code['REGION']['SOCR']:'');
			else if(typeof altasib_geobase.manual_code['REGION']=='string')
				altasib_geobase.region=altasib_geobase.manual_code['REGION'];
		}
		else if(typeof altasib_geobase.manual_code['REGION_NAME']!='undefined')
			altasib_geobase.region=altasib_geobase.manual_code['REGION_NAME'];

	}else if(altasib_geobase.auto_code!==null){
		altasib_geobase.city=altasib_geobase.auto_code['CITY_NAME'];
		altasib_geobase.region=altasib_geobase.auto_code['REGION_NAME'];
	}
}

altasib_geobase.getCookie=function(name){
	var nameEQ=name+'=';
	var ca=document.cookie.split(';');
	for(var i=0;i<ca.length;i++){
		var c=ca[i];
		while(c.charAt(0)==' ')
			c=c.substring(1,c.length);
		if(c.indexOf(nameEQ)==0)
			return c.substring(nameEQ.length,c.length);
	}
	return null;
}
altasib_geobase.send_form=function(){
	if(typeof submitForm!='undefined'&&typeof submitForm=='function'){
		if(altasib_geobase.is_mobile){
			if(!$('div#altasib_geobase_mb_popup').is(':visible')&&!altasib_geobase.sc_is_open&&!$('div#altasib_geobase_mb_window').is(':visible'))
				submitForm();
		}else{
			if(!$('div#altasib_geobase_popup').is(':visible')&&!altasib_geobase.sc_is_open&&!$('div#altasib_geobase_window').is(':visible'))
				submitForm();
		}
	}
}

altasib_geobase.get_loc_fields=function(input_val){
	if(typeof input_val=='undefined'||input_val==null||input_val=='')
		return;
	var arLocs=[];
	if(typeof(altasib_geobase.pt)!='undefined'&&typeof(altasib_geobase.pt_vals)!='undefined'){
		for(var i=0,len=altasib_geobase.pt.length;i<len;++i)
			arLocs.push(altasib_geobase.pt_vals[altasib_geobase.pt[i]]);
	}
	else if(typeof(altasib_geobase.field_loc_ind)!='undefined'&&typeof(altasib_geobase.field_loc_leg)!='undefined')
		arLocs=[altasib_geobase.field_loc_ind,altasib_geobase.field_loc_leg];

	for(var key in arLocs){
		var loc=arLocs[key];
		var locId=loc.split('_');
		if(typeof locId=='object'&&locId.length > 0){
			var prNum=locId[locId.length-1];
			var ctrProp=$('[name=COUNTRYORDER_PROP_'+prNum+']');

			if(typeof ctrProp=='undefined'||ctrProp.length <= 0)
				continue;

			ctrProp.prop("disabled",true).parent().append('<input type="hidden" name="'+loc+'" value="'+input_val+'">')
				.children('select').prop("disabled",true);
		}
	}
	return;
}

altasib_geobase.chrun=function(){
	if(altasib_geobase.timeoutId!='undefined')
		clearTimeout(altasib_geobase.timeoutId);

	if(typeof altasib_geobase.replace_handler=='function')
		altasib_geobase.replace_handler();
}
altasib_geobase.check_value=function(value,source){
	var res=false;

	if(typeof value=='undefined'||value==null||value==''||value.length==0)
		res=true;

	if(!res&&typeof value=='string'&&$.trim(value)=='')
		res=true;

	if(!res){
		for(var key in altasib_geobase.pv_default){
			if(value==altasib_geobase.pv_default[key])
				res=true;break;
		}
	}
	return res;
}