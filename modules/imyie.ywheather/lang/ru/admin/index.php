<?
$MESS["IMYIE_MODULES_PAGE_TITLE"] = "Список модулей";
$MESS["IMYIE_MODULES_PAGE_DESCRIPTION"] = "Данный модуль является основным для работы всех остальных модулей с партнерским кодом <strong>imyie</strong>.<br />Удаление/Установка выполняется на стандартной странице <a href=\"/bitrix/admin/partner_modules.php\" target=\"_blank\">списка доступных решений</a>.";
$MESS["IMYIE_MODULES_TH1"] = "Код модуля";
$MESS["IMYIE_MODULES_TH2"] = "Имя модуля";
$MESS["IMYIE_MODULES_TH3"] = "Партнер";
$MESS["IMYIE_MODULES_TH4"] = "Версия";
$MESS["IMYIE_MODULES_TH5"] = "Дата версии";
$MESS["IMYIE_MODULES_TH6"] = "Статус";
$MESS["IMYIE_MODULES_ACTION_EDIT"] = "Редактирование";
$MESS["IMYIE_MODULES_Y_INSTALLED"] = "Установлен";
$MESS["IMYIE_MODULES_N_INSTALLED"] = "Не установлен";
?>