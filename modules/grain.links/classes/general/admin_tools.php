<?

class CGrain_LinksAdminTools {

	function GetDataSourceList($bProperties=false) {
	
		if(!$bProperties) {
			$arDataSourcesTmp = Array(
			    "array_simple"=>Array("NAME"=>GetMessage("GRAIN_LINKS_DATA_SOURCE_ARRAY_SIMPLE"),"SORT"=>100),
			    "array_extended"=>Array("NAME"=>GetMessage("GRAIN_LINKS_DATA_SOURCE_ARRAY_EXTENDED"),"SORT"=>200),
			    "array_bitrix"=>Array("NAME"=>GetMessage("GRAIN_LINKS_DATA_SOURCE_ARRAY_BITRIX"),"SORT"=>300),
			    "html_select"=>Array("NAME"=>GetMessage("GRAIN_LINKS_DATA_SOURCE_HTML_SELECT"),"SORT"=>400),
			);
		} else {
			$arDataSourcesTmp = Array();
		}
		
		$arDataSourcesTmp = self::GetDataSourceListFromFolder(GRAIN_LINKS_SYSTEM_LIST_HANDLERS,$arDataSourcesTmp);
		$arDataSourcesTmp = self::GetDataSourceListFromFolder(GRAIN_LINKS_USER_LIST_HANDLERS,$arDataSourcesTmp);
		
		$arDataSourcesTmp = self::SortArrayByKeyDecimal($arDataSourcesTmp,"SORT","ASC");
		
		$arDataSources = Array();
		foreach($arDataSourcesTmp as $file=>$arDataSource)
			$arDataSources[$file] = $arDataSource["NAME"];
		
		return $arDataSources;
	
	}


	function GetDataSourceListFromFolder($FOLDER,$arDataSourcesTmp) {
	
		$arFilesList = @scandir($_SERVER["DOCUMENT_ROOT"].$FOLDER);
		if(is_array($arFilesList)) {
			foreach($arFilesList as $file) {
				if(!in_array($file,Array(".","..")) && is_dir($_SERVER["DOCUMENT_ROOT"].$FOLDER."/".$file)) {
					$arListDescription=Array();
					if(is_file($_SERVER["DOCUMENT_ROOT"].$FOLDER."/".$file."/.description.php")) {
						__IncludeLang($_SERVER["DOCUMENT_ROOT"].$FOLDER."/".$file."/lang/".LANGUAGE_ID."/.description.php");
						require($_SERVER["DOCUMENT_ROOT"].$FOLDER."/".$file."/.description.php");
					}
					if($bProperties && $arListDescription["USE_IN_PROPERTIES"]!="Y") continue;
					if(is_array($arListDescription["REQUIRED_MODULES"]))
						foreach($arListDescription["REQUIRED_MODULES"] as $module)
							if(strlen($module)>0 && !IsModuleInstalled($module))
								goto grain_filelist_loop_end;
					if(!array_key_exists("SORT",$arListDescription))
						$arListDescription["SORT"]="500";
					if(!array_key_exists("NAME",$arListDescription) || (array_key_exists("NAME",$arListDescription) && strlen($arListDescription["NAME"])<=0))
						$arListDescription["NAME"]=$file;
					$arDataSourcesTmp[$file] = $arListDescription;
				}
				grain_filelist_loop_end:
			}
		}
		return $arDataSourcesTmp;
	
	}


	function SortArrayByKeyDecimal($array,$key,$order,$maintain_index=true) {
	
		if(!is_array($array)) return $array;

		if($maintain_index) {
			uasort($array,create_function(
				'$a,$b',
				'return floatval($a["'.$key.'"])'.(strtolower($order)=="desc"?"<":">").'floatval($b["'.$key.'"]);'
			));
		} else {
			usort($array,create_function(
				'$a,$b',
				'return floatval($a["'.$key.'"])'.(strtolower($order)=="desc"?"<":">").'floatval($b["'.$key.'"]);'
			));
		}
	
		return $array;
	
	}

	
	function ShowDataSourceParams($data_source,$name_prefix,$arCurrentValues,$jsFunctionPrefix="grain_links") {
	
		if(strlen($data_source)<=0) return;

		$arComponentParameters = Array("PARAMETERS"=>Array());
		
		if(is_file($_SERVER["DOCUMENT_ROOT"].GRAIN_LINKS_USER_LIST_HANDLERS."/".$data_source."/.parameters.php")) {
		    __IncludeLang($_SERVER["DOCUMENT_ROOT"].GRAIN_LINKS_USER_LIST_HANDLERS."/".$data_source."/lang/".LANGUAGE_ID."/.parameters.php");
		    require($_SERVER["DOCUMENT_ROOT"].GRAIN_LINKS_USER_LIST_HANDLERS."/".$data_source."/.parameters.php");
		} elseif(is_file($_SERVER["DOCUMENT_ROOT"].GRAIN_LINKS_SYSTEM_LIST_HANDLERS."/".$data_source."/.parameters.php")) {
			__IncludeLang($_SERVER["DOCUMENT_ROOT"].GRAIN_LINKS_SYSTEM_LIST_HANDLERS."/".$data_source."/lang/".LANGUAGE_ID."/.parameters.php");
			require($_SERVER["DOCUMENT_ROOT"].GRAIN_LINKS_SYSTEM_LIST_HANDLERS."/".$data_source."/.parameters.php");
		}

		if(count($arComponentParameters["PARAMETERS"])<=0) return;

		if(!is_array($arCurrentValues)) {
			$arCurrentValues = Array();
			foreach($arComponentParameters["PARAMETERS"] as $NAME => $arParameter)
				if(array_key_exists("DEFAULT",$arParameter))
					$arCurrentValues[$NAME] = $arParameter["DEFAULT"];
		}
		
		?><table class="grain-links-dsparams"><?

		foreach($arComponentParameters["PARAMETERS"] as $NAME => $arParameter) {
		
			?><tr><td class="grain-links-dsparams-col-name"><?=$arParameter["NAME"]?></td><td class="grain-links-dsparams-col-input"><?
		
			switch($arParameter["TYPE"]) {
			
				case "LIST":
				
					?><select name="<?=$name_prefix?>[<?=$NAME?>]<?if($arParameter["MULTIPLE"]=="Y"):?>[]<?endif?>"<?if($arParameter["MULTIPLE"]=="Y"):?> multiple="multiple"<?endif;?> onchange="<?if($arParameter["ADDITIONAL_VALUES"]=="Y" && $arParameter["MULTIPLE"]!="Y"):?>window.<?=$jsFunctionPrefix?>_dsparams_addvalues_change(this);<?endif;if($arParameter["REFRESH"]=="Y"):?>window.<?=$jsFunctionPrefix?>_dsparams_refresh(false,'<?=$name_prefix?>');<?endif?>"><?
						if($arParameter["ADDITIONAL_VALUES"]=="Y" && $arParameter["MULTIPLE"]!="Y") {
							?><option value=""><?=GetMessage("GRAIN_LINKS_PROP_SETTINGS_ADDITIONAL_VALUES")?></option><?
						}
						if(is_array($arParameter["VALUES"])) foreach($arParameter["VALUES"] as $value=>$value_name) {
							?><option value="<?=htmlspecialchars($value)?>"<?if(($arParameter["MULTIPLE"]=="Y" && in_array($value,$arCurrentValues[$NAME])) || ($arParameter["MULTIPLE"]!="Y" && $value==$arCurrentValues[$NAME])):?> selected="selected"<?endif?>><?=$value_name?></option><?						
						}
					?></select><?
					if($arParameter["ADDITIONAL_VALUES"]=="Y" && $arParameter["MULTIPLE"]!="Y") {
						?><input id="<?=$jsFunctionPrefix?>_dspadd_<?=$name_prefix?>[<?=$NAME?>]" type="text"<?if(strlen($arCurrentValues[$NAME])>0 && array_key_exists($arCurrentValues[$NAME],$arParameter["VALUES"])):?> disabled="disabled"<?else:?> name="<?=$name_prefix?>[<?=$NAME?>]" value="<?=htmlspecialchars($arCurrentValues[$NAME])?>"<?endif?> /><?
					} elseif($arParameter["ADDITIONAL_VALUES"]=="Y" && $arParameter["MULTIPLE"]=="Y") {
						?><div class="grain-links-dsparams-multiple"><?
						if(is_array($arCurrentValues[$NAME])) foreach($arCurrentValues[$NAME] as $value) {
							if((strlen($value)>0 && array_key_exists($value,$arParameter["VALUES"])) || strlen($value)<=0) continue;
							?><input type="text" name="<?=$name_prefix?>[<?=$NAME?>][]" value="<?=htmlspecialchars($value)?>" /><?
						}
						?><input type="text" name="<?=$name_prefix?>[<?=$NAME?>][]" /><?
						?></div><?
						?><input value="+" type="button" onclick="window.<?=$jsFunctionPrefix?>_dsparams_addvalues_add(this);" /><?
					}
					
				
				break;
				
				case "CHECKBOX":
				
					?><input type="checkbox" name="<?=$name_prefix?>[<?=$NAME?>]" value="Y"<?if($arParameter["REFRESH"]=="Y"):?> onchange="window.<?=$jsFunctionPrefix?>_dsparams_refresh(false,'<?=$name_prefix?>');"<?endif;if($arCurrentValues[$NAME]=="Y"):?> checked="checked"<?endif?> /><?
				
				break;
				
				case "STRING":
				case "CUSTOM":
					if($arParameter["MULTIPLE"]!="Y") {
						?><input type="text" name="<?=$name_prefix?>[<?=$NAME?>]" value="<?=htmlspecialchars($arCurrentValues[$NAME])?>" /><?
					} else {
						?><div class="grain-links-dsparams-multiple"><?
						if(is_array($arCurrentValues[$NAME])) foreach($arCurrentValues[$NAME] as $value) {
							if(strlen($value)<=0) continue;
							?><input type="text" name="<?=$name_prefix?>[<?=$NAME?>][]" value="<?=htmlspecialchars($value)?>" /><?
						}
						?><input type="text" name="<?=$name_prefix?>[<?=$NAME?>][]" /><?
						?></div><?
						?><input value="+" type="button" onclick="window.<?=$jsFunctionPrefix?>_dsparams_addvalues_add(this);" /><?
					}
				break;
			
			}
			
			?></td></tr><?
		
		}
		
		?></table><?
	
	}

}

?>