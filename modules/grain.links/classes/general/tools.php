<?

class CGrain_LinksTools {

	function GetSelected($arParams,$selected_value,$bParamsPrepared=false) {

		if(!$bParamsPrepared)
			$arParams = self::PrepareParams($arParams);

		if(strlen($arParams["DATA_SOURCE"])<=0)
			return Array();
	
		if(!is_array($selected_value) && strlen($selected_value)<=0)
			return Array();
			
		if(is_array($selected_value)) {
			$selected_value_tmp = $selected_value;
			$selected_value = Array();
			foreach($selected_value_tmp as $value) 
				if(strlen($value)>0)
					$selected_value[] = $value;
			if(count($selected_value)<=0)
				return Array();
		}
	
		$arResult = Array();
		$arResult["DATA"] = Array();
		$arResult["SELECTED_VALUE"] = $selected_value;
		
		if(is_file($_SERVER["DOCUMENT_ROOT"].GRAIN_LINKS_USER_LIST_HANDLERS."/".$arParams["DATA_SOURCE"]."/list.php"))	{
			__IncludeLang($_SERVER["DOCUMENT_ROOT"].GRAIN_LINKS_USER_LIST_HANDLERS."/".$arParams["DATA_SOURCE"]."/lang/".LANGUAGE_ID."/list.php");
			require($_SERVER["DOCUMENT_ROOT"].GRAIN_LINKS_USER_LIST_HANDLERS."/".$arParams["DATA_SOURCE"]."/list.php");
		} elseif(is_file($_SERVER["DOCUMENT_ROOT"].GRAIN_LINKS_SYSTEM_LIST_HANDLERS."/".$arParams["DATA_SOURCE"]."/list.php")) {
			__IncludeLang($_SERVER["DOCUMENT_ROOT"].GRAIN_LINKS_SYSTEM_LIST_HANDLERS."/".$arParams["DATA_SOURCE"]."/lang/".LANGUAGE_ID."/list.php");
			require($_SERVER["DOCUMENT_ROOT"].GRAIN_LINKS_SYSTEM_LIST_HANDLERS."/".$arParams["DATA_SOURCE"]."/list.php");
		}

		return $arResult["DATA"];
	
	}

	function ParseDataArray($data_source,$source=false,$arData=false) {
	
		if($data_source!="html_select" && !is_array($source))
			return Array();

		if($data_source=="array_bitrix" && (!is_array($source["REFERENCE_ID"]) || !is_array($source["REFERENCE"])))
			return Array();

		if(!is_array($arData))
			$arData = Array();
		
		switch($data_source) {
		
			case "array_simple":
			
				foreach($source as $k=>$v) {
						
					$arData[$k] = Array(
						"NAME" => $v,
					);
					
				}
			
			break;
			
			case "array_extended":
		
				foreach($source as $k=>$v) {
						
					$arData[$v["VALUE"]] = Array(
						"NAME" => $v["NAME"],
						"URL" => $v["URL"],
					);
					
				}
		
			break;
			
			case "array_bitrix":

				foreach($source["REFERENCE_ID"] as $k=>$v) {
						
					$arData[$v] = Array(
						"NAME" => array_key_exists($k,$source["REFERENCE"])?$source["REFERENCE"][$k]:$v,
					);
					
				}
			
			break;
			
			case "html_select":
						
				global $GRAIN_LINKS_TOOLS_SOURCE;
				$GRAIN_LINKS_TOOLS_SOURCE = Array();
			
				$source = preg_replace_callback(
					'/<option.+?value\=["\']{1}(.+?)["\']{1}.*?>(.+?)<\/option>/i',
					create_function(
						'$matches',
						'global $GRAIN_LINKS_TOOLS_SOURCE; $GRAIN_LINKS_TOOLS_SOURCE[$matches[1]] = Array("NAME"=>$matches[2]); return $matches[0];'
					),
					$source
				);
				
				$arData = $GRAIN_LINKS_TOOLS_SOURCE;
				unset($GRAIN_LINKS_TOOLS_SOURCE);

			break;
		
		}
		
		return $arData;
	
	}


	function PrepareParams($arParams) {
	
        if(!is_array($arParams))
            return $arParams;

        $arParamsNew = $arParams;
        foreach($arParams as $k=>$v) {
            $arParamsNew["~".$k] = $v;
            if (isset($v) && is_string($v)) {
                if (preg_match("/[;&<>\"]/", $v))
                    $arParamsNew[$k] = htmlspecialcharsEx($v);
			} elseif(isset($v) && is_array($v)) {
                    $arParamsNew[$k] = htmlspecialcharsEx($v);
			}
        }
		
		return $arParamsNew;
	
	}
	
}

?>