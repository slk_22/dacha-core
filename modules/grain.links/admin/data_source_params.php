<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

if(CModule::IncludeModule("compression"))
	CCompress::Disable2048Spaces();

if(CModule::IncludeModule("grain.links")) {
		
	$val = false;
	if(strlen($_REQUEST["NAME_PREFIX"])>0 && array_key_exists($_REQUEST["NAME_PREFIX"],$_POST))
		$val = $_POST[$_REQUEST["NAME_PREFIX"]];

	if(strlen($_REQUEST["DATA_SOURCE"])>0) {
		if(strlen($_REQUEST["MODULE_JS_PREFIX"])>0)
			CGrain_LinksAdminTools::ShowDataSourceParams($_REQUEST["DATA_SOURCE"],$_REQUEST["NAME_PREFIX"],$val,$_REQUEST["MODULE_JS_PREFIX"]);
		else
			CGrain_LinksAdminTools::ShowDataSourceParams($_REQUEST["DATA_SOURCE"],$_REQUEST["NAME_PREFIX"],$val);
	}

} else echo "grain.links module not installed";

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin_after.php");?>