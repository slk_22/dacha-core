<?

$MESS["GRAIN_LINKS_PROP_NAME"] = "Universal links";

$MESS ['GRAIN_LINKS_PROP_SETTINGS_HEADER'] = "Data and interface";
$MESS ['GRAIN_LINKS_PROP_SETTINGS_HEADER_DATA_SOURCE'] = "Data source";
$MESS ['GRAIN_LINKS_PROP_SETTINGS_HEADER_INTERFACE'] = "Interface";
$MESS ['GRAIN_LINKS_PROP_SETTINGS_HEADER_TEMPLATES'] = "Custom edit template (grain:links.edit component)";

$MESS ['GRAIN_LINKS_PROP_SETTINGS_ADDITIONAL_VALUES'] = "(other)";

$MESS ['GRAIN_LINKS_PROP_SETTINGS_INTERFACE_TYPE'] = "Interface type";
$MESS ['GRAIN_LINKS_PROP_SETTINGS_INTERFACE_AJAX'] = "Use ajax";
$MESS ['GRAIN_LINKS_PROP_SETTINGS_INTERFACE_SELECT'] = "Select from list";
$MESS ['GRAIN_LINKS_PROP_SETTINGS_INTERFACE_SELECTSEARCH'] = "Select from list with search";
$MESS ['GRAIN_LINKS_PROP_SETTINGS_INTERFACE_SEARCH'] = "Just search";
$MESS ['GRAIN_LINKS_PROP_SETTINGS_INTERFACE_SHOW_URL'] = "Show element links";

$MESS ['GRAIN_LINKS_PROP_SETTINGS_PUBLIC_EDIT_TEMPLATE'] = "For public section";
$MESS ['GRAIN_LINKS_PROP_SETTINGS_ADMIN_EDIT_TEMPLATE'] = "For admin section";

$MESS ['GRAIN_LINKS_DATA_SOURCE_ARRAY_SIMPLE'] = "Simple array";
$MESS ['GRAIN_LINKS_DATA_SOURCE_ARRAY_EXTENDED'] = "Extended array";
$MESS ['GRAIN_LINKS_DATA_SOURCE_ARRAY_BITRIX'] = "Bitrix standard array";
$MESS ['GRAIN_LINKS_DATA_SOURCE_HTML_SELECT'] = "Select tag html code";

?>