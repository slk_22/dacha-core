<?

if(CModule::IncludeModule("learning")) {

	/* prepare parameters */
	
	$arCourses = $arParams["HIDE_COURSES"];
	$arParams["HIDE_COURSES"] = Array();
	foreach($arCourses as $course_id)
	    if(intval($course_id)>0)
	    	$arParams["HIDE_COURSES"][] = intval($course_id);

	$arParams["ACTIVE"] = $arParams["ACTIVE"]=="Y";
	$arParams["CHECK_PERMISSIONS"] = $arParams["CHECK_PERMISSIONS"]=="Y";
	
	$arParams["SORT_BY1"] = trim($arParams["SORT_BY1"]);
	if(strlen($arParams["SORT_BY1"])<=0)
		$arParams["SORT_BY1"] = "NAME";
	$arParams["SORT_ORDER1"] = strtoupper($arParams["SORT_ORDER1"]);
	if($arParams["SORT_ORDER1"]!="DESC")
		 $arParams["SORT_ORDER1"]="ASC";
	if(strlen($arParams["SORT_BY2"])<=0)
		$arParams["SORT_BY2"] = "ID";
	$arParams["SORT_ORDER2"] = strtoupper($arParams["SORT_ORDER2"]);
	if($arParams["SORT_ORDER2"]!="ASC")
		 $arParams["SORT_ORDER2"]="DESC";
	    
	/* build list */

	$arSort = array(
		$arParams["SORT_BY1"]=>$arParams["SORT_ORDER1"],
		$arParams["SORT_BY2"]=>$arParams["SORT_ORDER2"],
	);
	if(!array_key_exists("ID", $arSort))
		$arSort["ID"] = "DESC";
	
	$arFilter = Array();
	if($arParams["HIDE_COURSES"]) 
		$arFilter["!ID"] = $arParams["HIDE_COURSES"];

	if($arParams["ACTIVE"])
		$arFilter["ACTIVE"] = "Y";
	$arFilter["CHECK_PERMISSIONS"] = $arParams["CHECK_PERMISSIONS"]?"Y":"N";

	if(!$arParams["ADMIN_SECTION"])
		$arFilter["SITE_ID"]=SITE_ID;
	
	if($arResult["AJAX_RETURN"]) $arFilter["NAME"] = "%".$arResult["AJAX_SEARCH_QUERY"]."%";
	
	if($arResult["SELECTED_VALUE"]) $arFilter["ID"] = $arResult["SELECTED_VALUE"];

	$rsCourses = CCourse::GetList($arSort,$arFilter);
	
	while($arCourse=$rsCourses->GetNext()) {
	
	    $arItem = Array(
	    	"NAME" => $arCourse["NAME"],
	    );
	    
	    if($arParams["SHOW_URL"] && $arParams["COURSE_URL"]) {
	    
	    	$arCourse["SITE_SERVER_NAME"] = defined("SITE_SERVER_NAME")?SITE_SERVER_NAME:"";
	    	$arCourse["SITE_ID"] = defined("SITE_ID")?SITE_ID:"";
	    	$arCourse["SITE_DIR"] = defined("SITE_DIR")?SITE_DIR:"";
	    	
	    	$arItem["URL"] = $arParams["COURSE_URL"];
	    	foreach($arCourse as $FIELD_NAME=>$FIELD_VALUE)
	    		if(substr($FIELD_NAME,0,1)!="~")
	    			$arItem["URL"] = str_replace("#".$FIELD_NAME."#",$FIELD_VALUE,$arItem["URL"]);
	
	    	$arItem["URL"] = preg_replace("'/+'s", "/", $arItem["URL"]);
	    	$arItem["URL"] = htmlspecialcharsbx($arItem["URL"]);
	    	
	    }
	    
	    $arResult["DATA"][$arCourse["ID"]] = $arItem;
	    
	}
	
}
?>