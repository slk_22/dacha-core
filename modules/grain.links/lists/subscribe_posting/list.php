<?
if(CModule::IncludeModule("subscribe")) {

	/* prepare parameters */

	$arRubrics = $arParams["RUBRIC_ID"];
	$arParams["RUBRIC_ID"] = Array();
	foreach($arRubrics as $rubric_id)
	    if(intval($rubric_id)>0)
	    	$arParams["RUBRIC_ID"][] = intval($rubric_id);

	$arParams["SORT_BY1"] = trim($arParams["SORT_BY1"]);
	if(strlen($arParams["SORT_BY1"])<=0)
		$arParams["SORT_BY1"] = "STATUS";
	$arParams["SORT_ORDER1"] = strtoupper($arParams["SORT_ORDER1"]);
	if($arParams["SORT_ORDER1"]!="DESC")
		 $arParams["SORT_ORDER1"]="ASC";
	if(strlen($arParams["SORT_BY2"])<=0)
		$arParams["SORT_BY2"] = "DATE_SENT";
	$arParams["SORT_ORDER2"] = strtoupper($arParams["SORT_ORDER2"]);
	if($arParams["SORT_ORDER2"]!="ASC")
		 $arParams["SORT_ORDER2"]="DESC";
		
	/* build list */
	
	$arSort = array(
		$arParams["SORT_BY1"]=>$arParams["SORT_ORDER1"],
		$arParams["SORT_BY2"]=>$arParams["SORT_ORDER2"],
	);
	if(!array_key_exists("ID", $arSort))
		$arSort["ID"] = "DESC";
	
	$arFilter = Array();

	if(in_array($arParams["STATUS"],Array("S","D")))
		$arFilter["STATUS"] = $arParams["STATUS"];
	
	if($arParams["RUBRIC_ID"])
		$arFilter["RUB_ID"] = $arParams["RUBRIC_ID"];
	
	if($arResult["AJAX_RETURN"]) $arFilter["SUBJECT"] = $arResult["AJAX_SEARCH_QUERY"];
	
	if($arResult["SELECTED_VALUE"]) $arFilter["ID"] = $arResult["SELECTED_VALUE"];

	$cPosting = new CPosting;
	$rsPosting = $cPosting->GetList($arSort,$arFilter);
	
	while($arPosting=$rsPosting->GetNext()) {

		$arItem = Array(
			"NAME" => $arPosting["SUBJECT"],
		);
		
	    if($arParams["SHOW_URL"] && $arParams["POSTING_URL"]) {
	    
	    	$arPosting["SITE_SERVER_NAME"] = defined("SITE_SERVER_NAME")?SITE_SERVER_NAME:"";
	    	$arPosting["SITE_ID"] = defined("SITE_ID")?SITE_ID:"";
	    	$arPosting["SITE_DIR"] = defined("SITE_DIR")?SITE_DIR:"";
	    	
	    	$arItem["URL"] = $arParams["POSTING_URL"];
	    	foreach($arPosting as $FIELD_NAME=>$FIELD_VALUE)
	    		if(substr($FIELD_NAME,0,1)!="~")
	    			$arItem["URL"] = str_replace("#".$FIELD_NAME."#",$FIELD_VALUE,$arItem["URL"]);

	    	$arItem["URL"] = preg_replace("'/+'s", "/", $arItem["URL"]);
	    	$arItem["URL"] = htmlspecialcharsbx($arItem["URL"]);
	    	
	    }
		
		$arResult["DATA"][$arPosting["ID"]] = $arItem;
		
	}
	
}
?>