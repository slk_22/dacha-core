<?

if(CModule::IncludeModule("currency")) {

	/* prepare parameters */
	
	$arParams["ACTIVE"] = $arParams["ACTIVE"]=="Y";
	
	$arParams["SORT_BY"] = trim($arParams["SORT_BY"]);
	if(strlen($arParams["SORT_BY"])<=0)
	    $arParams["SORT_BY"] = "SORT";
	$arParams["SORT_ORDER"] = strtoupper($arParams["SORT_ORDER"]);
	if($arParams["SORT_ORDER"]!="DESC")
	     $arParams["SORT_ORDER"]="ASC";
	    
	/* build list */
	
	$rsCurrencies = CCurrency::GetList($by=$arParams["SORT_BY"],$order=$arParams["SORT_ORDER"],LANGUAGE_ID);
	
	while($arCurrency=$rsCurrencies->GetNext()) {

	    $arItem = Array(
	    	"NAME" => $arCurrency["FULL_NAME"],
	    );
	    	    
	    $arResult["DATA"][$arCurrency["CURRENCY"]] = $arItem;
	    
	}

	if($arResult["AJAX_RETURN"]) {
		foreach($arResult["DATA"] as $key => $arItem) 
			if(!preg_match("/".preg_quote($arResult["AJAX_SEARCH_QUERY"])."/i".(BX_UTF?"u":""),$arItem["NAME"]))
				unset($arResult["DATA"][$key]);
	} elseif($arResult["SELECTED_VALUE"]){
		foreach($arResult["DATA"] as $key => $arItem) 
			if(
				(is_array($arResult["SELECTED_VALUE"]) && !in_array($key,$arResult["SELECTED_VALUE"]))
				|| (!is_array($arResult["SELECTED_VALUE"]) && $arResult["SELECTED_VALUE"]!=$key)
			)
				unset($arResult["DATA"][$key]);
	}

}

?>