<?

if(CModule::IncludeModule("advertising")) {

	/* prepare parameters */
	
	$arTypes = $arParams["HIDE_TYPES"];
	$arParams["HIDE_TYPES"] = Array();
	foreach($arTypes as $type_id)
	    if(strlen($type_id)>0)
	    	$arParams["HIDE_TYPES"][] = $type_id;

	$arParams["ACTIVE"] = $arParams["ACTIVE"]=="Y";
	$arParams["CHECK_PERMISSIONS"] = $arParams["CHECK_PERMISSIONS"]=="Y";
	
	$arParams["SORT_BY"] = trim($arParams["SORT_BY"]);
	if(strlen($arParams["SORT_BY"])<=0)
	    $arParams["SORT_BY"] = "S_NAME";
	$arParams["SORT_ORDER"] = strtoupper($arParams["SORT_ORDER"]);
	if($arParams["SORT_ORDER"]!="DESC")
	     $arParams["SORT_ORDER"]="ASC";
	    
	/* build list */
	
	$arFilter = Array();
	if($arParams["HIDE_TYPES"]) {
		$arFilter["SID"] = "";
		foreach($arParams["HIDE_TYPES"] as $type_id) {
			if(strlen($arFilter["SID"])>0) $arFilter["SID"] .= "&";
			$arFilter["SID"] .= "~".$type_id."~";
		}
	}

	if($arParams["ACTIVE"])
		$arFilter["ACTIVE"] = "Y";
	
	if($arResult["AJAX_RETURN"]) {
		$arFilter["NAME"] = $arResult["AJAX_SEARCH_QUERY"];
		$arFilter["NAME_EXACT_MATCH"] = "N";
	}
	
	if($arResult["SELECTED_VALUE"]) {
		$filter_id = "";
		if(is_array($arResult["SELECTED_VALUE"])) {
			foreach($arResult["SELECTED_VALUE"] as $value) {
				if(strlen($filter_id)>0) $filter_id .= "|";
				$filter_id .= $value;
			}
		} else $filter_id = $arResult["SELECTED_VALUE"];
		if($arParams["HIDE_TYPES"] && strlen($arFilter["SID"])>0) 
			$arFilter["SID"] = $arFilter["SID"]."&(".$filter_id.")";
		else
			$arFilter["SID"] = $filter_id;
	}

	$rsTypes = CAdvType::GetList(($by=$arParams["SORT_BY"]), ($order=$arParams["SORT_ORDER"]),$arFilter,$is_filtered,$arParams["CHECK_PERMISSIONS"]?"Y":"N");
	
	while($arType=$rsTypes->GetNext()) {
	
	    $arItem = Array(
	    	"NAME" => $arType["NAME"],
	    );
	    
	    if($arParams["SHOW_URL"] && $arParams["TYPE_URL"]) {
	    
	    	$arType["SITE_SERVER_NAME"] = defined("SITE_SERVER_NAME")?SITE_SERVER_NAME:"";
	    	$arType["SITE_ID"] = defined("SITE_ID")?SITE_ID:"";
	    	$arType["SITE_DIR"] = defined("SITE_DIR")?SITE_DIR:"";
	    	
	    	$arItem["URL"] = $arParams["TYPE_URL"];
	    	foreach($arType as $FIELD_NAME=>$FIELD_VALUE)
	    		if(substr($FIELD_NAME,0,1)!="~")
	    			$arItem["URL"] = str_replace("#".$FIELD_NAME."#",$FIELD_VALUE,$arItem["URL"]);
	
	    	$arItem["URL"] = preg_replace("'/+'s", "/", $arItem["URL"]);
	    	$arItem["URL"] = htmlspecialcharsbx($arItem["URL"]);
	    	
	    }
	    
	    $arResult["DATA"][$arType["SID"]] = $arItem;
	    
	}
	
}
?>