<?
if(CModule::IncludeModule("iblock")) {

	/* prepare parameters */

	$arParams["SORT_BY1"] = trim($arParams["SORT_BY1"]);
	if(strlen($arParams["SORT_BY1"])<=0)
		$arParams["SORT_BY1"] = "SORT";
	$arParams["SORT_ORDER1"] = strtoupper($arParams["SORT_ORDER1"]);
	if($arParams["SORT_ORDER1"]!="DESC")
		 $arParams["SORT_ORDER1"]="ASC";
	if(strlen($arParams["SORT_BY2"])<=0)
		$arParams["SORT_BY2"] = "NAME";
	$arParams["SORT_ORDER2"] = strtoupper($arParams["SORT_ORDER2"]);
	if($arParams["SORT_ORDER2"]!="DESC")
		 $arParams["SORT_ORDER2"]="ASC";
		
	/* build list */
	
	$arSort = array(
		$arParams["SORT_BY1"]=>$arParams["SORT_ORDER1"],
		$arParams["SORT_BY2"]=>$arParams["SORT_ORDER2"],
	);
	if(!array_key_exists("ID", $arSort))
		$arSort["ID"] = "DESC";
	
	$arFilter = Array("LANGUAGE_ID"=>LANGUAGE_ID);
	
	if($arResult["AJAX_RETURN"]) $arFilter["NAME"] = $arResult["AJAX_SEARCH_QUERY"];
	
	if($arResult["SELECTED_VALUE"]) $arFilter["=ID"] = $arResult["SELECTED_VALUE"];

	$rsIBlockTypes = CIBlockType::GetList($arSort,$arFilter);
	
	while($arIBlockType=$rsIBlockTypes->GetNext()) {

		$arItem = Array(
			"NAME" => $arIBlockType["NAME"],
		);
		
		if($arParams["SHOW_URL"]) {
		
			$arItem["URL"] = str_replace(
				array("#SERVER_NAME#", "#SITE_DIR#", "#IBLOCK_TYPE_ID#"),
				array($arParams["ADMIN_SECTION"]?"":SITE_SERVER_NAME, $arParams["ADMIN_SECTION"]?"":SITE_DIR, $arIBlockType["ID"]),
				$arParams["~IBLOCK_TYPE_URL"]
			);
			$arItem["URL"] = preg_replace("'/+'s", "/", $arItem["URL"]);
			$arItem["URL"] = htmlspecialcharsbx($arItem["URL"]);
			
		}
		
		$arResult["DATA"][$arIBlockType["ID"]] = $arItem;
		
	}
	
}
?>