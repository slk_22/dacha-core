<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arSorts = Array("ASC"=>GetMessage("GRAIN_LINKS_LIST_CATALOG_STORE_SORT_ASC"), "DESC"=>GetMessage("GRAIN_LINKS_LIST_CATALOG_STORE_SORT_DESC"));
$arSortFields = Array(
    "ID"=>GetMessage("GRAIN_LINKS_LIST_CATALOG_STORE_SORT_FID"),
    "ADDRESS"=>GetMessage("GRAIN_LINKS_LIST_CATALOG_STORE_SORT_FADDRESS"),
    "TITLE"=>GetMessage("GRAIN_LINKS_LIST_CATALOG_STORE_SORT_FNAME"),
    "SORT"=>GetMessage("GRAIN_LINKS_LIST_CATALOG_STORE_SORT_FSORT"),
);


$arComponentParameters["PARAMETERS"]["ACTIVE"] = array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_CATALOG_STORE_PARAM_ACTIVE"),
	"TYPE" => "CHECKBOX",
    "DEFAULT" => "N",
);

$arComponentParameters["PARAMETERS"]["SORT_BY1"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_CATALOG_STORE_PARAM_SORT_BY1"),
	"TYPE" => "LIST",
	"DEFAULT" => "SORT",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER1"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_CATALOG_STORE_PARAM_SORT_ORDER1"),
	"TYPE" => "LIST",
	"DEFAULT" => "ASC",
	"VALUES" => $arSorts,
);

$arComponentParameters["PARAMETERS"]["SORT_BY2"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_CATALOG_STORE_PARAM_SORT_BY2"),
	"TYPE" => "LIST",
	"DEFAULT" => "ADDRESS",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER2"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_CATALOG_STORE_PARAM_SORT_ORDER2"),
	"TYPE" => "LIST",
	"DEFAULT" => "ASC",
	"VALUES" => $arSorts,
);

?>