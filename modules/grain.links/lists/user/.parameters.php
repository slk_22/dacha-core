<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$rsGroup = CGroup::GetList(($by="c_sort"), ($order="desc"));
$arGroups = Array();
while ($arGroup = $rsGroup->GetNext())
    $arGroups[$arGroup["ID"]] = $arGroup["NAME"];

$arSorts = Array("ASC"=>GetMessage("GRAIN_LINKS_LIST_USER_SORT_ASC"), "DESC"=>GetMessage("GRAIN_LINKS_LIST_USER_SORT_DESC"));
$arSortFields = Array(
    "ID"=>GetMessage("GRAIN_LINKS_LIST_USER_SORT_FID"),
    "NAME"=>GetMessage("GRAIN_LINKS_LIST_USER_SORT_FNAME"),
    "LAST_NAME"=>GetMessage("GRAIN_LINKS_LIST_USER_SORT_FLASTNAME"),
	"LOGIN"=>GetMessage("GRAIN_LINKS_LIST_USER_SORT_FLOGIN"),
	"LAST_LOGIN"=>GetMessage("GRAIN_LINKS_LIST_USER_SORT_FLASTLOGIN"),
	"TIMESTAMP_X"=>GetMessage("GRAIN_LINKS_LIST_USER_SORT_FTSTMP"),
	"DATE_REGISTER"=>GetMessage("GRAIN_LINKS_LIST_USER_SORT_FDATEREG"),
);

$arComponentParameters["PARAMETERS"]["GROUP_ID"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_USER_PARAM_GROUP_ID"),
	"TYPE" => "LIST",
	"VALUES" => $arGroups,
	"DEFAULT" => "",
	"MULTIPLE" => "Y",
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["ACTIVE"] = array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_USER_PARAM_ACTIVE"),
	"TYPE" => "CHECKBOX",
    "DEFAULT" => "N",
);

$arComponentParameters["PARAMETERS"]["SORT_BY"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_USER_PARAM_SORT_BY"),
	"TYPE" => "LIST",
	"DEFAULT" => "LAST_NAME",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_USER_PARAM_SORT_ORDER"),
	"TYPE" => "LIST",
	"DEFAULT" => "ASC",
	"VALUES" => $arSorts,
);

$arComponentParameters["PARAMETERS"]["USER_URL"] = Array(
	"NAME" => GetMessage("GRAIN_LINKS_LIST_USER_PARAM_USER_URL"),
	"TYPE" => "STRING",
	"PARENT" => "DATA_SOURCE",
);

?>