<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arSorts = Array("ASC"=>GetMessage("GRAIN_LINKS_LIST_SITE_SORT_ASC"), "DESC"=>GetMessage("GRAIN_LINKS_LIST_SITE_SORT_DESC"));
$arSortFields = Array(
    "ID"=>GetMessage("GRAIN_LINKS_LIST_SITE_SORT_FID"),
    "NAME"=>GetMessage("GRAIN_LINKS_LIST_SITE_SORT_FNAME"),
    "SORT"=>GetMessage("GRAIN_LINKS_LIST_SITE_SORT_FSORT"),
    "DIR"=>GetMessage("GRAIN_LINKS_LIST_SITE_SORT_FDIR"),
    "LENDIR"=>GetMessage("GRAIN_LINKS_LIST_SITE_SORT_FLENDIR"),
);

$arComponentParameters["PARAMETERS"]["SORT_BY"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_SITE_PARAM_SORT_BY"),
	"TYPE" => "LIST",
	"DEFAULT" => "SORT",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_SITE_PARAM_SORT_ORDER"),
	"TYPE" => "LIST",
	"DEFAULT" => "ASC",
	"VALUES" => $arSorts,
);

$arComponentParameters["PARAMETERS"]["ACTIVE"] = array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_SITE_PARAM_ACTIVE"),
	"TYPE" => "CHECKBOX",
    "DEFAULT" => "N",
);

$arComponentParameters["PARAMETERS"]["CUR_LANG_ONLY"] = array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_SITE_PARAM_CUR_LANG_ONLY"),
	"TYPE" => "CHECKBOX",
    "DEFAULT" => "N",
);

?>