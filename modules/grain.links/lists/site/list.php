<?

/* prepare parameters */

$arParams["ACTIVE"] = $arParams["ACTIVE"]=="Y";
$arParams["CUR_LANG_ONLY"] = $arParams["CUR_LANG_ONLY"]=="Y";

$arParams["SORT_BY"] = trim($arParams["SORT_BY"]);
if(strlen($arParams["SORT_BY"])<=0)
    $arParams["SORT_BY"] = "SORT";
$arParams["SORT_ORDER"] = strtoupper($arParams["SORT_ORDER"]);
if($arParams["SORT_ORDER"]!="DESC")
     $arParams["SORT_ORDER"]="ASC";
    
/* build list */

$arFilter = Array();

if($arParams["CUR_LANG_ONLY"])
    $arFilter["LANGUAGE_ID"]=LANGUAGE_ID;
if($arParams["ACTIVE"])
    $arFilter["ACTIVE"]="Y";

if($arResult["AJAX_RETURN"]) $arFilter["NAME"] = "%".$arResult["AJAX_SEARCH_QUERY"]."%";

if(!is_array($arResult["SELECTED_VALUE"])) $arFilter["ID"] = $arResult["SELECTED_VALUE"];

$rsSites = CSite::GetList($by=$arParams["SORT_BY"],$order=$arParams["SORT_ORDER"],$arFilter);

while($arSite=$rsSites->GetNext()) {

	if(
		$arResult["SELECTED_VALUE"]
		&& (
			(!is_array($arResult["SELECTED_VALUE"]) && $arResult["SELECTED_VALUE"]!=$arSite["ID"])
			|| (is_array($arResult["SELECTED_VALUE"]) && !in_array($arSite["ID"],$arResult["SELECTED_VALUE"]))
		)
	)
		continue;

    $arItem = Array(
    	"NAME" => $arSite["NAME"],
    );
    
    if($arParams["SHOW_URL"]) {

    	$arItem["URL"] = "";
    	if(is_array($arSite['DOMAINS']) && strlen($arSite['DOMAINS'][0]) > 0 || strlen($arSite['DOMAINS']) > 0)
	    	$arItem["URL"] .= "http://";
    	$arItem["URL"] .= is_array($arSite["DOMAINS"])?$arSite["DOMAINS"][0]:$arSite["DOMAINS"];
	   	$arItem["URL"] .= $arSite["DIR"];
        	
    }
    
    $arResult["DATA"][$arSite["ID"]] = $arItem;
    
}
	
?>