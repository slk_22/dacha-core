<?

$MESS["GRAIN_LINKS_LIST_FORUM_TOPIC_PARAM_PARENT_GROUP_ID"] = "Группа форумов (только для выборки форумов ниже)";
$MESS["GRAIN_LINKS_LIST_FORUM_TOPIC_PARAM_PARENT_GROUP_ROOT"] = "Верхний уровень";
$MESS["GRAIN_LINKS_LIST_FORUM_TOPIC_PARAM_APPROVED"] = "Только опубликованные";

$MESS["GRAIN_LINKS_LIST_FORUM_TOPIC_PARAM_SORT_BY1"] = "Поле для первой сортировки";
$MESS["GRAIN_LINKS_LIST_FORUM_TOPIC_PARAM_SORT_ORDER1"] = "Направление для первой сортировки";
$MESS["GRAIN_LINKS_LIST_FORUM_TOPIC_PARAM_SORT_BY2"] = "Поле для второй сортировки";
$MESS["GRAIN_LINKS_LIST_FORUM_TOPIC_PARAM_SORT_ORDER2"] = "Направление для второй сортировки";

$MESS["GRAIN_LINKS_LIST_FORUM_TOPIC_SORT_ASC"] = "По возрастанию";
$MESS["GRAIN_LINKS_LIST_FORUM_TOPIC_SORT_DESC"] = "По убыванию";
$MESS["GRAIN_LINKS_LIST_FORUM_TOPIC_SORT_FID"] = "ID";
$MESS["GRAIN_LINKS_LIST_FORUM_TOPIC_SORT_FTITLE"] = "Заголовок темы";
$MESS["GRAIN_LINKS_LIST_FORUM_TOPIC_SORT_FSTATE"] = "Состояние";
$MESS["GRAIN_LINKS_LIST_FORUM_TOPIC_SORT_FVIEWS"] = "Количество просмотров";
$MESS["GRAIN_LINKS_LIST_FORUM_TOPIC_SORT_FPOSTS"] = "Количество сообщений";
$MESS["GRAIN_LINKS_LIST_FORUM_TOPIC_SORT_FLPD"] = "Дата последнего сообщения";

$MESS["GRAIN_LINKS_LIST_FORUM_TOPIC_PARAM_TOPIC_URL"] = "Шаблон пути к теме форума";

?>