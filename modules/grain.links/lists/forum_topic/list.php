<?
if(CModule::IncludeModule("forum")) {

	/* prepare parameters */
	
	$arGroups = $arParams["FORUM_ID"];
	$arParams["FORUM_ID"] = Array();
	foreach($arGroups as $group_id)
		if(intval($group_id)>0)
			$arParams["FORUM_ID"][] = intval($group_id);

	$arParams["APPROVED"] = $arParams["APPROVED"]=="Y";
	
	$arParams["SORT_BY1"] = trim($arParams["SORT_BY1"]);
	if(strlen($arParams["SORT_BY1"])<=0)
		$arParams["SORT_BY1"] = "TITLE";
	$arParams["SORT_ORDER1"] = strtoupper($arParams["SORT_ORDER1"]);
	if($arParams["SORT_ORDER1"]!="DESC")
		 $arParams["SORT_ORDER1"]="ASC";
	if(strlen($arParams["SORT_BY2"])<=0)
		$arParams["SORT_BY2"] = "LAST_POST_DATE";
	$arParams["SORT_ORDER2"] = strtoupper($arParams["SORT_ORDER2"]);
	if($arParams["SORT_ORDER2"]!="ASC")
		 $arParams["SORT_ORDER2"]="DESC";
		
	/* build list */
	
	$arSort = array(
		$arParams["SORT_BY1"]=>$arParams["SORT_ORDER1"],
		$arParams["SORT_BY2"]=>$arParams["SORT_ORDER2"],
	);
	if(!array_key_exists("ID", $arSort))
		$arSort["ID"] = "DESC";

	$arFilter = Array();

	if($arParams["APPROVED"])
		$arFilter["APPROVED"] = "Y";

	if($arParams["FORUM_ID"])
		$arFilter["@FORUM_ID"] = $arParams["FORUM_ID"];

	if($arResult["SELECTED_VALUE"]) $arFilter[(is_array($arResult["SELECTED_VALUE"])?"@":"")."ID"] = $arResult["SELECTED_VALUE"];

	$rsTopics = CForumTopic::GetList($arSort,$arFilter);
		
	while($arTopic=$rsTopics->GetNext()) {

		$arItem = Array(
			"NAME" => $arTopic["TITLE"],
		);
		
		if($arParams["SHOW_URL"] && $arParams["TOPIC_URL"]) {
		
			$arTopic["SITE_SERVER_NAME"] = defined("SITE_SERVER_NAME")?SITE_SERVER_NAME:"";
			$arTopic["SITE_ID"] = defined("SITE_ID")?SITE_ID:"";
			$arTopic["SITE_DIR"] = defined("SITE_DIR")?SITE_DIR:"";
			
			$arItem["URL"] = $arParams["TOPIC_URL"];
			foreach($arTopic as $FIELD_NAME=>$FIELD_VALUE)
				if(substr($FIELD_NAME,0,1)!="~")
					$arItem["URL"] = str_replace("#".$FIELD_NAME."#",$FIELD_VALUE,$arItem["URL"]);

			$arItem["URL"] = preg_replace("'/+'s", "/", $arItem["URL"]);
			$arItem["URL"] = htmlspecialcharsbx($arItem["URL"]);
			
		}
		
		$arResult["DATA"][$arTopic["ID"]] = $arItem;
		
	}

	if($arResult["AJAX_RETURN"])
		foreach($arResult["DATA"] as $key => $arItem) 
			if(!preg_match("/".preg_quote($arResult["AJAX_SEARCH_QUERY"])."/i".(BX_UTF?"u":""),$arItem["NAME"]))
				unset($arResult["DATA"][$key]);

	
}
?>