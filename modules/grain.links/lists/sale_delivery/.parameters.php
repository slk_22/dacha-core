<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arSorts = Array("ASC"=>GetMessage("GRAIN_LINKS_LIST_SALE_DELIVERY_SORT_ASC"), "DESC"=>GetMessage("GRAIN_LINKS_LIST_SALE_DELIVERY_SORT_DESC"));
$arSortFields = Array(
    "NAME"=>GetMessage("GRAIN_LINKS_LIST_SALE_DELIVERY_SORT_FNAME"),
    "SORT"=>GetMessage("GRAIN_LINKS_LIST_SALE_DELIVERY_SORT_FSORT"),
);

$arType = Array(
    "ALL"=>GetMessage("GRAIN_LINKS_LIST_SALE_DELIVERY_PARAM_TYPE_ALL"),
    "CUSTOM"=>GetMessage("GRAIN_LINKS_LIST_SALE_DELIVERY_PARAM_TYPE_CUSTOM"),
    "AUTO"=>GetMessage("GRAIN_LINKS_LIST_SALE_DELIVERY_PARAM_TYPE_AUTO"),
);

$arComponentParameters["PARAMETERS"]["TYPE"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_SALE_DELIVERY_PARAM_TYPE"),
	"TYPE" => "LIST",
	"VALUES" => $arType,
	"DEFAULT" => "",
);

$arComponentParameters["PARAMETERS"]["ACTIVE"] = array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_SALE_DELIVERY_PARAM_ACTIVE"),
	"TYPE" => "CHECKBOX",
    "DEFAULT" => "N",
);

$arComponentParameters["PARAMETERS"]["SORT_BY"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_SALE_DELIVERY_PARAM_SORT_BY"),
	"TYPE" => "LIST",
	"DEFAULT" => "SORT",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_SALE_DELIVERY_PARAM_SORT_ORDER"),
	"TYPE" => "LIST",
	"DEFAULT" => "ASC",
	"VALUES" => $arSorts,
);

?>