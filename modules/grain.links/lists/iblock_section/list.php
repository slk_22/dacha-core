<?
if(CModule::IncludeModule("iblock")) {

	/* prepare parameters */
	
	$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
	$arParams["PARENT_SECTION"] = intval($arParams["PARENT_SECTION"]);
	$arParams["INCLUDE_SUBSECTIONS"] = $arParams["INCLUDE_SUBSECTIONS"]=="Y";
	$arParams["CHECK_PERMISSIONS"] = $arParams["CHECK_PERMISSIONS"]=="Y";
	$arParams["ACTIVE"] = $arParams["ACTIVE"]=="Y";
	
	$arParams["SORT_BY1"] = trim($arParams["SORT_BY1"]);
	if(strlen($arParams["SORT_BY1"])<=0)
		$arParams["SORT_BY1"] = "TIMESTAMP_X";
	$arParams["SORT_ORDER1"] = strtoupper($arParams["SORT_ORDER1"]);
	if($arParams["SORT_ORDER1"]!="ASC")
		 $arParams["SORT_ORDER1"]="DESC";
	if(strlen($arParams["SORT_BY2"])<=0)
		$arParams["SORT_BY2"] = "SORT";
	$arParams["SORT_ORDER2"] = strtoupper($arParams["SORT_ORDER2"]);
	if($arParams["SORT_ORDER2"]!="DESC")
		 $arParams["SORT_ORDER2"]="ASC";
		
	/* build list */
	
	$arSort = Array();
	if($arParams["INCLUDE_SUBSECTIONS"])
		$arSort["LEFT_MARGIN"] = "ASC";
	$arSort[$arParams["SORT_BY1"]] = $arParams["SORT_ORDER1"];
	$arSort[$arParams["SORT_BY2"]] = $arParams["SORT_ORDER2"];
	if(!array_key_exists("ID", $arSort))
		$arSort["ID"] = "DESC";
	
	$arFilter = array (
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
	);

	if($arParams["ACTIVE"]) {
		$arFilter["ACTIVE"] = "Y";
		$arFilter["GLOBAL_ACTIVE"] = "Y";
	}

	if($arParams["PARENT_SECTION"] && !$arParams["INCLUDE_SUBSECTIONS"]) {
		$arFilter["SECTION_ID"] = $arParams["PARENT_SECTION"];
	} elseif(!$arParams["INCLUDE_SUBSECTIONS"]) {
		$arFilter["SECTION_ID"] = 0;			
	} 
	
	$arParentSection = false;
	if(($arParams["PARENT_SECTION"] && !$arParams["INCLUDE_SUBSECTIONS"]) || ($arResult["AJAX_RETURN"] && !$arParams["INCLUDE_SUBSECTIONS"]) || $arResult["SELECTED_VALUE"])
		$arParentSection = CIBlockSection::GetByID($arParams["PARENT_SECTION"])->GetNext();

	$arFilter["CHECK_PERMISSIONS"] = $arParams["CHECK_PERMISSIONS"]?"Y":"N";

	if($arResult["AJAX_RETURN"] && (!$arParams["INCLUDE_SUBSECTIONS"] || ($arParams["INCLUDE_SUBSECTIONS"] && !$arParams["PARENT_SECTION"]))) 
		$arFilter["%NAME"] = $arResult["AJAX_SEARCH_QUERY"];
	
	if($arResult["SELECTED_VALUE"]) 
		$arFilter["ID"] = $arResult["SELECTED_VALUE"];
	
	$arSelect = Array(
		"ID",
		"IBLOCK_ID",
		"NAME",
		"DEPTH_LEVEL",
	);
	
	if($arParams["SHOW_URL"]) $arSelect[] = "SECTION_PAGE_URL";

	$rsSections = CIBlockSection::GetList($arSort,$arFilter,false,$arSelect);
	if($arParams["SHOW_URL"]) $rsSections->SetUrlTemplates("",$arParams["SECTION_URL"]);
	
	while($arSection=$rsSections->GetNext()) {
	
		if($arParams["PARENT_SECTION"] && $arParams["INCLUDE_SUBSECTIONS"] && !$arResult["SELECTED_VALUE"]) {
			if(!$arParentSection && $arParams["PARENT_SECTION"]==$arSection["ID"]) {
				$arParentSection = $arSection; 
				continue;
			} elseif(!$arParentSection) {
				continue;
			} elseif($arParentSection && $arSection["DEPTH_LEVEL"]<=$arParentSection["DEPTH_LEVEL"]) {
				break;
			}
		}
	
		$parentDepthLevel = false;
		if($arParams["PARENT_SECTION"] && $arParentSection)
			$parentDepthLevel = $arParentSection["DEPTH_LEVEL"];
		elseif(!$arParams["PARENT_SECTION"])
			$parentDepthLevel = 0;
		
		$arItem = Array(
			"NAME" => ($parentDepthLevel!==false?str_repeat(".",$arSection["DEPTH_LEVEL"]-$parentDepthLevel-1):"").$arSection["NAME"],
		);
		
		if($arParams["SHOW_URL"]) $arItem["URL"] = $arSection["SECTION_PAGE_URL"];
		
		$arResult["DATA"][$arSection["ID"]] = $arItem;
			
	}
	
	if($arResult["AJAX_RETURN"] && $arParams["INCLUDE_SUBSECTIONS"] && $arParams["PARENT_SECTION"]) {
		foreach($arResult["DATA"] as $key => $arItem) 
			if(!preg_match("/".preg_quote($arResult["AJAX_SEARCH_QUERY"])."/i".(BX_UTF?"u":""),$arItem["NAME"]))
				unset($arResult["DATA"][$key]);
	}
	
}
?>