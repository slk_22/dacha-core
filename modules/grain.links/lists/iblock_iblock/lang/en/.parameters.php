<?

$MESS["GRAIN_LINKS_LIST_IBLOCK_IBLOCK_PARAM_IBLOCK_TYPE"] = "Type of information block";
$MESS["GRAIN_LINKS_LIST_IBLOCK_IBLOCK_PARAM_ACTIVE"] = "Check elements activity";
$MESS["GRAIN_LINKS_LIST_IBLOCK_IBLOCK_PARAM_CHECK_PERMISSIONS"] = "Respect Access Permissions";

$MESS["GRAIN_LINKS_LIST_IBLOCK_IBLOCK_PARAM_SORT_BY1"] = "Field for the first sorting pass";
$MESS["GRAIN_LINKS_LIST_IBLOCK_IBLOCK_PARAM_SORT_ORDER1"] = "Direction for the first sorting pass";
$MESS["GRAIN_LINKS_LIST_IBLOCK_IBLOCK_PARAM_SORT_BY2"] = "Field for the second sorting pass";
$MESS["GRAIN_LINKS_LIST_IBLOCK_IBLOCK_PARAM_SORT_ORDER2"] = "Direction for the second sorting pass";

$MESS["GRAIN_LINKS_LIST_IBLOCK_IBLOCK_SORT_ASC"] = "Ascending";
$MESS["GRAIN_LINKS_LIST_IBLOCK_IBLOCK_SORT_DESC"] = "Descending";
$MESS["GRAIN_LINKS_LIST_IBLOCK_IBLOCK_SORT_FID"] = "ID";
$MESS["GRAIN_LINKS_LIST_IBLOCK_IBLOCK_SORT_FNAME"] = "Name";
$MESS["GRAIN_LINKS_LIST_IBLOCK_IBLOCK_FELEMENT_CNT"] = "Elements quantity";
$MESS["GRAIN_LINKS_LIST_IBLOCK_IBLOCK_SORT_FSORT"] = "Sorting";
$MESS["GRAIN_LINKS_LIST_IBLOCK_IBLOCK_SORT_FTSAMP"] = "Date of last change";

$MESS["GRAIN_LINKS_LIST_IBLOCK_IBLOCK_PARAM_IBLOCK_URL"] = "Iblock url template";

?>