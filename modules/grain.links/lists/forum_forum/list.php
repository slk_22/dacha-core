<?
if(CModule::IncludeModule("forum")) {

	/* prepare parameters */
	
	$arGroups = $arParams["GROUP_ID"];
	$arParams["GROUP_ID"] = Array();
	foreach($arGroups as $group_id)
		if(strlen(trim($group_id))>0)
			$arParams["GROUP_ID"][] = intval($group_id);

	$arParams["ACTIVE"] = $arParams["ACTIVE"]=="Y";
	$arParams["CHECK_PERMISSIONS"] = $arParams["CHECK_PERMISSIONS"]=="Y";
	
	$arParams["SORT_BY1"] = trim($arParams["SORT_BY1"]);
	if(strlen($arParams["SORT_BY1"])<=0)
		$arParams["SORT_BY1"] = "NAME";
	$arParams["SORT_ORDER1"] = strtoupper($arParams["SORT_ORDER1"]);
	if($arParams["SORT_ORDER1"]!="DESC")
		 $arParams["SORT_ORDER1"]="ASC";
	if(strlen($arParams["SORT_BY2"])<=0)
		$arParams["SORT_BY2"] = "SORT";
	$arParams["SORT_ORDER2"] = strtoupper($arParams["SORT_ORDER2"]);
	if($arParams["SORT_ORDER2"]!="DESC")
		 $arParams["SORT_ORDER2"]="ASC";
		
	/* build list */
	
	$arSort = array(
		$arParams["SORT_BY1"]=>$arParams["SORT_ORDER1"],
		$arParams["SORT_BY2"]=>$arParams["SORT_ORDER2"],
	);
	if(!array_key_exists("ID", $arSort))
		$arSort["ID"] = "DESC";

	$arFilter = Array();

	if($arParams["ACTIVE"])
		$arFilter["ACTIVE"] = "Y";
		
	if($arParams["CHECK_PERMISSIONS"] && !$GLOBALS["USER"]->IsAdmin())
		$arFilter["PERMS"] = array($GLOBALS["USER"]->GetGroups(), 'A');
	
	if($arResult["SELECTED_VALUE"]) $arFilter["ID"] = $arResult["SELECTED_VALUE"];

	$arForums = Array();
	
	if(count($arParams["GROUP_ID"])>1) {
	
		foreach($arParams["GROUP_ID"] as $GROUP_ID) {
		
			$arFilter["FORUM_GROUP_ID"] = $GROUP_ID;
		
			$rsForums = CForumNew::GetList($arSort,$arFilter);
			while($arForum=$rsForums->GetNext())
				$arForums[] = $arForum;
		
		}
	
	} else {

		if($arParams["GROUP_ID"])
			$arFilter["FORUM_GROUP_ID"] = $arParams["GROUP_ID"][0];

		$rsForums = CForumNew::GetList($arSort,$arFilter);
		while($arForum=$rsForums->GetNext())
			$arForums[] = $arForum;

	}

	foreach($arForums as $arForum) {

		$arItem = Array(
			"NAME" => $arForum["NAME"],
		);
		
		if($arParams["SHOW_URL"] && $arParams["FORUM_URL"]) {
		
			$arForum["SITE_SERVER_NAME"] = defined("SITE_SERVER_NAME")?SITE_SERVER_NAME:"";
			$arForum["SITE_ID"] = defined("SITE_ID")?SITE_ID:"";
			$arForum["SITE_DIR"] = defined("SITE_DIR")?SITE_DIR:"";
			
			$arItem["URL"] = $arParams["FORUM_URL"];
			foreach($arForum as $FIELD_NAME=>$FIELD_VALUE)
				if(substr($FIELD_NAME,0,1)!="~")
					$arItem["URL"] = str_replace("#".$FIELD_NAME."#",$FIELD_VALUE,$arItem["URL"]);

			$arItem["URL"] = preg_replace("'/+'s", "/", $arItem["URL"]);
			$arItem["URL"] = htmlspecialcharsbx($arItem["URL"]);
			
		}
		
		$arResult["DATA"][$arForum["ID"]] = $arItem;
		
	}

	if($arResult["AJAX_RETURN"])
		foreach($arResult["DATA"] as $key => $arItem) 
			if(!preg_match("/".preg_quote($arResult["AJAX_SEARCH_QUERY"])."/i".(BX_UTF?"u":""),$arItem["NAME"]))
				unset($arResult["DATA"][$key]);
	
}
?>