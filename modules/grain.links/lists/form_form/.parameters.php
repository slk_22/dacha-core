<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(CModule::IncludeModule("form")) {

	$rsForms = CForm::GetList();
	$arForms = Array();
	while ($arForm = $rsForms->GetNext())
	    $arForms[$arForm["ID"]] = $arForm["NAME"];
	
	$arSorts = Array("ASC"=>GetMessage("GRAIN_LINKS_LIST_FORM_FORM_SORT_ASC"), "DESC"=>GetMessage("GRAIN_LINKS_LIST_FORM_FORM_SORT_DESC"));
	$arSortFields = Array(
	    "S_ID"=>GetMessage("GRAIN_LINKS_LIST_FORM_FORM_SORT_FID"),
	    "S_NAME"=>GetMessage("GRAIN_LINKS_LIST_FORM_FORM_SORT_FNAME"),
		"S_SORT"=>GetMessage("GRAIN_LINKS_LIST_FORM_FORM_SORT_FSORT"),
	);

}

$arComponentParameters["PARAMETERS"]["SORT_BY"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_FORM_FORM_PARAM_SORT_BY"),
	"TYPE" => "LIST",
	"DEFAULT" => "S_SORT",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_FORM_FORM_PARAM_SORT_ORDER"),
	"TYPE" => "LIST",
	"DEFAULT" => "ASC",
	"VALUES" => $arSorts,
);

$arComponentParameters["PARAMETERS"]["FORM_URL"] = Array(
	"NAME" => GetMessage("GRAIN_LINKS_LIST_FORM_FORM_PARAM_FORM_URL"),
	"TYPE" => "STRING",
	"PARENT" => "DATA_SOURCE",
);

$arComponentParameters["PARAMETERS"]["HIDE_FORMS"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_FORM_FORM_PARAM_HIDE_FORMS"),
	"TYPE" => "LIST",
	"VALUES" => $arForms,
	"DEFAULT" => "",
	"MULTIPLE" => "Y",
	"ADDITIONAL_VALUES" => "Y",
);

?>