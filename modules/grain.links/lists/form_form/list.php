<?

if(CModule::IncludeModule("form")) {

	/* prepare parameters */
	
	$arGroups = $arParams["HIDE_FORMS"];
	$arParams["HIDE_FORMS"] = Array();
	foreach($arGroups as $group_id)
	    if(intval($group_id)>0)
	    	$arParams["HIDE_FORMS"][] = intval($group_id);
	
	$arParams["SORT_BY"] = trim($arParams["SORT_BY"]);
	if(strlen($arParams["SORT_BY"])<=0)
	    $arParams["SORT_BY"] = "S_SORT";
	$arParams["SORT_ORDER"] = strtoupper($arParams["SORT_ORDER"]);
	if($arParams["SORT_ORDER"]!="DESC")
	     $arParams["SORT_ORDER"]="ASC";
	    
	/* build list */
	
	$arFilter = Array();
	if($arParams["HIDE_FORMS"]) {
		$arFilter["ID"] = "";
		foreach($arParams["HIDE_FORMS"] as $group_id) {
			if(strlen($arFilter["ID"])>0) $arFilter["ID"] .= "&";
			$arFilter["ID"] .= "~".$group_id."~";
		}
	}
	
	if($arResult["AJAX_RETURN"]) {
		$arFilter["NAME"] = $arResult["AJAX_SEARCH_QUERY"];
		$arFilter["NAME_EXACT_MATCH"] = "N";
	}
	
	if($arResult["SELECTED_VALUE"]) {
		$filter_id = "";
		if(is_array($arResult["SELECTED_VALUE"])) {
			foreach($arResult["SELECTED_VALUE"] as $value) {
				if(strlen($filter_id)>0) $filter_id .= "|";
				$filter_id .= $value;
			}
		} else $filter_id = $arResult["SELECTED_VALUE"];
		if($arParams["HIDE_FORMS"] && strlen($arFilter["ID"])>0) 
			$arFilter["ID"] = $arFilter["ID"]."&(".$filter_id.")";
		else
			$arFilter["ID"] = $filter_id;
	}
	
	$rsForms = CForm::GetList(($by=$arParams["SORT_BY"]), ($order=$arParams["SORT_ORDER"]),$arFilter);
	
	while($arForm=$rsForms->GetNext()) {
	
	    $arItem = Array(
	    	"NAME" => $arForm["NAME"],
	    );
	    
	    if($arParams["SHOW_URL"] && $arParams["FORM_URL"]) {
	    
	    	$arForm["SITE_SERVER_NAME"] = defined("SITE_SERVER_NAME")?SITE_SERVER_NAME:"";
	    	$arForm["SITE_ID"] = defined("SITE_ID")?SITE_ID:"";
	    	$arForm["SITE_DIR"] = defined("SITE_DIR")?SITE_DIR:"";
	    	
	    	$arItem["URL"] = $arParams["FORM_URL"];
	    	foreach($arForm as $FIELD_NAME=>$FIELD_VALUE)
	    		if(substr($FIELD_NAME,0,1)!="~")
	    			$arItem["URL"] = str_replace("#".$FIELD_NAME."#",$FIELD_VALUE,$arItem["URL"]);
	
	    	$arItem["URL"] = preg_replace("'/+'s", "/", $arItem["URL"]);
	    	$arItem["URL"] = htmlspecialcharsbx($arItem["URL"]);
	    	
	    }
	    
	    $arResult["DATA"][$arForm["ID"]] = $arItem;
	    
	}
	
}
?>