<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(CModule::IncludeModule("vote")) {

	$rsChannel = CVoteChannel::GetList();
	$arChannels = Array();
	while($arChannel = $rsChannel->GetNext())
		$arChannels[$arChannel["ID"]] = $arChannel["TITLE"];

	$arSorts = Array("ASC"=>GetMessage("GRAIN_LINKS_LIST_VOTE_CHANNEL_SORT_ASC"), "DESC"=>GetMessage("GRAIN_LINKS_LIST_VOTE_CHANNEL_SORT_DESC"));
	$arSortFields = Array(
	    "S_ID"=>GetMessage("GRAIN_LINKS_LIST_VOTE_CHANNEL_SORT_FID"),
	    "S_TITLE"=>GetMessage("GRAIN_LINKS_LIST_VOTE_CHANNEL_SORT_FTITLE"),
		"S_C_SORT"=>GetMessage("GRAIN_LINKS_LIST_VOTE_CHANNEL_SORT_FSORT"),
		"S_TIMESTAMP"=>GetMessage("GRAIN_LINKS_LIST_VOTE_CHANNEL_SORT_FTMSTP"),
		"S_VOTES"=>GetMessage("GRAIN_LINKS_LIST_VOTE_CHANNEL_SORT_FVOTES"),
	);

}

$arComponentParameters["PARAMETERS"]["SORT_BY"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_VOTE_CHANNEL_PARAM_SORT_BY"),
	"TYPE" => "LIST",
	"DEFAULT" => "S_C_SORT",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_VOTE_CHANNEL_PARAM_SORT_ORDER"),
	"TYPE" => "LIST",
	"DEFAULT" => "ASC",
	"VALUES" => $arSorts,
);

$arComponentParameters["PARAMETERS"]["ACTIVE"] = array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_VOTE_CHANNEL_PARAM_ACTIVE"),
	"TYPE" => "CHECKBOX",
    "DEFAULT" => "N",
);

$arComponentParameters["PARAMETERS"]["VISIBLE"] = array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_VOTE_CHANNEL_PARAM_VISIBLE"),
	"TYPE" => "CHECKBOX",
    "DEFAULT" => "N",
);

$arComponentParameters["PARAMETERS"]["CHANNEL_URL"] = Array(
	"NAME" => GetMessage("GRAIN_LINKS_LIST_VOTE_CHANNEL_PARAM_CHANNEL_URL"),
	"TYPE" => "STRING",
	"PARENT" => "DATA_SOURCE",
);

$arComponentParameters["PARAMETERS"]["HIDE_CHANNELS"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_VOTE_CHANNEL_PARAM_HIDE_CHANNELS"),
	"TYPE" => "LIST",
	"VALUES" => $arChannels,
	"DEFAULT" => "",
	"MULTIPLE" => "Y",
	"ADDITIONAL_VALUES" => "Y",
);

?>