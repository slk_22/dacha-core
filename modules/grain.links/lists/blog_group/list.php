<?
if(CModule::IncludeModule("blog")) {

	/* prepare parameters */

	$arGroups = $arParams["HIDE_GROUPS"];
	$arParams["HIDE_GROUPS"] = Array();
	foreach($arGroups as $group_id)
		if(intval($group_id)>0)
			$arParams["HIDE_GROUPS"][] = intval($group_id);
	
	$arParams["ACTIVE"] = $arParams["ACTIVE"]=="Y";
	
	$arParams["SORT_BY1"] = trim($arParams["SORT_BY1"]);
	if(strlen($arParams["SORT_BY1"])<=0)
		$arParams["SORT_BY1"] = "NAME";
	$arParams["SORT_ORDER1"] = strtoupper($arParams["SORT_ORDER1"]);
	if($arParams["SORT_ORDER1"]!="DESC")
		 $arParams["SORT_ORDER1"]="ASC";
	if(strlen($arParams["SORT_BY2"])<=0)
		$arParams["SORT_BY2"] = "ID";
	$arParams["SORT_ORDER2"] = strtoupper($arParams["SORT_ORDER2"]);
	if($arParams["SORT_ORDER2"]!="ASC")
		 $arParams["SORT_ORDER2"]="DESC";
		
	/* build list */
	
	$arSort = array(
		$arParams["SORT_BY1"]=>$arParams["SORT_ORDER1"],
		$arParams["SORT_BY2"]=>$arParams["SORT_ORDER2"],
	);
	if(!array_key_exists("ID", $arSort))
		$arSort["ID"] = "DESC";
	
	$arFilter = Array();
	if($arParams["HIDE_GROUPS"])
		$arFilter["!ID"] = $arParams["HIDE_GROUPS"];

	if($arParams["ACTIVE"])
		$arFilter["ACTIVE"] = "Y";

	if($arResult["AJAX_RETURN"]) $arFilter["%NAME"] = $arResult["AJAX_SEARCH_QUERY"];
	
	if($arResult["SELECTED_VALUE"]) $arFilter["ID"] = $arResult["SELECTED_VALUE"];

	$rsGroups = CBlogGroup::GetList($arSort,$arFilter);
	
	while($arGroup=$rsGroups->GetNext()) {

		$arItem = Array(
			"NAME" => $arGroup["NAME"],
		);
		
		if($arParams["SHOW_URL"] && $arParams["GROUP_URL"]) {
		
			$arGroup["SITE_SERVER_NAME"] = defined("SITE_SERVER_NAME")?SITE_SERVER_NAME:"";
			$arGroup["SITE_ID"] = defined("SITE_ID")?SITE_ID:"";
			$arGroup["SITE_DIR"] = defined("SITE_DIR")?SITE_DIR:"";
			
			$arItem["URL"] = $arParams["GROUP_URL"];
			foreach($arGroup as $FIELD_NAME=>$FIELD_VALUE)
				if(substr($FIELD_NAME,0,1)!="~")
					$arItem["URL"] = str_replace("#".$FIELD_NAME."#",$FIELD_VALUE,$arItem["URL"]);

			$arItem["URL"] = preg_replace("'/+'s", "/", $arItem["URL"]);
			$arItem["URL"] = htmlspecialcharsbx($arItem["URL"]);
			
		}
		
		$arResult["DATA"][$arGroup["ID"]] = $arItem;
		
	}
	
}
?>