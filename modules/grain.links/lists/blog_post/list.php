<?
if(CModule::IncludeModule("blog")) {

	/* prepare parameters */

	$arTemp = $arParams["BLOG_ID"];
	$arParams["BLOG_ID"] = Array();
	foreach($arTemp as $temp_id)
		if(intval($temp_id)>0)
			$arParams["BLOG_ID"][] = intval($temp_id);

	$arTemp = $arParams["USER_LOGIN"];
	$arParams["USER_LOGIN"] = Array();
	foreach($arTemp as $temp_id)
		if(strlen(trim($temp_id))>0)
			$arParams["USER_LOGIN"][] = trim($temp_id);
	
	$arParams["ACTIVE"] = $arParams["ACTIVE"]=="Y";
	$arParams["OWN"] = $arParams["OWN"]=="Y";
	
	$arParams["SORT_BY1"] = trim($arParams["SORT_BY1"]);
	if(strlen($arParams["SORT_BY1"])<=0)
		$arParams["SORT_BY1"] = "DATE_PUBLISH";
	$arParams["SORT_ORDER1"] = strtoupper($arParams["SORT_ORDER1"]);
	if($arParams["SORT_ORDER1"]!="ASC")
		 $arParams["SORT_ORDER1"]="DESC";
	if(strlen($arParams["SORT_BY2"])<=0)
		$arParams["SORT_BY2"] = "TITLE";
	$arParams["SORT_ORDER2"] = strtoupper($arParams["SORT_ORDER2"]);
	if($arParams["SORT_ORDER2"]!="DESC")
		 $arParams["SORT_ORDER2"]="ASC";
		
	/* build list */
	
	$arSort = array(
		$arParams["SORT_BY1"]=>$arParams["SORT_ORDER1"],
		$arParams["SORT_BY2"]=>$arParams["SORT_ORDER2"],
	);
	if(!array_key_exists("ID", $arSort))
		$arSort["ID"] = "DESC";
	
	$arFilter = Array();

	if($arParams["BLOG_ID"])
		$arFilter["BLOG_ID"] = $arParams["BLOG_ID"];
	
	if($arParams["USER_LOGIN"])
		$arFilter["AUTHOR_LOGIN"] = $arParams["USER_LOGIN"];

	if($arParams["OWN"]) {
		$LOGIN = $GLOBALS["USER"]->GetLogin();
		if(strlen($LOGIN)<=0) $LOGIN = "a0";
		$arFilter["AUTHOR_LOGIN"] = $LOGIN;
	}

	if($arParams["ACTIVE"])
		$arFilter["ACTIVE"] = "Y";

	if($arResult["AJAX_RETURN"]) $arFilter["%TITLE"] = $arResult["AJAX_SEARCH_QUERY"];
	
	if($arResult["SELECTED_VALUE"]) $arFilter["ID"] = $arResult["SELECTED_VALUE"];

	$rsPosts = CBlogPost::GetList($arSort,$arFilter);
	
	while($arPost=$rsPosts->GetNext()) {

		$arItem = Array(
			"NAME" => $arPost["TITLE"],
		);
		
		if($arParams["SHOW_URL"] && $arParams["POST_URL"]) {
		
			$arPost["SITE_SERVER_NAME"] = defined("SITE_SERVER_NAME")?SITE_SERVER_NAME:"";
			$arPost["SITE_ID"] = defined("SITE_ID")?SITE_ID:"";
			$arPost["SITE_DIR"] = defined("SITE_DIR")?SITE_DIR:"";
			
			$arItem["URL"] = $arParams["POST_URL"];
			foreach($arPost as $FIELD_NAME=>$FIELD_VALUE)
				if(substr($FIELD_NAME,0,1)!="~")
					$arItem["URL"] = str_replace("#".$FIELD_NAME."#",$FIELD_VALUE,$arItem["URL"]);

			$arItem["URL"] = preg_replace("'/+'s", "/", $arItem["URL"]);
			$arItem["URL"] = htmlspecialcharsbx($arItem["URL"]);
			
		}
		
		$arResult["DATA"][$arPost["ID"]] = $arItem;
		
	}
	
}
?>