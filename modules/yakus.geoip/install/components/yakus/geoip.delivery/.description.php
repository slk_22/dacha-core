<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => GetMessage("YAKUS_GEOIP_DELIVERY_NAME"),
    "DESCRIPTION" => GetMessage("YAKUS_GEOIP_DELIVERY_DESCRIPTION"),
    "ICON" => "",
    "CACHE_PATH" => "Y",
    "SORT" => 1,
    "PATH" => array(
        "ID" => "yakus",
        "NAME" => GetMessage("YAKUS_COMPANY_NAME"),
        "SORT" => 1,

    ),
);
?>