<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
    "GROUPS"=>array(
        "GENERAL_SETTINGS"=>array(
            "NAME" => GetMessage('YAKUS_GEOIP_GROUP_GENERAL_SETTINGS'),
        ),
    ),
    "PARAMETERS" => array(
        "PRODUCT_ID" => array(
            "PARENT" => "GENERAL_SETTINGS",
            "NAME" => GetMessage("YAKUS_GEOIP_PRODUCT_ID"),
            "TYPE" => "STRING",
            "DEFAULT" => "N",
        ),
        "AJAX_LOAD" => array(
            "PARENT" => "GENERAL_SETTINGS",
            "NAME" => GetMessage("YAKUS_GEOIP_AJAX_LOAD"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "Y",
        ),
        "CONSIDER_PRODUCT_PARAMS" => array(
            "PARENT" => "GENERAL_SETTINGS",
            "NAME" => GetMessage("YAKUS_GEOIP_CONSIDER_PRODUCT_PARAMS"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "Y",
        ),
        "JQUERY" => array(
            "PARENT" => "GENERAL_SETTINGS",
            "NAME" => GetMessage("YAKUS_GEOIP_JQUERY"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
        ),
        "CACHE_TIME"  =>  Array("DEFAULT"=>3600),

    )
);


?>