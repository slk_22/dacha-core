<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $LOCATION_CITY_ID, $LOCATION_CITY_NAME;
$arResult = array();

if(!isset($arParams["CACHE_TIME"]))
    $arParams["CACHE_TIME"] = 36000000;

if(!isset($arParams["AJAX_LOAD"]))
    $arParams["AJAX_LOAD"] = 'Y';

if(CModule::IncludeModule('yakus.geoip') && CModule::IncludeModule('catalog') && CModule::IncludeModule('sale')) {
    $resM = CModule::IncludeModuleEx('yakus.geoip');
    if ($resM == 1 || $resM == 2) {

        if($this->StartResultCache(false, $LOCATION_CITY_ID)) {

            $arUserGroups = $USER->GetUserGroupArray();


            $arOffers = CCatalogSKU::getOffersList(array($arParams['PRODUCT_ID']));
            if (count($arOffers) > 0) {
                $minPrice = 0;
                foreach ($arOffers[$arParams['PRODUCT_ID']] as $arOffer) {
                    $arP = CCatalogProduct::GetOptimalPrice($arOffer['ID'], 1, $arUserGroups);
                    if ($minPrice == 0 || $minPrice > $arP['DISCOUNT_PRICE']) {
                        $minPrice = $arP['DISCOUNT_PRICE'];
                        $arParams['PRODUCT_ID'] = $arOffer['ID'];
                    }
                }

            } else {
                $arP = CCatalogProduct::GetOptimalPrice($arParams['PRODUCT_ID'], 1, $arUserGroups);
                $minPrice = $arP['DISCOUNT_PRICE'];
            }

            $arProduct = CCatalogProduct::GetByID($arParams['PRODUCT_ID']);

            $arFields = array(
                'PRODUCT_ID' => $arParams['PRODUCT_ID'],
            );
            if($arParams['CONSIDER_PRODUCT_PARAMS']=='Y'){
                $arFields = array_merge($arFields, array(
                    'PRICE' => $minPrice,
                    'WEIGHT' => $arProduct['WEIGHT'],
                    'WIDTH' => $arProduct['WIDTH'],
                    'HEIGHT' => $arProduct['HEIGHT'],
                    'LENGTH' => $arProduct['LENGTH'],
                ));
            }

            CYakusGeoIP::Delivery($arResult, $arFields);

            $this->SetResultCacheKeys(array(
                "AllDelivery",
            ));

            $this->IncludeComponentTemplate();
        }

    } else {
        if ($resM == 3) {
            echo GetMessage('YAKUS_GEOIP_DEMO_EXPIRED');
        }
    }
}
?>