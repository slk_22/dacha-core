<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if($_REQUEST['action']=='loadDelivery'){
    global $LOCATION_CITY_ID, $LOCATION_CITY_NAME;
    $LOCATION_CITY_ID = intval($APPLICATION->get_cookie("LOCATION_CITY_ID"));
    $LOCATION_CITY_NAME = $APPLICATION->get_cookie("LOCATION_CITY_NAME");

    $APPLICATION->IncludeComponent(
        "yakus:geoip.delivery",
        $_REQUEST['component_template'],
        array(
            "FROM_AJAX_PHP"=>'Y',

            "PRODUCT_ID" => $_REQUEST['PRODUCT_ID'],
            "COMPONENT_TEMPLATE" => $_REQUEST['COMPONENT_TEMPLATE'],
            "CONSIDER_PRODUCT_PARAMS" => $_REQUEST['CONSIDER_PRODUCT_PARAMS'],
            "AJAX_LOAD" => "N",
            "JQUERY" => "N",
            "CACHE_TYPE" => "A",
        ),
        false
    );
}
?>