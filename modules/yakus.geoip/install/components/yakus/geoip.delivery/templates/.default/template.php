<?global $LOCATION_CITY_ID, $LOCATION_CITY_NAME;?>

<?if($arParams['AJAX_LOAD']=='Y'){?>
    <div id="y-delivery-cart" class="y-delivery-cart">
        <div class="y-delivery-title"><?=GetMessage('YAKUS_GEOIP_DELIVERY_IN_YOUR_CITY')?></div>
        <div class="y-delivery-preloader"></div>
    </div>
    <script>
        var arParams = <?=CUtil::PhpToJsObject($arParams);?>;
        showDeliveryLocation('<?=$arParams['PRODUCT_ID']?>', '<?=str_replace($_SERVER['DOCUMENT_ROOT'], '', __DIR__)?>', arParams);
    </script>
<?}else{?>
    <?if($arParams['FROM_AJAX_PHP']!='Y'){?>
        <div id="y-delivery-cart" class="y-delivery-cart">
    <?}?>

    <div class="y-delivery-title"><?=GetMessage('YAKUS_GEOIP_DELIVERY_IN_YOUR_CITY')?> (<a href="javascript:void(0)" onclick="$('.set-changeRegion').click();"><?=$GLOBALS['LOCATION_CITY_NAME']?></a>)</div>
    <table class="y-delivery-cart-table">
        <?/*<tr class="y-delivery-tr-head"><td><?=GetMessage('YAKUS_GEOIP_DELIVERY_NAME');?></td><td><?=GetMessage('YAKUS_GEOIP_DELIVERY_TRANSIT');?></td><td><?=GetMessage('YAKUS_GEOIP_DELIVERY_PRICE');?></td></tr>*/?>

        <?
        if(count($arResult['AllDelivery'])>0) {
            foreach ($arResult['AllDelivery'] as $delivery) {
                if (!isset($delivery['PROFILES'])) {
                    ?>
                    <tr>
                        <td><?= $delivery['NAME'] ?> <? if (!empty($delivery['DESCRIPTION'])) { ?><span
                                class="y-hover-description-icon">?<div
                                    class="y-hover-description-body"><?= $delivery['DESCRIPTION'] ?></div>
                                </span><? } ?></td>
                        <td><?= $delivery['PADEJ_DAY'] != '-' ? $delivery['TRANSIT'].' '.CYakusGeoIP::padej($delivery['PADEJ_DAY'], GetMessage('YAKUS_GEOIP_DAY1'), GetMessage('YAKUS_GEOIP_DAY2'), GetMessage('YAKUS_GEOIP_DAY5'), false) : '-'; ?></td>
                        <td><?= CurrencyFormat($delivery['PRICE'], CCurrency::GetBaseCurrency()) ?></td>
                    </tr>
                <?
                } else {
                    foreach ($delivery['PROFILES'] as $profile) {
                        ?>
                        <tr>
                            <td>
                                <?if(strlen($delivery['NAME'])>0){?>
                                    <?=$delivery['NAME'] ?> (<?= $profile['TITLE']?>)
                                <?}else{?>
                                    <?= $profile['TITLE'] ?>
                                <?}?>

                                <? if (!empty($profile['DESCRIPTION'])) { ?><span
                                    class="y-hover-description-icon">?<div
                                        class="y-hover-description-body"><?=$profile['DESCRIPTION'] ?></div>
                                    </span><? } ?></td>
                            <td><?= $profile['PADEJ_DAY'] != '-' ? $profile['TRANSIT'].' '.CYakusGeoIP::padej($profile['PADEJ_DAY'], GetMessage('YAKUS_GEOIP_DAY1'), GetMessage('YAKUS_GEOIP_DAY2'), GetMessage('YAKUS_GEOIP_DAY5'), false) : '-'; ?></td>
                            <td><?= CurrencyFormat($profile['PRICE'], CCurrency::GetBaseCurrency()) ?></td>
                        </tr>
                    <? } ?>
                <?
                }
            }
        }else{
            ?>
            <tr><td colspan="3"><?=GetMessage('YAKUS_GEOIP_NOT_DELIVERY')?></td></tr>
        <?
        }
        ?>
    </table>

    <?if($arParams['FROM_AJAX_PHP']!='Y'){?>
        </div>
    <?}?>
<?}?>