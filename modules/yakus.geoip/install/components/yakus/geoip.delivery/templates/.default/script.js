$(function(){

})

function showDeliveryLocation(product_id, url_cur_template, component_template){
    $.ajax({
        url: url_cur_template+'/ajax.php',
        data: {action: 'loadDelivery', PRODUCT_ID: (product_id), COMPONENT_TEMPLATE: (arParams.COMPONENT_TEMPLATE), CONSIDER_PRODUCT_PARAMS: (arParams.CONSIDER_PRODUCT_PARAMS)},
        dataType: 'html',
        success: function(msg){
            $('#y-delivery-cart').html(msg);
        }
    })
}