<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $LOCATION_CITY_ID, $LOCATION_CITY_NAME;
$arResult = array();

if(!isset($arParams['SHOW_LINE']) || empty($arParams['SHOW_LINE'])){
    $arParams['SHOW_LINE'] = 'Y';
}

if(empty($arParams['SHOW_POPUP'])){
    $arParams['SHOW_POPUP'] = 'Y';
}

if(empty($arParams['SHOW_CONFIRM'])){
    $arParams['SHOW_CONFIRM'] = 'Y';
}

if(!empty($arParams['CITY_LIST']) && is_array($arParams['CITY_LIST'])) {
    $arParams['CITY_LIST'] = array_diff($arParams['CITY_LIST'], array('')); //������� ������ �������� �������
}else{
    $arParams['CITY_LIST'] = array(
        0 => GetMessage("YAKUS_GEOIP_MOSKVA"),
        1 => GetMessage("YAKUS_GEOIP_SANKT_PETERBURG"),
        2 => GetMessage("YAKUS_GEOIP_NOVOSIBIRSK"),
        3 => GetMessage("YAKUS_GEOIP_NIJNIY_NOVGOROD"),
        4 => GetMessage("YAKUS_GEOIP_EKATERINBURG"),
        5 => GetMessage("YAKUS_GEOIP_SAMARA"),
        6 => GetMessage("YAKUS_GEOIP_OMSK"),
        7 => GetMessage("YAKUS_GEOIP_KAZANQ"),
        8 => GetMessage("YAKUS_GEOIP_CELABINSK"),
        9 => GetMessage("YAKUS_GEOIP_ROSTOV_NA_DONU"),
        10 => GetMessage("YAKUS_GEOIP_UFA"),
        11 => GetMessage("YAKUS_GEOIP_VOLGOGRAD"),
        12 => GetMessage("YAKUS_GEOIP_PERMQ"),
        13 => GetMessage("YAKUS_GEOIP_KRASNOARSK"),
        14 => GetMessage("YAKUS_GEOIP_SARATOV"),
        15 => GetMessage("YAKUS_GEOIP_VORONEJ"),
        16 => GetMessage("YAKUS_GEOIP_TOLQATTI"),
        17 => GetMessage("YAKUS_GEOIP_KRASNODAR"),
        18 => GetMessage("YAKUS_GEOIP_ULQANOVSK"),
        19 => GetMessage("YAKUS_GEOIP_IJEVSK"),
        20 => GetMessage("YAKUS_GEOIP_AROSLAVLQ"),
        21 => GetMessage("YAKUS_GEOIP_BARNAUL"),
        22 => GetMessage("YAKUS_GEOIP_IRKUTSK"),
        23 => GetMessage("YAKUS_GEOIP_VLADIVOSTOK"),
        24 => GetMessage("YAKUS_GEOIP_HABAROVSK"),
        25 => GetMessage("YAKUS_GEOIP_NOVOKUZNECK"),
        26 => GetMessage("YAKUS_GEOIP_ORENBURG"),
        27 => GetMessage("YAKUS_GEOIP_RAZANQ"),
        28 => GetMessage("YAKUS_GEOIP_PENZA"),
        29 => GetMessage("YAKUS_GEOIP_TUMENQ"),
        30 => GetMessage("YAKUS_GEOIP_NABEREJNYE_CELNY"),
        31 => GetMessage("YAKUS_GEOIP_ASTRAHANQ"),
        32 => GetMessage("YAKUS_GEOIP_LIPECK"),
    );
}

if($arParams['CITY_LIST_SORT']=='NAME'){
    sort($arParams['CITY_LIST']);
}

CModule::IncludeModule('yakus.geoip');

$resM = CModule::IncludeModuleEx('yakus.geoip');
if($resM == 1 || $resM == 2) {

    CYakusGeoIP::GetLocation();

    //��� ������ ������� �� ����������� ����
    $rsL = CSaleLocation::GetList(array("SORT" => "ASC", "CITY_NAME_LANG" => "ASC"), array("LID" => LANGUAGE_ID), false, false, array());
    while ($arL = $rsL->Fetch()) {
        ?>
        <?if ($arL["CITY_NAME"]) {
            $arResult['CITIES'][$arL["ID"]] = $arL["CITY_NAME"] . (!empty($arL["REGION_NAME"]) ? ', ' . $arL["REGION_NAME"] : '') . ', ' . $arL["COUNTRY_NAME"];
        }
    }

    $this->IncludeComponentTemplate();
}else{
    if($resM==3){
        echo GetMessage('YAKUS_GEOIP_DEMO_EXPIRED');
    }
}

?>