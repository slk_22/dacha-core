<?
$MESS['GROUP_GENERAL_SETTINGS']='Основные настройки';
$MESS["JQUERY"]="Подключить JQUERY если он не установлен на сайте";
$MESS["SHOW_LINE"]="Выводить название города";
$MESS["SHOW_POPUP"]="Выводить окно с выбором городов из списка местоположений";
$MESS["SHOW_CONFIRM"]="Спрашивать пользователя правильно ли определен его город?";
$MESS["CITY_LIST"]='Названия местоположений для вывода их во всплывающем окне по умолчанию (под строкой ввода)';
$MESS["CITY_LIST_SORT"]='Сортировка списка городов';
$MESS['CITY_LIST_SORT_LIST']='Как указано в списке';
$MESS['CITY_LIST_SORT_NAME']='По названию';


$MESS["YAKUS_GEOIP_MOSKVA"] = "Москва";
$MESS["YAKUS_GEOIP_SANKT_PETERBURG"] = "Санкт-Петербург";
$MESS["YAKUS_GEOIP_NOVOSIBIRSK"] = "Новосибирск";
$MESS["YAKUS_GEOIP_NIJNIY_NOVGOROD"] = "Нижний Новгород";
$MESS["YAKUS_GEOIP_EKATERINBURG"] = "Екатеринбург";
$MESS["YAKUS_GEOIP_SAMARA"] = "Самара";
$MESS["YAKUS_GEOIP_OMSK"] = "Омск";
$MESS["YAKUS_GEOIP_KAZANQ"] = "Казань";
$MESS["YAKUS_GEOIP_CELABINSK"] = "Челябинск";
$MESS["YAKUS_GEOIP_ROSTOV_NA_DONU"] = "Ростов-на-Дону";
$MESS["YAKUS_GEOIP_UFA"] = "Уфа";
$MESS["YAKUS_GEOIP_VOLGOGRAD"] = "Волгоград";
$MESS["YAKUS_GEOIP_PERMQ"] = "Пермь";
$MESS["YAKUS_GEOIP_KRASNOARSK"] = "Красноярск";
$MESS["YAKUS_GEOIP_SARATOV"] = "Саратов";
$MESS["YAKUS_GEOIP_VORONEJ"] = "Воронеж";
$MESS["YAKUS_GEOIP_TOLQATTI"] = "Тольятти";
$MESS["YAKUS_GEOIP_KRASNODAR"] = "Краснодар";
$MESS["YAKUS_GEOIP_ULQANOVSK"] = "Ульяновск";
$MESS["YAKUS_GEOIP_IJEVSK"] = "Ижевск";
$MESS["YAKUS_GEOIP_AROSLAVLQ"] = "Ярославль";
$MESS["YAKUS_GEOIP_BARNAUL"] = "Барнаул";
$MESS["YAKUS_GEOIP_IRKUTSK"] = "Иркутск";
$MESS["YAKUS_GEOIP_VLADIVOSTOK"] = "Владивосток";
$MESS["YAKUS_GEOIP_HABAROVSK"] = "Хабаровск";
$MESS["YAKUS_GEOIP_NOVOKUZNECK"] = "Новокузнецк";
$MESS["YAKUS_GEOIP_ORENBURG"] = "Оренбург";
$MESS["YAKUS_GEOIP_RAZANQ"] = "Рязань";
$MESS["YAKUS_GEOIP_PENZA"] = "Пенза";
$MESS["YAKUS_GEOIP_TUMENQ"] = "Тюмень";
$MESS["YAKUS_GEOIP_NABEREJNYE_CELNY"] = "Набережные Челны";
$MESS["YAKUS_GEOIP_ASTRAHANQ"] = "Астрахань";
$MESS["YAKUS_GEOIP_LIPECK"] = "Липецк";
?>