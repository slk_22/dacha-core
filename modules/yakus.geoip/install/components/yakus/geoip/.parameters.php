<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
    "GROUPS"=>array(
        "GENERAL_SETTINGS"=>array(
            "NAME" => GetMessage('GROUP_GENERAL_SETTINGS'),
        ),
    ),
    "PARAMETERS" => array(
        "SHOW_LINE" => array(
            "PARENT" => "GENERAL_SETTINGS",
            "NAME" => GetMessage("SHOW_LINE"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "Y",
        ),
        "SHOW_POPUP" => array(
            "PARENT" => "GENERAL_SETTINGS",
            "NAME" => GetMessage("SHOW_POPUP"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "Y",
        ),
        "SHOW_CONFIRM" => array(
            "PARENT" => "GENERAL_SETTINGS",
            "NAME" => GetMessage("SHOW_CONFIRM"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "Y",
        ),
        "CITY_LIST" => array(
            "PARENT" => "GENERAL_SETTINGS",
            "NAME" => GetMessage("CITY_LIST"),
            "TYPE" => "STRING",
            'MULTIPLE'=>'Y',
            "DEFAULT"=>array(
                0 => GetMessage("YAKUS_GEOIP_MOSKVA"),
                1 => GetMessage("YAKUS_GEOIP_SANKT_PETERBURG"),
                2 => GetMessage("YAKUS_GEOIP_NOVOSIBIRSK"),
                3 => GetMessage("YAKUS_GEOIP_NIJNIY_NOVGOROD"),
                4 => GetMessage("YAKUS_GEOIP_EKATERINBURG"),
                5 => GetMessage("YAKUS_GEOIP_SAMARA"),
                6 => GetMessage("YAKUS_GEOIP_OMSK"),
                7 => GetMessage("YAKUS_GEOIP_KAZANQ"),
                8 => GetMessage("YAKUS_GEOIP_CELABINSK"),
                9 => GetMessage("YAKUS_GEOIP_ROSTOV_NA_DONU"),
                10 => GetMessage("YAKUS_GEOIP_UFA"),
                11 => GetMessage("YAKUS_GEOIP_VOLGOGRAD"),
                12 => GetMessage("YAKUS_GEOIP_PERMQ"),
                13 => GetMessage("YAKUS_GEOIP_KRASNOARSK"),
                14 => GetMessage("YAKUS_GEOIP_SARATOV"),
                15 => GetMessage("YAKUS_GEOIP_VORONEJ"),
                16 => GetMessage("YAKUS_GEOIP_TOLQATTI"),
                17 => GetMessage("YAKUS_GEOIP_KRASNODAR"),
                18 => GetMessage("YAKUS_GEOIP_ULQANOVSK"),
                19 => GetMessage("YAKUS_GEOIP_IJEVSK"),
                20 => GetMessage("YAKUS_GEOIP_AROSLAVLQ"),
                21 => GetMessage("YAKUS_GEOIP_BARNAUL"),
                22 => GetMessage("YAKUS_GEOIP_IRKUTSK"),
                23 => GetMessage("YAKUS_GEOIP_VLADIVOSTOK"),
                24 => GetMessage("YAKUS_GEOIP_HABAROVSK"),
                25 => GetMessage("YAKUS_GEOIP_NOVOKUZNECK"),
                26 => GetMessage("YAKUS_GEOIP_ORENBURG"),
                27 => GetMessage("YAKUS_GEOIP_RAZANQ"),
                28 => GetMessage("YAKUS_GEOIP_PENZA"),
                29 => GetMessage("YAKUS_GEOIP_TUMENQ"),
                30 => GetMessage("YAKUS_GEOIP_NABEREJNYE_CELNY"),
                31 => GetMessage("YAKUS_GEOIP_ASTRAHANQ"),
                32 => GetMessage("YAKUS_GEOIP_LIPECK"),
            )
        ),
        "CITY_LIST_SORT" => array(
            "PARENT" => "GENERAL_SETTINGS",
            "NAME" => GetMessage("CITY_LIST_SORT"),
            "TYPE" => "LIST",
            "VALUES" => array(
                'LIST'=>GetMessage("CITY_LIST_SORT_LIST"),
                'NAME'=>GetMessage("CITY_LIST_SORT_NAME"),
            ),
            "DEFAULT" => "LIST",
        ),
        "JQUERY" => array(
            "PARENT" => "GENERAL_SETTINGS",
            "NAME" => GetMessage("JQUERY"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
        ),

    )
);


?>