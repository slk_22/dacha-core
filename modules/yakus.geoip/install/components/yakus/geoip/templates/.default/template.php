<?global $LOCATION_CITY_ID, $LOCATION_CITY_NAME, $LOCATION_CITY_FIRST;?>

<div class="yakus-geo-ip">
    <?if(!empty($arResult['CITIES'])){?>
        <?if($arParams['SHOW_LINE']=='Y'){?>
            <?=GetMessage("YAKUS_GEOIP_YOUR_CITY")?>: <a href="javascript:void(0)" class="set-changeRegion"><?=!empty($LOCATION_CITY_NAME)?$LOCATION_CITY_NAME:GetMessage("YAKUS_GEOIP_SELECT_CITY")?></a>
        <?}?>

        <?if($arParams['SHOW_CONFIRM']=='Y' && $LOCATION_CITY_FIRST){?>
            <div id="modal-confirm" class="modal-confirm">
                <button type="button" class="close-y" data-dismiss="modal-y" aria-hidden="true">&times;</button>
                <div class="modal-confirm-title"><?=GetMessage("YAKUS_GEOIP_YOUR_CITY")?></div>
                <div class="modal-confirm-your-town"><?=$LOCATION_CITY_NAME?> ?</div>
                <button id="modal-confirm-no" class="modal-confirm-no"><?=GetMessage("YAKUS_GEOIP_NO")?></button>
                <button id="modal-confirm-yes" class="modal-confirm-yes"><?=GetMessage("YAKUS_GEOIP_YES")?></button>
            </div>
        <?}?>

        <?if($arParams['SHOW_POPUP']=='Y'){?>
            <div class="modal-y" id="changeRegion">
                <div class="modal-y-dialog">
                    <div class="modal-y-content">
                        <div class="modal-y-header">
                            <button type="button" class="close-y" data-dismiss="modal-y" aria-hidden="true">&times;</button>
                            <h4 class="modal-y-title"><?=GetMessage("YAKUS_GEOIP_VYBERITE_GOROD_V_KO")?></h4>
                        </div>
                        <div class="modal-y-body">

                            <form id="form_location" name="form_location" action="<?=$_SERVER['REQUEST_URI']?>" method="post">
                                <select id="LOCATIONS" name="LOCATIONS" style="display: none;" onchange="cGOROD(this);" >
                                    <option value="0"><?=GetMessage("YAKUS_GEOIP_VYBERITE_GOROD")?></option>
                                    <option value="0"><?=GetMessage("YAKUS_GEOIP_NET_V_SPISKE")?></option>
                                    <?
                                    foreach($arResult['CITIES'] as $id=>$name){?>
                                        <?if($name):?>
                                            <option value="<?=$id?>"><?=htmlspecialchars($name)?></option>
                                        <?endif;?>
                                    <?}?>
                                </select>
                                <input type="text" id="city_name_txt" onkeyup="GetCITY(this);" data-id="0" class="form-control-y" placeholder="<?=GetMessage("YAKUS_GEOIP_VYBERITE_SVOY_GOROD")?>" autocomplete="off">
                                <div id="list_city" style="display: none;"></div>

                                <?
                                $count_in_columt = ceil(count($arParams['CITY_LIST'])/3);
                                $count = 0;
                                foreach($arParams['CITY_LIST'] as $str_city_name){

                                    if($count==0){
                                        ?><ul class="col-xs-4-y"><?
                                    }

                                    ?>
                                        <li><a href="javascript:void(0)"><?=$str_city_name?></a></li>
                                    <?

                                    $count++;
                                    if($count==$count_in_columt){
                                        $count=0;
                                        ?></ul><?
                                    }

                                }

                                if($count!=0){
                                    $count=0;
                                    ?></ul><?
                                }
                                ?>


                                <div style="clear: both"></div>
                                <button type="submit" class="submit_location btn-y btn-primary-y"><?=GetMessage("YAKUS_GEOIP_VYBRATQ_GOROD")?></button>
                            </form>



                        </div>
                    </div><!-- /.modal-y-content -->
                </div><!-- /.modal-y-dialog -->
            </div><!-- /.modal-y -->
            <div id="shadow-y"></div>
        <?}?>
    <?}else{?>
        <?if($arParams['SHOW_LINE']=='Y' && !empty($LOCATION_CITY_NAME)){?>

            <?=GetMessage("YAKUS_GEOIP_YOUR_CITY")?>: <?echo $LOCATION_CITY_NAME;?>
        <?}elseif($arParams['SHOW_LINE']=='Y'){?>
            <?=GetMessage("YAKUS_GEOIP_YOUR_CITY_NON")?>
        <?}?>
    <?}?>
</div>