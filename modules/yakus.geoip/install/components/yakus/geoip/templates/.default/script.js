$(function(){
    $('body').on('click', '.set-changeRegion', function(){
        $('#changeRegion').css('left', $(window).width()/2-300).css('top', $(window).height()/2-$('#changeRegion').height()/2).fadeIn();
        $('#shadow-y').width($(window).width()).height($(window).height()).fadeIn();
    });

    $('.close-y, #shadow-y').click(function(){
        $('#changeRegion').fadeOut();
        $('#shadow-y').fadeOut();
        $('#list_city').hide();
        $('#modal-confirm').fadeOut();
    })

    $('.modal-confirm-no').click(function(){
        $('#modal-confirm').fadeOut();
        $('.set-changeRegion').click();
    })

    $('.modal-confirm-yes').click(function(){
        $('#modal-confirm').fadeOut();
    })

    $('#changeRegion').click(function(){//hide select cities on click body modal
        $('#list_city').hide();
    })

    $('#city_name_txt').click(function(){
        $(this).select();
    })

    $("#form_location a").click(function(){
        SetCity(this, 0);
    })

    $("#form_location a").dblclick(function(){
        $('.submit_location').click();
    })
})


//enter �� ������ ����� ������
$(document).keypress(function(e) {
    if(e.keyCode == 13) {
        if ($("#city_name_txt").is(":focus")) {
            $('#list_city a:first-child').click();
            $('.submit_location').click();
            return false;
        }
    }

});

/*SELECT_CITY*/
function GetCITY(el)
{
    var search_str = $(el).val();
    if($(el).val().length>1)
    {
        var list_city = "";
        $(el).parent().find("option").each(function(){
            var str = $(this).text();
            if(str.indexOf(search_str)==0)
            {
                list_city+='<a href="javascript: void(0);" value="'+$(this).val()+'" onclick="SetCity(this,'+$(this).val()+')">'+str+'</a>';
            }
        });
        if(list_city!="") $(el).next().html(list_city).show();//���������� ������ ���������� �������
        else $(el).next().html("").hide();
    }
    else
    {
        $(el).val(search_str.toUpperCase());
    }
}

function SetCity(el, val){

//val id ��������������
//el js ������� �� ������� �������
    if(parseInt(val)==0){
        //������� id �������������� �� �������� (������ id ����� ���������� � data-id, �� � ����� ������ ������� ��� ������ data-id ���, �.�. ��� ��������� html)
        var search_str = $(el).text();
        var link_local_id = parseInt($(el).attr('data-id'));
        if(link_local_id > 0){
            val = link_local_id;
        }else{
            $(el).parents('.modal-y-body').find("option").each(function(){
                var str = $(this).text();
                var id = $(this).val();
                //if(str.indexOf(search_str)==0){
                only_gorod = str.substr(0, str.indexOf(','));
                if(only_gorod == search_str){
                    val = id;

                }
            });
        }
    }
    $(el).parents('.modal-y-body').find("#LOCATIONS option[value="+val+"]").prop({'selected':'selected'});
    $(el).parents('.modal-y-body').find('#city_name_txt').val($(el).text()).attr('data-id', val);
    $(el).parents('.modal-y-body').find('#list_city').html("").hide();

    $('.submit_location').click();
}