<?
$MESS["YAKUS_GEOIP_YOUR_CITY"] = "Ваш город";
$MESS["YAKUS_GEOIP_VYBERITE_GOROD_V_KO"] = "Выберите город, в который необходимо доставить покупку";
$MESS["YAKUS_GEOIP_VYBERITE_GOROD"] = "Выберите город";
$MESS["YAKUS_GEOIP_NET_V_SPISKE"] = "Нет в списке";
$MESS["YAKUS_GEOIP_VYBERITE_SVOY_GOROD"] = "Выберите свой город доставки";
$MESS["YAKUS_GEOIP_VYBRATQ_GOROD"] = "Выбрать город";
$MESS["YAKUS_GEOIP_SELECT_CITY"] = "Выберите город";
$MESS["YAKUS_GEOIP_YOUR_CITY_NON"] = "Ваш город не определен";
$MESS['YAKUS_GEOIP_NO']='Выбрать другой';
$MESS['YAKUS_GEOIP_YES']='Да, все верно';
?>