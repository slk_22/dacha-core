<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
IncludeTemplateLangFile(__FILE__);
global $arElementsFilterID;
$arElementsFilterID = array();
			if($APPLICATION->GetCurDir()!=SITE_DIR):
					?></div><!--.col_left_in_noborder-->
				</div><!--.col_x2_left--><?
			endif;
			?><div class="col_x1_right"><?
				if($APPLICATION->GetCurDir()!=SITE_DIR):
				?><div class="adv_block"><?
				$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
						"AREA_FILE_SHOW" => "file",
						"PATH" => SITE_TEMPLATE_PATH."/include_areas/adv_bot.php"
					)
				);
				$arFilterTop = array(
						"!ID" => $arElementsFilterID,
						"!PROPERTY_ITEM_STATUS" => false
					);
					$arElementsID = $APPLICATION->IncludeComponent("bitrix:news.list", "last", array(
						"IBLOCK_TYPE" => "articles",
						"IBLOCK_ID" => "#IBLOCK_ID_news#",
						"NEWS_COUNT" => "10",
						"SORT_BY1" => "propertysort_ITEM_STATUS",
						"SORT_ORDER1" => "ASC",
						"SORT_BY2" => "ACTIVE_FROM",
						"SORT_ORDER2" => "DESC",
						"FILTER_NAME" => "arFilterTop",
						"FIELD_CODE" => array(
							0 => "",
							1 => "",
						),
						"PROPERTY_CODE" => array(
							0 => "",
							1 => "",
						),
						"CHECK_DATES" => "Y",
						"DETAIL_URL" => SITE_DIR."articles/#SECTION_CODE#/#ELEMENT_CODE#/",
						"AJAX_MODE" => "N",
						"AJAX_OPTION_JUMP" => "N",
						"AJAX_OPTION_STYLE" => "Y",
						"AJAX_OPTION_HISTORY" => "N",
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "36000000",
						"CACHE_FILTER" => "N",
						"CACHE_GROUPS" => "Y",
						"PREVIEW_TRUNCATE_LEN" => "",
						"ACTIVE_DATE_FORMAT" => "d.m.Y",
						"SET_TITLE" => "N",
						"SET_STATUS_404" => "N",
						"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
						"ADD_SECTIONS_CHAIN" => "N",
						"HIDE_LINK_WHEN_NO_DETAIL" => "N",
						"PARENT_SECTION" => "",
						"PARENT_SECTION_CODE" => "",
						"DISPLAY_TOP_PAGER" => "N",
						"DISPLAY_BOTTOM_PAGER" => "N",
						"PAGER_TITLE" => "Новости",
						"PAGER_SHOW_ALWAYS" => "N",
						"PAGER_TEMPLATE" => "",
						"PAGER_DESC_NUMBERING" => "N",
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
						"PAGER_SHOW_ALL" => "N",
						"DISPLAY_DATE" => "N",
						"DISPLAY_NAME" => "Y",
						"DISPLAY_PICTURE" => "N",
						"DISPLAY_PREVIEW_TEXT" => "N",
						"DISPLAY_DISPLAY" => "2",
						"AJAX_OPTION_ADDITIONAL" => "",
						"DEFAULT_SECTION_NAME" => GetMessage('MAIN_LIST_TITLE'),
						),
						false
					);
				?></div><?
				endif;
				if($APPLICATION->GetCurDir()!=SITE_DIR){
					$arElementsFilterID = array_merge($arElementsFilterID, $arElementsID);
					$arFilterTop = array(
						"!ID" => $arElementsFilterID,
						"!PROPERTY_ITEM_TOP" => false
					);
					$APPLICATION->IncludeComponent("bitrix:news.list", "last", array(
						"IBLOCK_TYPE" => "articles",
						"IBLOCK_ID" => "#IBLOCK_ID_news#",
						"NEWS_COUNT" => "15",
						"SORT_BY1" => "propertysort_ITEM_STATUS",
						"SORT_ORDER1" => "ASC",
						"SORT_BY2" => "ACTIVE_FROM",
						"SORT_ORDER2" => "DESC",
						"FILTER_NAME" => "arFilterTop",
						"FIELD_CODE" => array(
							0 => "",
							1 => "",
						),
						"PROPERTY_CODE" => array(
							0 => "",
							1 => "",
						),
						"CHECK_DATES" => "Y",
						"DETAIL_URL" => SITE_DIR."articles/#SECTION_CODE#/#ELEMENT_CODE#/",
						"AJAX_MODE" => "N",
						"AJAX_OPTION_JUMP" => "N",
						"AJAX_OPTION_STYLE" => "Y",
						"AJAX_OPTION_HISTORY" => "N",
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "36000000",
						"CACHE_FILTER" => "N",
						"CACHE_GROUPS" => "Y",
						"PREVIEW_TRUNCATE_LEN" => "",
						"ACTIVE_DATE_FORMAT" => "d.m.Y",
						"SET_TITLE" => "N",
						"SET_STATUS_404" => "N",
						"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
						"ADD_SECTIONS_CHAIN" => "N",
						"HIDE_LINK_WHEN_NO_DETAIL" => "N",
						"PARENT_SECTION" => "",
						"PARENT_SECTION_CODE" => "",
						"DISPLAY_TOP_PAGER" => "N",
						"DISPLAY_BOTTOM_PAGER" => "N",
						"PAGER_TITLE" => "Новости",
						"PAGER_SHOW_ALWAYS" => "N",
						"PAGER_TEMPLATE" => "",
						"PAGER_DESC_NUMBERING" => "N",
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
						"PAGER_SHOW_ALL" => "N",
						"DISPLAY_DATE" => "N",
						"DISPLAY_NAME" => "Y",
						"DISPLAY_PICTURE" => "N",
						"DISPLAY_PREVIEW_TEXT" => "N",
						"DISPLAY_DISPLAY" => "2",
						"AJAX_OPTION_ADDITIONAL" => "",
						"DEFAULT_SECTION_NAME" => GetMessage('LAST_LIST_TITLE'),
						),
						false
					);
				}
			if($APPLICATION->GetCurDir()==SITE_DIR):
			?><div class="adv_block"><?
				$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
						"AREA_FILE_SHOW" => "file",
						"PATH" => SITE_TEMPLATE_PATH."/include_areas/adv_bot.php"
					)
				);
			?></div><?
			endif
			?></div>
		</div><!--.wrapIn-->
	</div><!--.wrap-->
	<footer class="wrap">
		<div class="wrapIn clearfix">
			<div class="f_logo">
				<a class="logo" href="<?=SITE_DIR?>"><?
					$APPLICATION->IncludeFile(
						$APPLICATION->GetTemplatePath("include_areas/logo.php"),
						Array(),
						Array("MODE"=>"html")
					);
					$time_now = time()
				?></a>
			</div>
			<div class="f_info">
				<?
				$APPLICATION->IncludeFile(
					$APPLICATION->GetTemplatePath("include_areas/info.php"),
					Array(),
					Array("MODE"=>"html")
				);
				?>
			</div>
			<div class="f_info2">
				<?
				$APPLICATION->IncludeFile(
					$APPLICATION->GetTemplatePath("include_areas/info2.php"),
					Array(),
					Array("MODE"=>"html")
				);
				?>
				<a href="<?=SITE_DIR?>feedback/"><?=GetMessage("FEEDBACK_NAME")?></a>
			</div>
			<div class="clear"></div>
			<div class="powered_by">Powered by - <a href="http://redsign.ru/">ALFA Systems</a></div>
		</div>
	</footer>

</div><!--#page-->

	<div style="display:none;">AlfaSystems massmedia K3FN2SA</div>
	
</body>
</html>