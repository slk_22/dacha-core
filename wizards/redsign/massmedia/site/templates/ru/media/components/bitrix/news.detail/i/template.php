<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?><article class="news_detail"><?
	if($arParams["DISPLAY_NAME"]!="N" && $arResult["NAME"]):
		?><h1><?=$arResult["NAME"];
		if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):
			?> | <time class="text_color"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></time><?
		endif;
		?></h1><?
	endif;
	if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arResult["FIELDS"]["PREVIEW_TEXT"]):
		?><p class="preview_text"><strong><?=$arResult["FIELDS"]["PREVIEW_TEXT"];unset($arResult["FIELDS"]["PREVIEW_TEXT"]);?></strong></p><?
	endif;
	if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):
		?><img class="detail_picture" src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" alt="<?=$arResult["NAME"]?>"  title="<?=$arResult["NAME"]?>" /><?
	endif;
	?><div class="content"><?
	if($arResult["NAV_RESULT"]):
		if($arParams["DISPLAY_TOP_PAGER"]):
			echo $arResult["NAV_STRING"];
		endif;
		echo $arResult["NAV_TEXT"];
		if($arParams["DISPLAY_BOTTOM_PAGER"]):
			echo $arResult["NAV_STRING"];
		endif;
	elseif(strlen($arResult["DETAIL_TEXT"])>0):
		echo $arResult["DETAIL_TEXT"];
	else:
		echo $arResult["PREVIEW_TEXT"];
	endif;
	foreach($arResult["FIELDS"] as $code=>$value):
		echo GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?=$value;?><br /><?
	endforeach;
	foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty):
		echo $arProperty["NAME"]?>:&nbsp;<?
		if(is_array($arProperty["DISPLAY_VALUE"])):
			echo implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);
		else:
			echo $arProperty["DISPLAY_VALUE"];
		endif
		?><br /><?
	endforeach;
	?></div><?
	if(array_key_exists("USE_SHARE", $arParams) && $arParams["USE_SHARE"] == "Y"){
		?><div class="news-detail-share">
			<noindex><?
			$APPLICATION->IncludeComponent("bitrix:main.share", "", array(
					"HANDLERS" => $arParams["SHARE_HANDLERS"],
					"PAGE_URL" => $arResult["~DETAIL_PAGE_URL"],
					"PAGE_TITLE" => $arResult["~NAME"],
					"SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
					"SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
					"HIDE" => $arParams["SHARE_HIDE"],
				),
				$component,
				array("HIDE_ICONS" => "Y")
			);
			?></noindex>
		</div><?
	}
?></article>
