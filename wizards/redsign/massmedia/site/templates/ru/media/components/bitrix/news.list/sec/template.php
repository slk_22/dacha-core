<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$iItemCount = count($arResult["ITEMS"]);
if($iItemCount):
if($arParams["DISPLAY_TOP_PAGER"]):
	echo $arResult["NAV_STRING"];
endif;
?><section class="news_list news_sec clearfix"><?
$arSection = end($arResult["SECTION"]["PATH"]);
if(!empty($arSection["NAME"]) && $arParams["DISPLAY_SECTION_NAME"] != "N"):
	?><h1><a href="<?=$arSection["SECTION_PAGE_URL"]?>"><?=$arSection["NAME"]?></a></h1><?
endif;
foreach($arResult["ITEMS"] as $keyItem => $arItem):
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?><article class="news_item long" id="<?=$this->GetEditAreaId($arItem['ID']);?>"><?
		if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):
			if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):
				?><a class="picture" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img class="picture" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" /></a><?
			else:
				?><img class="picture" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" /><?
			endif;
		endif;
		if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):
			?><h1><?
			if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):
				?><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a><?
			else:
				echo $arItem["NAME"];
			endif;
			if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):
				?> | <time class="text_color"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></time><?
			endif
			?></h1><?
		endif
		?><div class="body"><?
		if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]	):
			echo $arItem["PREVIEW_TEXT"];
		endif;
		foreach($arItem["FIELDS"] as $code=>$value):
			?><small><?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?=$value;?></small><br /><?
		endforeach;
		foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):
			?><small><?=$arProperty["NAME"]?>:&nbsp;<?
			if(is_array($arProperty["DISPLAY_VALUE"])):
				echo implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);
			else:
				echo $arProperty["DISPLAY_VALUE"];
			endif
			?></small><br /><?
		endforeach;
		?></div><?
	?></article><?
endforeach;
?></section><?
if($arParams["DISPLAY_BOTTOM_PAGER"]):
	echo $arResult["NAV_STRING"];
endif;
endif?>