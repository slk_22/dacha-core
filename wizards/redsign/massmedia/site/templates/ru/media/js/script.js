$(document).ready(function(){
	var search_input_width = false;
	$('#page > header .search_form input[type="text"]').on("focus", function(){
		search_input_width = $(this).width();
		$(this).animate({"width": "200px"}, 200);
	}).on("blur", function(){
		var input = $(this);
		input.animate({"width": search_input_width+"px"}, 200, function(){
			input.removeAttr("style");
		})
	});
});