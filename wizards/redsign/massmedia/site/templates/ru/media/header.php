<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?><!DOCTYPE html>
<html>
<head>
	<title><?$APPLICATION->ShowTitle()?> | <?=COption::GetOptionString("main", "site_name")?></title>
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery-1.10.1.min.js"></script><?
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/script.js');
	?><!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->
	<?$APPLICATION->ShowHead();?>
	<link href="<?=SITE_TEMPLATE_PATH?>/media.css" rel="stylesheet" />
</head>
<body>
<?$APPLICATION->ShowPanel()?>
<div id="page">
	<header>
		<div class="wrap">
			<div class="wrapIn clearfix"><?
			$APPLICATION->IncludeComponent("imyie:ywheather", "home", array(
				"LANG_CHARSET" => "AUTO",
				"COUNTRY" => "Rossiya",
				"CITY" => "27612",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "10000",
				"DISPLAY_DAYS" => "1",
				"WHEATHER_PAGE" => "/wheather/"
				),
				false
			);
			?><time class="date_now"><?$time_now = time(); echo FormatDate("l, j F, Y", $time_now)?> <span class="text_color"> <?=FormatDate("H:i", $time_now)?></span></time>
			<a class="logo" href="<?=SITE_DIR?>"><?
				$APPLICATION->IncludeFile(
					$APPLICATION->GetTemplatePath("include_areas/logo.php"),
					Array(),
					Array("MODE"=>"html")
				);
			?></a><?
			$APPLICATION->IncludeComponent("bitrix:search.title", "i", array(
				"NUM_CATEGORIES" => "1",
				"TOP_COUNT" => "5",
				"ORDER" => "date",
				"USE_LANGUAGE_GUESS" => "N",
				"CHECK_DATES" => "N",
				"SHOW_OTHERS" => "N",
				"PAGE" => SITE_DIR."search/",
				"CATEGORY_0_TITLE" => "",
				"CATEGORY_0" => array(
					0 => "no",
				),
				"SHOW_INPUT" => "Y",
				"INPUT_ID" => "title-search-input",
				"CONTAINER_ID" => "search_form",
				"SEARCH_TEXT_SAMPLE" => "Поиск..."
				),
				false
			);?>
			</div>
		</div><?
		$APPLICATION->IncludeComponent(
				"bitrix:menu",
				"head",
				Array(
					"ROOT_MENU_TYPE" => "top",
					"MAX_LEVEL" => "1",
					"CHILD_MENU_TYPE" => "top",
					"USE_EXT" => "Y",
					"DELAY" => "N",
					"ALLOW_MULTI_SELECT" => "N",
					"MENU_CACHE_TYPE" => "N",
					"MENU_CACHE_TIME" => "3600",
					"MENU_CACHE_USE_GROUPS" => "Y",
					"MENU_CACHE_GET_VARS" => ""
				),
			false
			);
	?></header>
	<div id="content" class="wrap">
		<div class="wrapIn clearfix"><?
		if($APPLICATION->GetCurDir()!=SITE_DIR):
			?><div class="col_x2_left">
				<div class="col_left_in_noborder"><?
		endif;
		if($APPLICATION->GetCurDir()!=SITE_DIR && strpos($APPLICATION->GetCurDir(),"articles/")===false):
			?><h1><?$APPLICATION->ShowTitle()?></h1><?
		endif;?>