<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

CopyDirFiles(
	WIZARD_ABSOLUTE_PATH."/site/public/ru",
	WIZARD_SITE_PATH,
	$rewrite = true,
	$recursive = true,
	$delete_after_copy = false
);

WizardServices::PatchHtaccess(WIZARD_SITE_PATH);?>