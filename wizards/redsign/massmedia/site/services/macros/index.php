<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!CModule::IncludeModule("iblock"))
	return;

	$wiz_site_id = WIZARD_SITE_ID;

if (isset($wiz_site_id) && ($wiz_site_id!="s1"))
	$SITE_SUBCATALOG = WIZARD_SITE_DIR."/";

	// replace macros #SITE_DIR#
	$sitDir = WIZARD_SITE_PATH;
	WizardServices::ReplaceMacrosRecursive($_SERVER["DOCUMENT_ROOT"]."/", Array("SITE_DIR" => WIZARD_SITE_DIR));
	WizardServices::ReplaceMacrosRecursive($sitDir."/", Array("SITE_DIR" => WIZARD_SITE_DIR));

	$tmplDir = $_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/".WIZARD_TEMPLATE_ID."/";
	WizardServices::ReplaceMacrosRecursive($tmplDir, Array("SITE_DIR" => WIZARD_SITE_DIR));

	// ____________________________________________________________________________________________________________________________ //

	// take ID iblocks

	$arrFilter1 = array(
		array(
			"IBLOCK_TYPE" => "articles",
			"IBLOCK_CODE" => "news",
			"IBLOCK_XML_ID" => "news_".WIZARD_SITE_ID,
		),
	);

	foreach($arrFilter1 as $filter1)
	{
		$rsIBlock = CIBlock::GetList(array(), array( "TYPE" => $filter1["IBLOCK_TYPE"], "CODE" => $filter1["IBLOCK_CODE"], "XML_ID" => $filter1["IBLOCK_XML_ID"] ));
		if ($arIBlock = $rsIBlock->Fetch())
		{
			$code1 = $filter1["IBLOCK_CODE"];
			$arrIBlockIDs[$code1] = $arIBlock["ID"];
		}
	}

	// ____________________________________________________________________________________________________________________________ //

	// General macros
	// #SITE_DIR#

	// replace macros for iblock "news"
	// #IBLOCK_ID_news#
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/_index.php", array("IBLOCK_ID_news" => $arrIBlockIDs["news"]));
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/.top.menu_ext.php", array("IBLOCK_ID_news" => $arrIBlockIDs["news"]));
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/articles/index.php", array("IBLOCK_ID_news" => $arrIBlockIDs["news"]));
	CWizardUtil::ReplaceMacros($_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/".WIZARD_TEMPLATE_ID."/footer.php", array("IBLOCK_ID_news" => $arrIBlockIDs["news"]));
	// ____________________________________________________________________________________________________________________________ //

	// replace phone nubmer
	$phoneCode = $wizard->GetVar("siteTelephoneCode");
	$phoneNumber = $wizard->GetVar("siteTelephone");
	CWizardUtil::ReplaceMacros($_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/".WIZARD_TEMPLATE_ID."/include_areas/info.php", array("PHONE_MAIN" => $phoneCode.' '.$phoneNumber));
	
	$EMail = $wizard->GetVar("shopEmail");
	CWizardUtil::ReplaceMacros($_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/".WIZARD_TEMPLATE_ID."/include_areas/info.php", array("EMAIL" => $EMail));
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/feedback/index.php", array("EMAIL" => $EMail));
	
	// replace siteDiscription and siteKeywords
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/.section.php", array("SITE_DESCRIPTION" => $wizard->GetVar("siteMetaDescription")));
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/.section.php", array("SITE_KEYWORDS" => $wizard->GetVar("siteMetaKeywords")));

	// ____________________________________________________________________________________________________________________________ //
?>