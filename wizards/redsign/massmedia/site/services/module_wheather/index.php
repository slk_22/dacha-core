<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!IsModuleInstalled('imyie.ywheather')){
	// InstallDB
	RegisterModule("imyie.ywheather");

	// InstallEvents

	// InstallOptions

	// InstallFiles
	CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/imyie.ywheather/install/components", $_SERVER["DOCUMENT_ROOT"]."/bitrix/components", true, true);

	// InstallPublic
}?>