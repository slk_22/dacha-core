<?
$MESS["SERVICE_STEP_SITE_CREATE"] = "Создание сайта";
$MESS["SERVICE_STEP_COPY_FILES"] = "Копирование файлов";
$MESS["SERVICE_STEP_COPY_TEMPLATES"] = "Копирование шаблона и темы";
$MESS["SERVICE_STEP_SET_MENU_TYPES"] = "Установка типов меню";
$MESS["SERVICE_STEP_URL_REWRITE"] = "Обработка URLREWRITE";
$MESS["SERVICE_STEP_SETTINGS"] = "Настройка сайта";
$MESS["SERVICE_STEP_IBLOCK"] = "Установка типов и импорт инфоблоков";
$MESS["SERVICE_STEP_INSTALL_MODULE_WHEATHER"] = "Модуль. Информер Погоды.";
$MESS["SERVICE_STEP_MACROS"] = "Замена макросов";
?>