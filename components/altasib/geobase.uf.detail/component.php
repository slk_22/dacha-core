<? if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)
	die();

$incMod = CModule::IncludeModuleEx("altasib.geobase");
if ($incMod == '0' || $incMod == '3')
	return false;

$arSelCity = CAltasibGeoBaseSelected::GetCurrentCityFromSelected();

if(!empty($arSelCity) && count($arSelCity) > 0)
{
	$arUFields = CAltasibGeoBaseSelected::GetFieldsCity($arSelCity["ID"],
		($arParams["NO_NAME"] == "Y" ? false : LANGUAGE_ID));
}
else
{
	$arUFields = CAltasibGeoBaseSelected::GetFieldsCity(0,
		($arParams["NO_NAME"] == "Y" ? false : LANGUAGE_ID));
}

if(!empty($arSelCity) && count($arSelCity) > 0)
{
	if(!empty($arSelCity["C_NAME"]) && strlen($arSelCity["C_NAME"]) > 0)
		$arResult["SELECT_CITY"] = $arSelCity["C_NAME"];
	elseif(!empty($arSelCity["C_NAME_EN"]) && strlen($arSelCity["C_NAME_EN"]) > 0)
		$arResult["SELECT_CITY"] = $arSelCity["C_NAME_EN"];

	if(!empty($arSelCity["R_FNAME"]) && strlen($arSelCity["R_FNAME"]) > 0)
		$arResult["SELECT_REGION"] = $arSelCity["R_FNAME"];

	if(!empty($arSelCity["CTR_NAME_RU"]) && strlen($arSelCity["CTR_NAME_RU"]) > 0)
		$arResult["SELECT_COUNTRY"] = $arSelCity["CTR_NAME_RU"];
	else
		$arResult["SELECT_COUNTRY"] = GetMessage("ALTASIB_SHOW_RUSSIA");

	if(!empty($arSelCity["CTR_CODE"]) && strlen($arSelCity["CTR_CODE"]) > 0)
		$arResult["SELECT_COUNTRY_CODE"] = $arSelCity["CTR_CODE"];
	else
		$arResult["SELECT_COUNTRY_CODE"] = GetMessage("ALTASIB_SHOW_RU");
}

if(empty($arParams["USERFIELD_LIST"]))
	$arParams["USERFIELD_LIST"] = array();

if(!empty($arUFields) && !empty($arParams["USERFIELD_LIST"]))
{
	foreach($arUFields as $arFld)
	{
		if(in_array($arFld["FIELD_NAME"], $arParams["USERFIELD_LIST"]))
			$arResult["UF"][] = $arFld;
	}
}

$this->IncludeComponentTemplate();
?>