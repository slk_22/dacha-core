<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$mid = "altasib.geobase";

$incMod = CModule::IncludeModuleEx($mid);
if ($incMod == '0' || $incMod == '3')
	return false;

$arResult = CAltasibGeoBase::GetDataKladr();
$arResult["auto"] = CAltasibGeoBase::GetAddres();

$arResult["REGION_DISABLE"] = COption::GetOptionString($mid, 'region_disable', 'N');
$arResult["POPUP_BACK"] = COption::GetOptionString($mid, "popup_back", "Y");
$arResult['MODE_LOCATION'] = strtoupper(COption::GetOptionString($mid, "mode_location", "CITIES"));

////Mobile detect////

$checkType = CAltasibGeoBase::DeviceIdentification();

/////////////////////

if ($checkType == 'mobile' || $checkType == 'pda') {
	$this->IncludeComponentTemplate("mobile");
}
else
	$this->IncludeComponentTemplate();
?>